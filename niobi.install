<?php

/**
 * @file
 * Install, update and uninstall functions for the niobi installation profile.
 */

use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;
use Drupal\shortcut\Entity\Shortcut;

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * @see system_install()
 */
function niobi_install() {
  // Set front page to the user's dashboard.
  \Drupal::configFactory()->getEditable('system.site')->set('page.front', '/user')->save(TRUE);

  // Allow visitor account creation with administrative approval.
  $user_settings = \Drupal::configFactory()->getEditable('user.settings');
  $user_settings->set('register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)->save(TRUE);

  // Enable default permissions for system roles.
  // Currently these are commented as we are not yet sure what we want to grant
  //~ user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['access comments']);
  //~ user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['access comments', 'post comments', 'skip comment approval']);

  // Assign user 1 the "administrator" role.
  $user = User::load(1);
  $user->roles[] = 'administrator';
  $user->save();

  // We install some menu links, so we have to rebuild the router, to ensure the
  // menu links are valid.
  \Drupal::service('router.builder')->rebuildIfNeeded();

  // We will set user permissions once we know what we want to set
  //~ user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['access content']);
  //~ user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['access content']);

  // Allow authenticated users to use shortcuts.
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['access shortcuts']);

  // Populate the default shortcut set.
  $shortcut = Shortcut::create([
    'shortcut_set' => 'default',
    'title' => t('Add content'),
    'weight' => -20,
    'link' => ['uri' => 'internal:/node/add'],
  ]);
  $shortcut->save();

  $shortcut = Shortcut::create([
    'shortcut_set' => 'default',
    'title' => t('All content'),
    'weight' => -19,
    'link' => ['uri' => 'internal:/admin/content'],
  ]);
  $shortcut->save();

  // Allow all users to use search.
  user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['search content']);
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['search content']);

  // Enable the admin theme.
  \Drupal::configFactory()->getEditable('node.settings')->set('use_admin_theme', TRUE)->save(TRUE);

  // Enable niobi_group module and niobi_theme theme, now that everything else has been loaded, and the prerequisite
  // configs have been isntalled.
  \Drupal::service('theme_installer')->install(['niobi_theme']);

  // Some of the dependent modules require permissions to be rebuilt. Do so now.
  node_access_rebuild(FALSE);
}
