<?php

namespace Drupal\niobi_form\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Niobi Form edit form.
 *
 * @ingroup niobi_form
 */
class NiobiFormForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\niobi_form\Entity\NiobiForm */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Niobi Form.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Niobi Form.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.niobi_form.canonical', ['niobi_form' => $entity->id()]);
  }

}
