<?php

namespace Drupal\niobi_form\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class NiobiFormTypeForm.
 */
class NiobiFormTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $niobi_form_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $niobi_form_type->label(),
      '#description' => $this->t("Label for the Niobi Form type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $niobi_form_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\niobi_form\Entity\NiobiFormType::load',
      ],
      '#disabled' => !$niobi_form_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $niobi_form_type = $this->entity;
    $status = $niobi_form_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Niobi Form type.', [
          '%label' => $niobi_form_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Niobi Form type.', [
          '%label' => $niobi_form_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($niobi_form_type->toUrl('collection'));
  }

}
