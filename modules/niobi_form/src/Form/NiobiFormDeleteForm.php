<?php

namespace Drupal\niobi_form\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Niobi Form entities.
 *
 * @ingroup niobi_form
 */
class NiobiFormDeleteForm extends ContentEntityDeleteForm {


}
