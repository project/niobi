<?php

namespace Drupal\niobi_form\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\niobi_form\Entity\NiobiFormInterface;
use Drupal\webform\WebformEntityReferenceManagerInterface;
use Drupal\webform\WebformInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a controller for niobi form entity references.
 */
class NiobiFormEntityReferenceController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The webform entity reference manager.
   *
   * @var \Drupal\webform\WebformEntityReferenceManagerInterface
   */
  protected $webformEntityReferenceManager;

  /**
   * Constructs a WebformNodeEntityReferenceController object.
   *
   * @param \Drupal\webform\WebformEntityReferenceManagerInterface $webform_entity_reference_manager
   *   The webform entity reference manager.
   */
  public function __construct(WebformEntityReferenceManagerInterface $webform_entity_reference_manager) {
    $this->webformEntityReferenceManager = $webform_entity_reference_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('webform.entity_reference_manager')
    );
  }

  /**
   * Set the current webform for a niobi_form with multiple webform attached.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\niobi_form\Entity\NiobiFormInterface $niobi_form
   *   A niobi_form.
   * @param \Drupal\webform\WebformInterface $webform
   *   A webform.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect to a selected destination or the niobi_form's URL.
   */
  public function change(Request $request, NiobiFormInterface $niobi_form, WebformInterface $webform) {
    $this->webformEntityReferenceManager->setUserWebformId($niobi_form, $webform->id());
    return new RedirectResponse($request->query->get('destination') ?: $niobi_form->toUrl()->setAbsolute()->toString());
  }

}
