<?php

namespace Drupal\niobi_form\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Niobi Form type entity.
 *
 * @ConfigEntityType(
 *   id = "niobi_form_type",
 *   label = @Translation("Niobi Form type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\niobi_form\NiobiFormTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\niobi_form\Form\NiobiFormTypeForm",
 *       "edit" = "Drupal\niobi_form\Form\NiobiFormTypeForm",
 *       "delete" = "Drupal\niobi_form\Form\NiobiFormTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\niobi_form\NiobiFormTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "niobi_form_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "niobi_form",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/niobi/niobi_form_type/{niobi_form_type}",
 *     "add-form" = "/admin/niobi/niobi_form_type/add",
 *     "edit-form" = "/admin/niobi/niobi_form_type/{niobi_form_type}/edit",
 *     "delete-form" = "/admin/niobi/niobi_form_type/{niobi_form_type}/delete",
 *     "collection" = "/admin/niobi/niobi_form_type"
 *   }
 * )
 */
class NiobiFormType extends ConfigEntityBundleBase implements NiobiFormTypeInterface {

  /**
   * The Niobi Form type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Niobi Form type label.
   *
   * @var string
   */
  protected $label;

}
