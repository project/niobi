<?php

namespace Drupal\niobi_form\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Niobi Form entities.
 *
 * @ingroup niobi_form
 */
interface NiobiFormInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Niobi Form name.
   *
   * @return string
   *   Name of the Niobi Form.
   */
  public function getName();

  /**
   * Sets the Niobi Form name.
   *
   * @param string $name
   *   The Niobi Form name.
   *
   * @return \Drupal\niobi_form\Entity\NiobiFormInterface
   *   The called Niobi Form entity.
   */
  public function setName($name);

  /**
   * Gets the Niobi Form creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Niobi Form.
   */
  public function getCreatedTime();

  /**
   * Sets the Niobi Form creation timestamp.
   *
   * @param int $timestamp
   *   The Niobi Form creation timestamp.
   *
   * @return \Drupal\niobi_form\Entity\NiobiFormInterface
   *   The called Niobi Form entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Niobi Form published status indicator.
   *
   * Unpublished Niobi Form are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Niobi Form is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Niobi Form.
   *
   * @param bool $published
   *   TRUE to set this Niobi Form to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\niobi_form\Entity\NiobiFormInterface
   *   The called Niobi Form entity.
   */
  public function setPublished($published);

}
