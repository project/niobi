<?php

namespace Drupal\niobi_form\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Niobi Form type entities.
 */
interface NiobiFormTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
