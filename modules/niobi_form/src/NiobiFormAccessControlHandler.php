<?php

namespace Drupal\niobi_form;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\node\Entity\Node;

/**
 * Access controller for the Niobi Form entity.
 *
 * @see \Drupal\niobi_form\Entity\NiobiForm.
 */
class NiobiFormAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\niobi_form\Entity\NiobiFormInterface $entity */
    switch ($operation) {
      case 'view':
        if ($entity->hasField('field_visibility')) {
          $viz = $entity->get('field_visibility')->getValue();
          if (isset($viz[0]['value']) && $viz[0]['value']) {
            return accessResult::allowed();
          }
        }
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished niobi form entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published niobi form entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit niobi form entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete niobi form entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add niobi form entities');
  }

}
