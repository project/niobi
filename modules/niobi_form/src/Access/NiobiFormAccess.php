<?php

namespace Drupal\niobi_form\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\niobi_form\Entity\NiobiFormInterface;
use Drupal\node\Entity\Node;
use Drupal\webform\Access\WebformEntityAccess;
use Drupal\webform\Access\WebformSubmissionAccess;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Defines the custom access control handler for the niobi form.
 */
class NiobiFormAccess {

  /**
   * Check whether the user can access a niobi_form's webform results.
   *
   * @param string $operation
   *   Operation being performed.
   * @param string $entity_access
   *   Entity access rule that needs to be checked.
   * @param \Drupal\niobi_form\Entity\NiobiFormInterface $niobi_form
   *   A niobi_form.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public static function checkWebformResultsAccess(NiobiFormInterface $niobi_form, AccountInterface $account) {
    if ($account->hasPermission('administer niobi form entities')) {
      return AccessResult::allowed();
    }
    // if user is in admin team of the workflow, then users can view
    $applicationId = \Drupal::request()->query->get('application_id');
    if (!empty($applicationId)) {
      $application = current(\Drupal::entityTypeManager()
        ->getStorage('niobi_application')
        ->loadByProperties(['uuid' => $applicationId]));
      if (!empty($application)) {
        $workflow = $application->getApplicationWorkflow();
        if (!empty($workflow)) {
          if ($workflow->containForm($niobi_form) && $workflow->isOnAdminTeam($account->id())) {
            /** @var \Drupal\webform\WebformEntityReferenceManagerInterface $entity_reference_manager */
//            $entity_reference_manager = \Drupal::service('webform.entity_reference_manager');
//            $webform = $entity_reference_manager->getWebform($niobi_form);
//            return WebformEntityAccess::checkResultsAccess($webform, $niobi_form);
            return AccessResult::allowed();
          }
        }
      }
    } else {
      $workflows = \Drupal::entityTypeManager()
        ->getStorage('niobi_application_workflow')
        ->loadByProperties(['field_workflow_admin_team' => $account->id()]);
      foreach ($workflows as $flow) {
        if ($flow->containForm($niobi_form)) {
          return AccessResult::allowed();
        }
      }
    }

    return AccessResult::neutral();
  }

  /**
   * Check whether the user can access a niobi_form's webform log.
   *
   * @param string $operation
   *   Operation being performed.
   * @param string $entity_access
   *   Entity access rule that needs to be checked.
   * @param \Drupal\niobi_form\Entity\NiobiFormInterface $niobi_form
   *   A niobi_form.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public static function checkWebformLogAccess($operation, $entity_access, NiobiFormInterface $niobi_form, AccountInterface $account) {
    $access_result = static::checkWebformResultsAccess($operation, $entity_access, $niobi_form, $account);
    if (!$access_result->isAllowed()) {
      return $access_result;
    }

    /** @var \Drupal\webform\WebformEntityReferenceManagerInterface $entity_reference_manager */
    $entity_reference_manager = \Drupal::service('webform.entity_reference_manager');
    $webform = $entity_reference_manager->getWebform($niobi_form);
    if (!$webform->hasSubmissionLog()) {
      $access_result = AccessResult::forbidden();
    }

    return $access_result->addCacheableDependency($webform)->addCacheTags(['config:webform.settings']);
  }

  /**
   * Check whether the user can access a niobi_form's webform.
   *
   * @param string $operation
   *   Operation being performed.
   * @param string $entity_access
   *   Entity access rule that needs to be checked.
   * @param \Drupal\niobi_form\Entity\NiobiFormInterface $niobi_form
   *   A niobi_form.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public static function checkWebformAccess($operation, $entity_access, NiobiFormInterface $niobi_form, AccountInterface $account) {
    return static::checkAccess($operation, $entity_access, $niobi_form, NULL, $account);
  }

  /**
   * Check whether the user can access a niobi_form's webform submission.
   *
   * @param string $operation
   *   Operation being performed.
   * @param string $entity_access
   *   Entity access rule that needs to be checked.
   * @param \Drupal\niobi_form\Entity\NiobiFormInterface $niobi_form
   *   A niobi_form.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   A webform submission.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public static function checkWebformSubmissionAccess($operation, $entity_access, NiobiFormInterface $niobi_form, WebformSubmissionInterface $webform_submission, AccountInterface $account) {
    $access_result = static::checkAccess($operation, $entity_access, $niobi_form, $webform_submission, $account);
    if ($access_result->isForbidden()) {
      return $access_result;
    }

    switch ($operation) {
      case 'webform_submission_edit_all':
        return WebformSubmissionAccess::checkWizardPagesAccess($webform_submission);

      case 'webform_submission_resend':
        return WebformSubmissionAccess::checkResendAccess($webform_submission, $account);

      case 'webform_submission_duplicate':
        /** @var \Drupal\webform\WebformEntityReferenceManagerInterface $entity_reference_manager */
        $entity_reference_manager = \Drupal::service('webform.entity_reference_manager');
        $webform = $entity_reference_manager->getWebform($niobi_form);
        return WebformEntityAccess::checkWebformSettingValue($webform, 'submission_user_duplicate', TRUE);
    }

    return $access_result;
  }

  /**
   * Check whether the user can access a niobi_form's webform and/or submission.
   *
   * @param string $operation
   *   Operation being performed.
   * @param string $entity_access
   *   Entity access rule that needs to be checked.
   * @param \Drupal\niobi_form\Entity\NiobiFormInterface $niobi_form
   *   A niobi_form.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   A webform submission.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected static function checkAccess($operation, $entity_access, NiobiFormInterface $niobi_form, WebformSubmissionInterface $webform_submission = NULL, AccountInterface $account = NULL) {
    /** @var \Drupal\webform\WebformEntityReferenceManagerInterface $entity_reference_manager */
    $entity_reference_manager = \Drupal::service('webform.entity_reference_manager');

    $webform = $entity_reference_manager->getWebform($niobi_form);
    // Check that the $niobi_form has a valid webform reference.
    if (!$webform) {
      return AccessResult::forbidden();
    }

    // Check that the webform submission was created via the niobi form.
    if ($webform_submission && $webform_submission->getSourceEntity() != $niobi_form) {
      return AccessResult::forbidden();
    }

    // Check the $niobi_form operation.
    if ($operation && $niobi_form->access($operation, $account)) {
      return AccessResult::allowed();
    }

    // Check entity access.
    if ($entity_access) {
      // Check entity access for the webform.
      if (strpos($entity_access, 'webform.') === 0
        && $webform->access(str_replace('webform.', '', $entity_access), $account)) {
        return AccessResult::allowed();
      }
      // Check entity access for the webform submission.
      if (strpos($entity_access, 'webform_submission.') === 0
        && $webform_submission->access(str_replace('webform_submission.', '', $entity_access), $account)) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

}
