<?php

namespace Drupal\niobi_form\Plugin\Field\FieldFormatter;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\webform\Plugin\WebformSourceEntityManager;
use Drupal\webform\Plugin\Field\FieldFormatter\WebformEntityReferenceFormatterBase;
use Drupal\webform\WebformMessageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Niobi-modified Plugin implementation of the 'Webform rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "niobi_form_webform_entity_reference_entity_view",
 *   label = @Translation("Niobi Form Webform"),
 *   description = @Translation("Display the referenced webform with default submission data."),
 *   field_types = {
 *     "webform"
 *   }
 * )
 */
class NiobiFormWebformEntityReferenceEntityFormatter extends WebformEntityReferenceFormatterBase {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The webform message manager.
   *
   * @var \Drupal\webform\WebformMessageManagerInterface
   */
  protected $messageManager;

  /**
   * WebformEntityReferenceEntityFormatter constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\webform\WebformMessageManagerInterface $message_manager
   *   The webform message manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, RendererInterface $renderer, ConfigFactoryInterface $config_factory, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager, WebformMessageManagerInterface $message_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $renderer, $config_factory);
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->messageManager = $message_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('renderer'),
      $container->get('config.factory'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('webform.message_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    $settings['source_entity'] = TRUE;
    $settings['edit_class'] = 'button';
    $settings['form_class'] = 'button';
    $settings['no_results_class'] = 'button';
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Set submission source entity: @source_entity', ['@source_entity' => $this->getSetting('source_entity') ? $this->t('Yes') : $this->t('No')]);
    $summary[] = $this->t('Add button class: @no_results_class', ['@no_results_class' => $this->getSetting('no_results_class')]);
    $summary[] = $this->t('Edit webform class: @edit_class', ['@edit_class' => $this->getSetting('edit_class')]);
    $summary[] = $this->t('Form settings class: @form_class', ['@form_class' => $this->getSetting('form_class')]);
    $summary[] = $this->t('After making changes, please click "Save" for the display form, to make changes permanent.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $entity_type_definition = $this->entityTypeManager->getDefinition($this->fieldDefinition->getTargetEntityTypeId());
    $title = $this->t("Use this field's %entity_type entity as the webform submission's source entity.", ['%entity_type' => $entity_type_definition->getLabel()]);
    $description = $this->t("If unchecked, the current page's entity will be used as the webform submission's source entity. For example, if this webform was displayed on a node's page, the current node would be used as the webform submission's source entity.", ['%entity_type' => $entity_type_definition->getLabel()]);

    $form = parent::settingsForm($form, $form_state);
    $form['source_entity'] = [
      '#title' => $title,
      '#description' => $description,
      '#type' => 'checkbox',
      '#return_type' => TRUE,
      '#default_value' => $this->getSetting('source_entity'),
    ];
    $form['no_results_class'] = [
      '#title' => t('Classes for Add Button'),
      '#description' => t('If no form is selected, an add link will be generated with these classes.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('no_results_class'),
    ];
    $form['edit_class'] = [
      '#title' => t('Classes for Edit Button'),
      '#description' => t('If a form is selected, an edit link will be generated with these classes.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('edit_class'),
    ];
    $form['form_class'] = [
      '#title' => t('Classes for Webform Buttons'),
      '#description' => t('If a form is selected, webform settings links will be generated with these classes.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('form_class'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Get items entity, which is the entity that the webform
    // is directly attached to.
    $items_entity = $items->getEntity();

    // Get (main) source entity.
    $source_entity = WebformSourceEntityManager::getMainSourceEntity($items_entity);

    $route = $this->routeMatch->getRouteName();
    $is_node_edit = (preg_match('/\.edit_form$/', $route) || preg_match('/\.content_translation_add$/', $route));

    $elements = [];
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $elements[$delta] = [
        '#type' => 'webform',
        '#webform' => $entity,
        '#default_data' => (!empty($items[$delta]->default_data)) ? Yaml::decode($items[$delta]->default_data) : [],
        '#entity' => ($this->getSetting('source_entity')) ? $source_entity : NULL,
      ];
      $this->setCacheContext($elements[$delta], $entity, $items[$delta]);
    }
    if(empty($elements)) {
      $opts = [
        'attributes' => ['class' => $this->getSetting('no_results_class')],
        'query' => [
          'destination' => \Drupal::service('path.current')->getPath(),
          'niobi_form_attach' => $source_entity->id()
        ]
      ];
      $url = Url::fromRoute('entity.webform.add_form',[],$opts);
      $l = Link::fromTextAndUrl('+ Create Webform', $url);
      $elements[] = $l->toRenderable();
      $instructions = t('No webform selected. If necessary, create a form using the button above, and select it from the "Edit" tab.');
      $elements[] = ['#markup' => $instructions];
    }
    else {
      foreach($elements as $e) {
        $add = '';
        $edit_opts = [
          'attributes' => ['class' => $this->getSetting('edit_class')],
        ];
        $accessManager = \Drupal::service('access_manager');
        if ($accessManager->checkNamedRoute('entity.webform.edit_form', ['webform' => $e['#webform']->id()], \Drupal::currentUser())) {
          $url = Url::fromRoute('entity.webform.edit_form', ['webform' => $e['#webform']->id()], $edit_opts);
          $l = Link::fromTextAndUrl('Edit ' . $e['#webform']->label(), $url);
          $add = $l->toString();
        }

        $form_opts = [
          'attributes' => ['class' => $this->getSetting('form_class')],
        ];
        $webform_links  = [
          'entity.webform.test_form' => t('Test'),
          'entity.webform.results_submissions' => t('Results'),
          'entity.webform.handlers' => t('Email/Handlers'),
          'entity.webform.settings' => t('Form Settings'),
        ];
        foreach ($webform_links as $wl_key => $wl_value) {
          if ($accessManager->checkNamedRoute($wl_key, ['webform' => $e['#webform']->id()], \Drupal::currentUser())) {
            $url = Url::fromRoute($wl_key,['webform' => $e['#webform']->id()], $form_opts);
            $l = Link::fromTextAndUrl($wl_value, $url);
            $add .= '&nbsp;&nbsp;' . $l->toString();
          }
        }
        $elements = array_merge([['#type' => 'markup', '#markup' => $add]], $elements);
      }
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity) {
    // Always allow access so that the Webform element can determine if the
    // Webform is accessible or an access denied message should be displayed.
    // @see \Drupal\webform\Element\Webform
    return AccessResult::allowed();
  }

}
