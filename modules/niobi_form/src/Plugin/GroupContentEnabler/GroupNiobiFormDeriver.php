<?php

namespace Drupal\niobi_form\Plugin\GroupContentEnabler;

use Drupal\niobi_form\Entity\NiobiFormType;
use Drupal\Component\Plugin\Derivative\DeriverBase;

class GroupNiobiFormDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach (NiobiFormType::loadMultiple() as $name => $niobi_form_type) {
      $label = $niobi_form_type->label();
      $this->derivatives[$name] = [
        'entity_bundle' => $name,
        'label' => t('Form (@type)', ['@type' => $label]),
        'description' => t('Adds %type content to groups both publicly and privately.', ['%type' => $label]),
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
