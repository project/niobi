<?php

namespace Drupal\niobi_form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Prevents niobi_form module from being uninstalled whilst any Niobi Forms exist.
 */
class NiobiFormUninstallValidator implements ModuleUninstallValidatorInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a WebformNodeUninstallValidator.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation) {
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    $reasons = [];
    if ($module == 'niobi_form') {
      // The Niobi Form type is provided by the Niobi Form module. Prevent
      // uninstall if there are any niobi_formss of that type.
      if ($this->hasWebformNodes()) {
        $reasons[] = $this->t('To uninstall Niobi Form, delete all content that has the Webform content type.');
      }
    }
    return $reasons;
  }

  /**
   * Determines if there is any Niobi Forms or not.
   *
   * @return bool
   *   TRUE if there are Niobi Forms, FALSE otherwise.
   */
  protected function hasWebformNodes() {
    $niobi_forms = $this->entityTypeManager->getStorage('niobi_form')->getQuery()
      ->condition('type', 'webform')
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->execute();
    return !empty($niobi_forms);
  }

}
