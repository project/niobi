<?php

namespace Drupal\niobi_form\EventSubscriber;

use Drupal\Core\Url;
use Drupal\niobi_form\Entity\NiobiForm;
use Drupal\user\Entity\User;

/**
 * Class NiobiFormSubscriberUtilities
 * @package Drupal\niobi_form\EventSubscriber
 */
class NiobiFormSubscriberUtilities  {
  public static function assignForm($entity, $niobi_form_id) {
    $niobi_form = NiobiForm::load($niobi_form_id);
    $data = [
      'target_id' => $entity->id(),
      'default_data' => '',
      'status' => 'open',
      'open' => '',
      'close' => ''
    ];
    $niobi_form->set('field_form', $data);
    $niobi_form->save();
    \Drupal::messenger()->addMessage(t('Webform attached, please edit the form, and add fields.'));
  }
}