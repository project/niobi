<?php

namespace Drupal\niobi_form\EventSubscriber;

use Drupal\Core\Url;
use Drupal\entity_events\Event\EntityEvent;
use Drupal\entity_events\EventSubscriber\EntityEventDeleteSubscriber;
use Drupal\webform\Entity\Webform;
use Drupal\user\Entity\User;

/**
 * Class NiobiFormAssignFormSubscriber
 * @package Drupal\niobi_form\EventSubscriber
 */
class NiobiFormAssignFormDeleteSubscriber extends EntityEventDeleteSubscriber {

  public function onEntityDelete(EntityEvent $event) {
    $entity = $event->getEntity();
    $niobi_form = \Drupal::request()->get('niobi_form_attach');
    if($entity->getEntityTypeId() === 'niobi_form') {
      $v = $entity->get('field_form')->getValue();
      if (isset($v[0]['target_id'])) {
        $webform = Webform::load($v[0]['target_id']);
        $webform->delete();
      }
    }
  }

}