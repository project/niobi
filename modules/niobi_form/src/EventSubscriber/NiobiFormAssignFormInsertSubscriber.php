<?php

namespace Drupal\niobi_form\EventSubscriber;

use Drupal\Core\Url;
use Drupal\entity_events\Event\EntityEvent;
use Drupal\entity_events\EventSubscriber\EntityEventInsertSubscriber;
use Drupal\user\Entity\User;

/**
 * Class NiobiFormAssignFormSubscriber
 * @package Drupal\niobi_form\EventSubscriber
 */
class NiobiFormAssignFormInsertSubscriber extends EntityEventInsertSubscriber {

  public function onEntityInsert(EntityEvent $event) {
    $entity = $event->getEntity();
    if($entity->getEntityTypeId() === 'webform') {
      $niobi_form = \Drupal::request()->get('niobi_form_attach');
      if ($niobi_form) {
        NiobiFormSubscriberUtilities::assignForm($entity, $niobi_form);
      }
    }
  }
}