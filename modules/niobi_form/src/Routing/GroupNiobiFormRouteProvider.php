<?php

namespace Drupal\niobi_form\Routing;

use Drupal\niobi_form\Entity\NiobiFormType;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for group_niobi_form group content.
 */
class GroupNiobiFormRouteProvider {

  /**
   * Provides the shared collection route for group niobi_form plugins.
   */
  public function getRoutes() {
    $routes = $plugin_ids = $permissions_add = $permissions_create = [];

    foreach (NiobiFormType::loadMultiple() as $name => $niobi_form_type) {
      $plugin_id = "group_niobi_form:$name";

      $plugin_ids[] = $plugin_id;
      $permissions_add[] = "create $plugin_id content";
      $permissions_create[] = "create $plugin_id entity";
    }

    // If there are no niobi_form types yet, we cannot have any plugin IDs and should
    // therefore exit early because we cannot have any routes for them either.
    if (empty($plugin_ids)) {
      return $routes;
    }

    $routes['entity.group_content.group_niobi_form_relate_page'] = new Route('group/{group}/niobi_form/add');
    $routes['entity.group_content.group_niobi_form_relate_page']
      ->setDefaults([
        '_title' => 'Relate Form Content',
        '_controller' => '\Drupal\niobi_form\Controller\GroupNiobiFormController::addPage',
      ])
      ->setRequirement('_group_permission', implode('+', $permissions_add))
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE);

    $routes['entity.group_content.group_niobi_form_add_page'] = new Route('group/{group}/niobi_form/create');
    $routes['entity.group_content.group_niobi_form_add_page']
      ->setDefaults([
        '_title' => 'Create Form Content',
        '_controller' => '\Drupal\niobi_form\Controller\GroupNiobiFormController::addPage',
        'create_mode' => TRUE,
      ])
      ->setRequirement('_group_permission', implode('+', $permissions_create))
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE);

    return $routes;
  }

}
