<?php

namespace Drupal\niobi_form;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for niobi_form.
 */
class NiobiFormTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
