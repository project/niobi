<?php

/**
 * @file
 * Contains niobi_application.page.inc.
 *
 * Page callback for Niobi Application entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Niobi Application templates.
 *
 * Default template: niobi_application.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_niobi_application(array &$variables) {
  // Fetch NiobiApplication Entity Object.
  $niobi_application = $variables['elements']['#niobi_application'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
