<?php

/**
 * @file
 * Views area plugin info for niobi_app.
 */

/**
 * Implements hook_views_data_alter().
 */
function niobi_app_views_data_alter(array &$data) {
  // Area Plugins
  $data['niobi_application']['niobi_app_instructions'] = [
    'title' => t('Niobi Application: Instructions'),
    'help' => t('Render a nominate/apply action with instructions for users with applications.'),
    'area' => ['id' => 'niobi_app_instructions'],
  ];
  $data['niobi_application']['niobi_app_no_results'] = [
    'title' => t('Niobi Application: No Results'),
    'help' => t('Render a nominate/apply action for users with no application.'),
    'area' => ['id' => 'niobi_app_no_results'],
  ];
  $data['views']['niobi_app_workflow_custom_reports'] = [
    'title' => t('Niobi Application: Custom reports list'),
    'help' => t('Render a list of custom reports tied to this workflow'),
    'area' => ['id' => 'niobi_app_workflow_custom_reports'],
  ];

  // Field Plugins
  $data['niobi_application']['niobi_app_application_field_value'] = [
    'title' => t('Niobi Application: Application Field Value'),
    'help' => t('The value of a particular application field.'),
    'field' => ['id' => 'niobi_app_application_field_value'],
  ];
  $data['niobi_application']['niobi_app_status_actions'] = [
    'title' => t('Niobi Application: Status and Actions'),
    'help' => t('Show the current status and actions for application'),
    'field' => ['id' => 'niobi_app_status_actions'],
  ];
  $data['niobi_application']['niobi_app_admin_status_actions'] = [
    'title' => t('Niobi Application: Administrative Status and Actions'),
    'help' => t('Show the current status and admin-enabled actions for application'),
    'field' => ['id' => 'niobi_app_admin_status_actions'],
  ];
  $data['niobi_application']['niobi_app_app_field_for_review'] = [
    'title' => t('Niobi Application: Application Fields for Review Dashboards'),
    'help' => t('Get application field values for use in dashboards.'),
    'field' => ['id' => 'niobi_app_app_field_for_review'],
  ];
  $data['niobi_application']['niobi_app_review_assignments'] = [
    'title' => t('Niobi Application: Review Assignments'),
    'help' => t('List of Review assignments and statuses'),
    'field' => ['id' => 'niobi_app_review_assignments'],
  ];
  $data['niobi_application']['niobi_app_numeric_review_score'] = [
    'title' => t('Niobi Application: Numeric Review Score'),
    'help' => t('Report of total review score for stage'),
    'field' => ['id' => 'niobi_app_numeric_review_score'],
  ];
  $data['niobi_application']['niobi_app_review_field_value'] = [
    'title' => t('Niobi Application: Review Field Value'),
    'help' => t('The value of a particular review field.'),
    'field' => ['id' => 'niobi_app_review_field_value'],
  ];
}
