<?php

/**
 * @file
 * Contains niobi_application_workflow_stage.page.inc.
 *
 * Page callback for Niobi Application Workflow Stage entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Niobi Application Workflow Stage templates.
 *
 * Default template: niobi_application_workflow_stage.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_niobi_application_workflow_stage(array &$variables) {
  // Fetch NiobiApplicationWorkflowStage Entity Object.
  $niobi_application_workflow_stage = $variables['elements']['#niobi_application_workflow_stage'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
