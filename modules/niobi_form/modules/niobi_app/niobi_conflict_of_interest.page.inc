<?php

/**
 * @file
 * Contains niobi_conflict_of_interest.page.inc.
 *
 * Page callback for Conflict of Interest entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Conflict of Interest templates.
 *
 * Default template: niobi_conflict_of_interest.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_niobi_conflict_of_interest(array &$variables) {
  // Fetch NiobiConflictOfInterest Entity Object.
  $niobi_conflict_of_interest = $variables['elements']['#niobi_conflict_of_interest'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
