<?php

/**
 * @file
 * Contains niobi_application_reviewer_pool.page.inc.
 *
 * Page callback for Niobi application reviewer pool entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Niobi application reviewer pool templates.
 *
 * Default template: niobi_application_reviewer_pool.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_niobi_application_reviewer_pool(array &$variables) {
  // Fetch NiobiApplicationReviewerPool Entity Object.
  $niobi_application_reviewer_pool = $variables['elements']['#niobi_application_reviewer_pool'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
