<?php
/**
 * @file
 * Install, update and uninstall functions for the niobi_app module.
 */

use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Database\Database;


/**
 * Implements hook_install().
 *
 */
function niobi_app_install() {
  \Drupal::service('config.installer')->installDefaultConfig('module', 'niobi_app');

  /**
   * Install the custom configs for group & department dashboards
   */
  $group_config = \Drupal::configFactory()->getEditable('core.entity_view_display.group.group.default');
  $group_settings = $group_config->getOriginal('third_party_settings');
  $group_settings['field_group']['group_app_manager'] = array(
    'children' => array(
      'niobi_applications_group_application_manager_entity_view_1',
      'niobi_applications_reviewer_pool_manager_entity_view_1'
    ),
    'label' => 'Application Manager',
    'parent_name' => 'group_group_tabs',
    'region' => 'content',
    'weight' => '22',
    'format_type' => 'tab',
    'format_settings' => array(
      'id' => '',
      'classes' => '',
      'formatter' => 'closed',
      'description' => '',
    ),
  );
  $group_settings['field_group']['group_group_tabs']['children'][] = 'group_app_manager';
  $group_config->set('third_party_settings', $group_settings)->save();

  /**
   * Install Single Form config on group dashboard
   */
  $group_storage_config = \Drupal::configFactory()->getEditable('views.view.niobi_form_forms_in_group');
  $group_settings = $group_storage_config->getOriginal('display');
  $group_settings['default']['display_options']['header']['views_add_button_application_form'] = array(
    'id' => 'views_add_button',
    'table' => 'views',
    'field' => 'views_add_button',
    'relationship' => 'none',
    'group_type' => 'group',
    'admin_label' => '',
    'empty' => TRUE,
    'tokenize' => TRUE,
    'type' => 'group_content+group_content_type_c99dc6e408c08',
    'context' => '{{ raw_arguments.gid }}',
    'button_text' => '+ Add Application-Enabled Form',
    'button_classes' => 'btn btn-success button',
    'button_attributes' => '',
    'button_prefix' => [
      'value' => '',
      'format' => 'basic_html',
    ],
    'button_suffix' => [
      'value' => '&nbsp;&nbsp;',
      'format' => 'basic_html'
    ],
    'query_string' => '',
    'destination' => TRUE,
    'plugin_id' => 'views_add_button_area',
  );
  $group_storage_config->set('display', $group_settings)->save();

  /**
   * Install argument parameter for message view.
   */
//  $view_config = \Drupal::configFactory()->getEditable('views.view.flashpoint_message_my_messages');
//  $view_settings = $view_config->getOriginal('display');
//  $view_settings['default']['display_options']['arguments']['field_group_target_id'] = [
//    'id' => 'field_group_target_id',
//    'table' => 'message__field_group',
//    'field' => 'field_group_target_id',
//    'relationship' => 'none',
//    'group_type' => 'group',
//    'admin_label' => '',
//    'default_action' => 'default',
//    'exception' => [
//      'value' => 'all',
//      'title_enable' => FALSE,
//      'title' => 'All',
//    ],
//    'title_enable' => FALSE,
//    'title' => '',
//    'default_argument_type' => 'current_user_flashpoint_communities',
//    'default_argument_options' => [],
//    'default_argument_skip_url' => FALSE,
//    'summary_options' => [
//      'base_path' => '',
//      'count' => TRUE,
//      'items_per_page' => 25,
//      'override' => FALSE,
//    ],
//    'summary' => [
//      'sort_order' => 'asc',
//      'number_of_records' => 0,
//      'format' => 'default_summary',
//    ],
//    'specify_validation' => FALSE,
//    'validate' => [
//      'type' => 'none',
//      'fail' => 'not found',
//    ],
//    'validate_options' => [],
//    'break_phrase' => TRUE,
//    'not' => FALSE,
//    'plugin_id' => 'numeric',
//  ];
//  $view_config->setData($view_settings)->save();

}

/**
 * Implements hook_uninstall().
 */
function niobi_app_uninstall() {
  /**
   * Uninstall custom configs for group dashboard
   */
  $group_config = \Drupal::configFactory()->getEditable('core.entity_view_display.group.group.default');
  $group_settings = $group_config->getOriginal('third_party_settings');
  unset($group_settings['field_group']['group_app_manager']);
  $group_settings['field_group']['group_group_tabs']['children'] = array_diff($group_settings['field_group']['group_group_tabs']['children'], ['group_app_manager']);
  $group_config->set('third_party_settings', $group_settings)->save();

  $group_storage_config = \Drupal::configFactory()->getEditable('views.view.niobi_form_forms_in_group');
  $group_settings = $group_storage_config->getOriginal('display');
  if (isset($group_settings['default']['display_options']['header']['views_add_button_application_form'])) {
    unset($group_settings['default']['display_options']['header']['views_add_button_application_form']);
    $group_storage_config->set('display', $group_settings)->save();
  }

  \Drupal::service('config.manager')->uninstall('module', 'niobi_app');
}

/**
 * Add Conflict of Interest Entities
 */
function niobi_app_update_8101(&$sandbox) {
  $entity_manager = \Drupal::entityTypeManager();
  $update_manager = \Drupal::entityDefinitionUpdateManager();
  $definitions = $entity_manager->getDefinitions();
  $update_manager->installEntityType($definitions['niobi_conflict_of_interest']);
  $update_manager->installEntityType($definitions['niobi_conflict_of_interest_type']);
}

/**
 * Extend the name field to 511 characters
 */
function niobi_app_update_8102(&$sandbox) {
  $spec = array(
    'type' => 'varchar',
    'description' => "Name",
    'length' => 511,
    'not null' => FALSE,
  );
  $schema = Database::getConnection()->schema();
  $schema->changeField('niobi_application_field_data', 'name', 'name', $spec) ;
  $schema->changeField('niobi_application_workflow_field_data', 'name', 'name', $spec) ;
  $schema->changeField('niobi_application_workflow_stage_field_data', 'name', 'name', $spec) ;
  $schema->changeField('niobi_application_reviewer_pool_field_data', 'name', 'name', $spec) ;
  $schema->changeField('niobi_conflict_of_interest_field_data', 'name', 'name', $spec) ;
}