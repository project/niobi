<?php

namespace Drupal\niobi_app;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Niobi application reviewer pool entities.
 *
 * @ingroup niobi_app
 */
class NiobiApplicationReviewerPoolListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Niobi application reviewer pool ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\niobi_app\Entity\NiobiApplicationReviewerPool */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.niobi_application_reviewer_pool.edit_form',
      ['niobi_application_reviewer_pool' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
