<?php

namespace Drupal\niobi_app;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for niobi_application_workflow_stage.
 */
class NiobiApplicationWorkflowStageTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
