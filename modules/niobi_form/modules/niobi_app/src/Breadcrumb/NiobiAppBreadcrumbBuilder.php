<?php


namespace Drupal\niobi_app\Breadcrumb;


use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupContent;
use Drupal\niobi_app\Entity\NiobiApplication;
use Symfony\Component\HttpFoundation\Request;


class NiobiAppBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $params = $route_match->getParameters()->all();

    // If we are on an application workflow page, we are making a breadcrumb.
    if (isset($params['niobi_application_workflow']) && !empty($params['niobi_application_workflow'])) {
      $niobi_application_workflow = \Drupal::routeMatch()->getParameter('niobi_application_workflow');
      return is_object($niobi_application_workflow);
    }

    if (isset($params['niobi_application']) && !empty($params['niobi_application'])) {
      return is_object($params['niobi_application']);
    }

    if (isset($params['niobi_form']) && !empty($params['niobi_form'])) {
      $applicationUid =  \Drupal::request()->query->get('application_id');
      if (!empty($applicationUid)) {
        $application = current(\Drupal::entityTypeManager()
          ->getStorage('niobi_application')
          ->loadByProperties(['uuid' => $applicationUid]));
        return !empty($application);
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $url = Url::fromRoute('<front>');
    $breadcrumb->addLink(Link::fromTextAndUrl('Home', $url));

    $applicationUid =  \Drupal::request()->query->get('application_id');
    if (!empty($applicationUid)) {
      $niobi_application = current(\Drupal::entityTypeManager()
        ->getStorage('niobi_application')->loadByProperties(['uuid' => $applicationUid]));
    }

    if (empty($niobi_application)) {
      /* @var $niobi_application \Drupal\niobi_app\Entity\NiobiApplication */
      $niobi_application = \Drupal::routeMatch()
        ->getParameter('niobi_application');
    }

    if (!empty($niobi_application)) {
      $niobi_application_workflow = current($niobi_application->field_application_workflow->referencedEntities());
    }

    if (!isset($niobi_application_workflow)) {
      /* @var $niobi_application_workflow \Drupal\niobi_app\Entity\NiobiApplicationWorkflow */
      $niobi_application_workflow = \Drupal::routeMatch()
        ->getParameter('niobi_application_workflow');
    }

    if (!empty($niobi_application_workflow)) {
      $gcs = GroupContent::loadByEntity($niobi_application_workflow);
    }

    if (!empty($gcs)) {
      $gc = array_shift($gcs);
      /* @var $gc \Drupal\group\Entity\GroupContent */
      $group = $gc->getGroup();
      /* @var $group \Drupal\group\Entity\Group */
      // Group members see the group. Other users see the root application page.
      $current_user = \Drupal::currentUser();
      $membership = $group->getMember($current_user);
      if ($membership) {
        $link = $group->toLink($group->label(), 'canonical');
        $breadcrumb->addLink($link);
        // Add workflow if we are on a report page.
        if($route_match->getRouteName() !== 'entity.niobi_application_workflow.canonical') {
          $workflow_link = $niobi_application_workflow->toLink($niobi_application_workflow->label(), 'canonical');
          $breadcrumb->addLink($workflow_link);
        }
      }
      else {
        $app_link = Link::createFromRoute('Applications', 'view.niobi_application_open_applications.page_1');
        $breadcrumb->addLink($app_link);
        // Add workflow if we are on a report page.
        if($route_match->getRouteName() !== 'entity.niobi_application_workflow.canonical') {
          $workflow_link = $niobi_application_workflow->toLink($niobi_application_workflow->label(), 'canonical');
          $breadcrumb->addLink($workflow_link);
        }
      }

      $breadcrumb->addCacheContexts(['group_membership.audience', 'route']);
      return $breadcrumb;
    }
    else {
      $breadcrumb->addCacheContexts(['route']);
      return $breadcrumb;
    }
  }
}
