<?php

namespace Drupal\niobi_app\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides routes for group_niobi_resource group content.
 */
class NiobiAppRouteSubscriber extends RouteSubscriberBase {

  /**
   * @param RouteCollection $collection
   * @return array
   */
  public function alterRoutes(RouteCollection $collection) {
    $webform_access_routes = ['entity.webform.results_submissions', 'entity.webform.test_form'];
    foreach ($webform_access_routes as $wa_route) {
      $route = $collection->get($wa_route);
      $reqs = $route->getRequirements();
      $reqs['_niobi_app_view_access'] = 'TRUE';
      $route->setRequirements($reqs);
//      $collection->add($wa_route, $route);

//      if ($wa_route == 'entity.webform.results_submissions') {
//        $route->setRequirement('_custom_access',
//          '\Drupal\niobi_app\Access\NiobiAppViewAccess:checkResultsAccess');
//      }
    }
  }
}
