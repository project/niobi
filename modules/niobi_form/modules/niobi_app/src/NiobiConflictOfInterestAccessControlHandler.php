<?php

namespace Drupal\niobi_app;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Conflict of Interest entity.
 *
 * @see \Drupal\niobi_app\Entity\NiobiConflictOfInterest.
 */
class NiobiConflictOfInterestAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\niobi_app\Entity\NiobiConflictOfInterestInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished conflict of interest entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published conflict of interest entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit conflict of interest entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete conflict of interest entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add conflict of interest entities');
  }

}
