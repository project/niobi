<?php
/**
 * @file
 * Provides Drupal\niobi_app\NiobiAppDecisionInterface;
 */
namespace Drupal\niobi_app;
/**
 * An interface for all NiobiAppDecisionInterface type plugins.
 */
interface NiobiAppDecisionInterface {
  /**
   * Provide a description of the plugin.
   * @return string
   *   A string description of the plugin.
   */
  public function description();
}