<?php

namespace Drupal\niobi_app;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Niobi Application Workflow Stage entity.
 *
 * @see \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage.
 */
class NiobiApplicationWorkflowStageAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStageInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          $accessResult = AccessResult::allowedIfHasPermission($account, 'view unpublished niobi application workflow stage entities');
        } else {
          $accessResult = AccessResult::allowedIfHasPermission($account, 'view published niobi application workflow stage entities');
        }
        break;
      case 'update':
        $accessResult = AccessResult::allowedIfHasPermission($account, 'edit niobi application workflow stage entities');
        break;
      case 'delete':
        $accessResult = AccessResult::allowedIfHasPermission($account, 'delete niobi application workflow stage entities');
    }

    if (isset($accessResult) && $accessResult->isAllowed()) {
      $workflow = current($entity->field_parent_workflow->referencedEntities());
      if (!empty($workflow)) {
        if ($operation !== 'delete') {
          return AccessResult::allowedIf($workflow->access($operation, $account));
        } else {
          // allow admin team to delete workflow stage
          if ($workflow->isOnAdminTeam()) {
            return AccessResult::allowed();
          }
        }
      }
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add niobi application workflow stage entities');
  }

}
