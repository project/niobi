<?php

namespace Drupal\niobi_app;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Conflict of Interest entities.
 *
 * @ingroup niobi_app
 */
class NiobiConflictOfInterestListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Conflict of Interest ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\niobi_app\Entity\NiobiConflictOfInterest $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.niobi_conflict_of_interest.edit_form',
      ['niobi_conflict_of_interest' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
