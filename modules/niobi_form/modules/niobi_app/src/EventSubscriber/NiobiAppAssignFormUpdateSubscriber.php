<?php

namespace Drupal\niobi_app\EventSubscriber;

use Drupal\Core\Url;
use Drupal\entity_events\Event\EntityEvent;
use Drupal\entity_events\EventSubscriber\EntityEventUpdateSubscriber;
use Drupal\niobi_app\Entity\NiobiApplication;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Class NiobiAppAssignFormUpdateSubscriber
 * @package Drupal\niobi_app\EventSubscriber
 */
class NiobiAppAssignFormUpdateSubscriber extends EntityEventUpdateSubscriber {

  public function onEntityUpdate(EntityEvent $event) {
    $entity = $event->getEntity();
    if($entity->getEntityTypeId() === 'webform_submission') {
      NiobiAppAssignBase::handleWebformSubmission($entity);
    }
  }
}