<?php

namespace Drupal\niobi_app\EventSubscriber;

use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\niobi_app\Utilities\NiobiAppPromoteUtilities;

/**
 * Class NiobiAppAssignFormInsertSubscriber
 * @package Drupal\niobi_app\EventSubscriber
 */
class NiobiAppAssignBase {

  public static function handleWebformSubmission($entity) {
    $uuid = \Drupal::request()->request->get('application_id');
    // Use webform element if we don't have the query parameter.
    if (empty($uuid)) {
      /* @var $entity \Drupal\webform\Entity\WebformSubmission */
      $uuid = $entity->getElementData('application_id');
    }
    // If there is no element, assume we are outside the scope of this module.
    if (is_string($uuid) && (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $uuid) === 1)) {
      $s = \Drupal::entityTypeManager()->getStorage('niobi_application')->loadByProperties(['uuid' => $uuid]);
      if (!empty($s)) {
        $id = array_keys($s)[0];
        $application = NiobiApplication::load($id);
        $status = $application->get('field_application_status')->getValue();
        if (isset($status[0]['value'])){
          $status = $status[0]['value'];
          switch($status) {
            case 'nomination':
            case 'nomination_claim':
              $nomination_vals = $application->get('field_nomination_submissions')->getValue();
              $set = [$entity->id()];
              foreach ($nomination_vals as $val) {
                if (!in_array($val['target_id'], $set)) {
                  $set[] = $val['target_id'];
                }
              }
              $application->set('field_nomination_submissions', $set);
              $application->set('field_nominator', \Drupal::currentUser()->id());
              $complete_status = !$entity->isDraft();
              if ($complete_status) {
                $advance = $application->getCurrentApplicationStage()->field_nomination_claim_advance->value;
                if ($advance) {
                  $application->set('field_application_status', 'nomination_claim');
                }
              }
              $application->save();
              break;
            case 'application':
              $application_vals = $application->get('field_application_submissions')->getValue();
              $set = [$entity->id()];
              foreach ($application_vals as $val) {
                $set[] = $val['target_id'];
              }
              $application->set('field_application_submissions', $set);
              $application->save();
              if (NiobiAppPromoteUtilities::applicationReadyToSubmit($application)) {
                $stage = $application->getCurrentApplicationStage();
                $auto_submit = $stage->get('field_auto_submit')->getValue();
                $auto_submit = isset($auto_submit[0]['value']) ? $auto_submit[0]['value'] : FALSE;
                if($auto_submit) {
                  NiobiAppPromoteUtilities::submitApplication($application);
                }
              }
              break;
            case 'review':
              $review_vals = $application->get('field_review_submissions')->getValue();
              $set = [$entity->id()];
              foreach ($review_vals as $val) {
                $set[] = $val['target_id'];
              }
              $application->set('field_review_submissions', $set);
              $application->save();
              break;
          }
        }
      }
    }
  }
}
