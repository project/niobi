<?php

namespace Drupal\niobi_app\EventSubscriber;

use Drupal\Core\Url;
use Drupal\entity_events\Event\EntityEvent;
use Drupal\entity_events\EventSubscriber\EntityEventInsertSubscriber;
use Drupal\niobi_app\Entity\NiobiApplication;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Class NiobiAppAssignFormInsertSubscriber
 * @package Drupal\niobi_app\EventSubscriber
 */
class NiobiAppAssignFormInsertSubscriber extends EntityEventInsertSubscriber {

  public function onEntityInsert(EntityEvent $event) {
    $entity = $event->getEntity();
    if($entity->getEntityTypeId() === 'webform_submission') {
      NiobiAppAssignBase::handleWebformSubmission($entity);
    }
  }
}