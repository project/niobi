<?php

namespace Drupal\niobi_app;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for niobi_conflict_of_interest.
 */
class NiobiConflictOfInterestTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
