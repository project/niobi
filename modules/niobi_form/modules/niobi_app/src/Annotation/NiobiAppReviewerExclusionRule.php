<?php

namespace Drupal\niobi_app\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a NiobiAppReviewerExclusionRule annotation object.
 *
 * Plugin Namespace: Plugin\niobi_app
 *
 * @see plugin_api
 *
 * @Annotation
 */
class NiobiAppReviewerExclusionRule extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the NiobiAppReviewerExclusionRule.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * The category under which the NiobiAppReviewerExclusionRule should be listed in the UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $category;

}