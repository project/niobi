<?php

namespace Drupal\niobi_app\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a NiobiAppDecision annotation object.
 *
 * Plugin Namespace: Plugin\niobi_app
 *
 * @see plugin_api
 *
 * @Annotation
 */
class NiobiAppDecision extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the NiobiAppDecision.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * The category under which the NiobiAppDecision should be listed in the UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $category;

}