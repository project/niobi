<?php

namespace Drupal\niobi_app;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Niobi application reviewer pool entity.
 *
 * @see \Drupal\niobi_app\Entity\NiobiApplicationReviewerPool.
 */
class NiobiApplicationReviewerPoolAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\niobi_app\Entity\NiobiApplicationReviewerPoolInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished niobi application reviewer pool entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published niobi application reviewer pool entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit niobi application reviewer pool entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete niobi application reviewer pool entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add niobi application reviewer pool entities');
  }

}
