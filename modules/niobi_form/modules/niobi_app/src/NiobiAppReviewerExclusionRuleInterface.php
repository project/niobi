<?php
/**
 * @file
 * Provides Drupal\niobi_app\NiobiAppReviewerExclusionRuleInterface;
 */
namespace Drupal\niobi_app;
/**
 * Interface NiobiAppReviewerExclusionRuleInterface
 * @package Drupal\niobi_app
 */
interface NiobiAppReviewerExclusionRuleInterface {
  /**
   * Provide a description of the plugin.
   * @return string
   *   A string description of the plugin.
   */
  public function description();
}