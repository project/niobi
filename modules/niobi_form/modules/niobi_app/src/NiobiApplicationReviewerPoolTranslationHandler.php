<?php

namespace Drupal\niobi_app;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for niobi_application_reviewer_pool.
 */
class NiobiApplicationReviewerPoolTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
