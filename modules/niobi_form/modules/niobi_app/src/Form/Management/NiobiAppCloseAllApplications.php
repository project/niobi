<?php

namespace Drupal\niobi_app\Form\Management;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;
use Drupal\niobi_form\Entity\NiobiForm;

/**
 * Form controller for Niobi Application launch checklists.
 *
 * @ingroup niobi_app
 */
class NiobiAppCloseAllApplications extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'niobi_app_webform_access';
  }

  /**
   * @param NiobiForm $niobi_form
   * @return array
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  private function getOpenApplications(NiobiApplicationWorkflow $niobi_application_workflow) {
    $query = \Drupal::entityQuery('niobi_application');
    $query->condition('field_application_workflow', $niobi_application_workflow->id());
    $query->condition('field_application_status', 'complete', '<>');
    $result = $query->execute();
    return NiobiApplication::loadMultiple($result);
  }

  /**
   * @return array
   */
  private function printHeader() {
    $ret = [];
    $ret['title-line'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => t('About this page'),
    ];
    $ret['header-description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => t('By clicking "submit," you will close all active applications.
      This is normally performed after testing, or at the end of an application period.'),
    ];

    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NiobiApplicationWorkflow $niobi_application_workflow = NULL) {
    if ($niobi_application_workflow) {
      $form['application_workflow'] = [
        '#type' => 'hidden',
        '#value' => $niobi_application_workflow->id()
      ];

      $form['header'] = $this->printHeader();

      $form['list'] = [
        '#type' => 'item_list',
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#title' => t('Applications to be closed'),
        '#items' => [],
      ];
      foreach($this->getOpenApplications($niobi_application_workflow) as $app) {
        $form['list']['#items'][] = $app->label();
      }

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit')
      ];
    }
    return $form;
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $workflow_id = $form_state->getValue('application_workflow');
    $workflow = NiobiApplicationWorkflow::load($workflow_id);

    $applications = $this->getOpenApplications($workflow);
    foreach ($applications as $app) {
      $app->set('field_application_status', 'complete')->save();
    }

    \Drupal::messenger()->addMessage('The applications have been closed.');
  }

}
