<?php

namespace Drupal\niobi_app\Form\Management;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;
use Drupal\niobi_form\Entity\NiobiForm;
use Drupal\task\Entity\Task;

/**
 * Form controller for Niobi Application launch checklists.
 *
 * @ingroup niobi_app
 */
class NiobiAppCloseAllReviewAssignments extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'niobi_app_webform_access';
  }

  /**
   * @param NiobiForm $niobi_form
   * @return array
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  private function getOpenAssignments(NiobiApplicationWorkflow $niobi_application_workflow) {
    $query = \Drupal::entityQuery('task');
    $query->condition('field_application.entity.field_application_workflow', $niobi_application_workflow->id());
    $query->condition('status', 'closed', '<>');
    $result = $query->execute();
    return Task::loadMultiple($result);
  }

  /**
   * @return array
   */
  private function printHeader() {
    $ret = [];
    $ret['title-line'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => t('About this page'),
    ];
    $ret['header-description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => t('By clicking "submit," you will close all active review assignments.
      This is normally performed at the end of an application period.'),
    ];

    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NiobiApplicationWorkflow $niobi_application_workflow = NULL) {
    if ($niobi_application_workflow) {
      $form['application_workflow'] = [
        '#type' => 'hidden',
        '#value' => $niobi_application_workflow->id()
      ];

      $form['header'] = $this->printHeader();

      $form['list'] = [
        '#type' => 'item_list',
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#title' => t('Assignments to be closed'),
        '#items' => [],
      ];
      foreach($this->getOpenAssignments($niobi_application_workflow) as $assignment) {
        $form['list']['#items'][] = $assignment->label();
      }

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit')
      ];
    }
    return $form;
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $workflow_id = $form_state->getValue('application_workflow');
    $workflow = NiobiApplicationWorkflow::load($workflow_id);

    $assignments = $this->getOpenAssignments($workflow);
    foreach ($assignments as $assignment) {
      $assignment->set('status', 'closed')->save();
      $assignment->set('close_type', $this->t('closed by admin'))->save();
    }

    \Drupal::messenger()->addMessage('The assignments have been closed.');
  }

}
