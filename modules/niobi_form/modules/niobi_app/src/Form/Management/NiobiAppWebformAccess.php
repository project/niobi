<?php

namespace Drupal\niobi_app\Form\Management;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;
use Drupal\niobi_app\Utilities\NiobiAppAccessUtilities;

/**
 * Form controller for Niobi Application launch checklists.
 *
 * @ingroup niobi_app
 */
class NiobiAppWebformAccess extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'niobi_app_webform_access';
  }

//  /**
//   * @param NiobiForm $niobi_form
//   * @return array
//   * @throws \Drupal\Core\Entity\EntityMalformedException
//   */
//  private function printFormStatus(NiobiForm $niobi_form) {
//    $webform = $niobi_form->getWebform();
//    if (!empty($webform)) {
//      $ret = [];
//      /* @var $webform \Drupal\webform\Entity\Webform */
//      $handlers = $webform->getHandlers();
//      foreach($handlers->getIterator()->getArrayCopy() as $key => $item) {
//        /* @var $item \Drupal\webform\Plugin\WebformHandler\EmailWebformHandler */
//        if (is_object($item) && method_exists($item, 'getEmailConfiguration')) {
//          $item_config = $item->getEmailConfiguration();
//          $ret[] = [
//            '#type' => 'html_tag',
//            '#tag' => 'h4',
//            '#value' => '',
//            'em' => [
//              '#type' => 'html_tag',
//              '#tag' => 'em',
//              '#value' => $item->getLabel(),
//            ]
//          ];
//          $data_to_get = [
//            'from_mail' => t('From'),
//            'from_name' => t('From Name'),
//            'to_mail' => t('To'),
//            'cc_mail' => t('Cc'),
//            'bcc_mail' => t('Bcc'),
//            'subject' => t('Subject'),
//            'body' => t('Body'),
//          ];
//          foreach($data_to_get as $k => $val) {
//            $ret[] = [
//              '#type' => 'html_tag',
//              '#tag' => 'p',
//              '#value' => '',
//              'strong' => [
//                '#type' => 'html_tag',
//                '#tag' => 'strong',
//                '#value' => $val,
//              ]
//            ];
//            $ret[] = [
//              '#type' => 'html_tag',
//              '#tag' => 'p',
//              '#value' => $item_config[$k] ? $item_config[$k] : t('None'),
//            ];
//          }
//          $ret[] = [
//            '#type' => 'html_tag',
//            '#tag' => 'hr',
//            '#value' => '',
//          ];
//        }
//      }
//      if (empty($ret)) {
//        $ret[] = [
//          '#type' => 'html_tag',
//          '#tag' => 'p',
//          '#value' => 'No email handlers found.',
//        ];
//      }
//      return $ret;
//    }
//    else {
//      return [
//        '#type' => 'html_tag',
//        '#tag' => 'p',
//        '#value' => t('No form was found.'),
//      ];
//    }
//  }

  /**
   * @return array
   */
  private function printHeader() {
    $ret = [];
    $ret['title-line'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => t('About this report'),
    ];
    $ret['header-description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => t('This report contains both current statuses and controls for webform submission access.
      You may need to come here if you need to grant file download access for reviewers, for example.'),
    ];

    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NiobiApplicationWorkflow $niobi_application_workflow = NULL) {
    if ($niobi_application_workflow) {
      $form['application_workflow'] = [
        '#type' => 'hidden',
        '#value' => $niobi_application_workflow->id()
      ];
      $stages = $niobi_application_workflow->getStages();

      $form['header'] = $this->printHeader();
      foreach ($stages as $stage) {
        /* @var $stage \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage */
        $stage_id = 'stage_' . $stage->id();
        $stage_options_id = $stage_id . '_options';
        $pools = $stage->getReviewerPools(TRUE);
        $form[$stage_id] = [
          '#type' => 'fieldset',
          '#title' => $stage->label(),
        ];
        $form[$stage_id][$stage_options_id] = [
          '#type' => 'checkboxes',
          '#options' => [
            'owner_access' => t('Grant submission access on all forms to workflow owner.'),
            'admin_access' => t('Grant submission access on all forms to workflow admin team.'),
            'view_access' => t('Grant submission access on all forms to workflow view access team.'),
          ],
        ];
        foreach ($pools as $pool) {
          $pool_id = $stage_options_id . '_pool_' . $pool->id();
          $msg = t('Grant submission access on all forms to the reviewer pool %pool', ['%pool' => $pool->label()]);
          $form[$stage_id][$stage_id . '_options']['#options'][$pool_id] = $msg;
        }
      }
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Submit')
      ];
    }
    return $form;
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $workflow_id = $form_state->getValue('application_workflow');
    $workflow = NiobiApplicationWorkflow::load($workflow_id);
    $workflow_leaders = NiobiAppAccessUtilities::getAdminTeams($workflow, TRUE);
    $stages = $workflow->getStages();
    foreach ($stages as $stage) {
      /* @var $stage \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage */
      $stage_id = 'stage_' . $stage->id();
      $stage_options_id = $stage_id . '_options';
      $options = $form_state->getValue($stage_options_id);
      $pools = NiobiAppAccessUtilities::getReviewersForStage($stage, TRUE, TRUE);

      // Build list of people
      $add_list = [];
      if ($options['owner_access']) {
        $add_list[] = $workflow_leaders['owner'];
      }
      if ($options['admin_access']) {
        foreach ($workflow_leaders['admin'] as $a) {
          !in_array($a, $add_list) ? $add_list[] = $a : NULL;
        }
      }
      if ($options['view_access']) {
        $add = $workflow->getViewAccessTeam();
        foreach ($add as $a) {
          !in_array($a, $add_list) ? $add_list[] = $a : NULL;
        }
      }

      foreach ($pools as $pool_id => $reviewers) {
        $pool_id = $stage_options_id . '_pool_' . $pool_id;
        if ($options[$pool_id]) {
          foreach ($reviewers as $r) {
            !in_array($r, $add_list) ? $add_list[] = $r : NULL;
          }
        }
      }

      NiobiAppAccessUtilities::updateWebformAccess($add_list, [$stage]);
    }
    \Drupal::messenger()->addMessage('Settings applied. You may now check each form to confirm access.');
  }



}
