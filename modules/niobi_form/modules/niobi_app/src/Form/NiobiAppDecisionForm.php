<?php

namespace Drupal\niobi_app\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\Entity\NiobiApplication;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form controller for Niobi Application edit forms.
 *
 * @ingroup niobi_app
 */
class NiobiAppDecisionForm extends FormBase {

  public function getFormId() {
    return 'niobi_app_decision_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NiobiApplication $niobi_application = NULL, $decision_plugin = NULL) {

    if ($niobi_application) {

      $decision_plugin = $decision_plugin ? $decision_plugin : 'close_application';
      // Add common options
      $form['application_id'] = [
        '#type' => 'hidden',
        '#value' => $niobi_application->id(),
      ];
      $form['application'] = [
        '#type' => 'markup',
      ];

      $app_form = \Drupal::formBuilder()->getForm(NiobiApplicationSubmissions::class, $niobi_application->id());
      $renderer = \Drupal::service('renderer');
      // Get the tabs string and set as the element markup.
      $form['application']['#markup'] = $renderer->render($app_form)->__toString();
      $form['application']['#weight'] = 0;

      // Alter with options from the chosen plugin.
      $plugin_manager = \Drupal::service('plugin.manager.niobi_app_decision');
      $plugin_definitions = $plugin_manager->getDefinitions();

      foreach ($plugin_definitions as $pd) {
        if (!empty($pd['id']) && $pd['id'] === $decision_plugin) {
          $form = $pd['class']::alterDecisionForm($form, $form_state, $niobi_application);
          $form['decision_type'] = [
            '#type' => 'hidden',
            '#value' => $pd['id'],
          ];
          $form['decision_type_label'] = [
            '#type' => 'markup',
            '#markup' => '<h2>' . t('Application Decision: %decision', ['%decision' => $pd['label']]) . '</h2>',
            '#weight' => -100,
          ];
          $form['confirm'] = [
            '#type' => 'markup',
            '#markup' => '<h4>' . t('You are submitting the following decision for this application: %decision', ['%decision' => $pd['label']]) .'</h4>',
            '#weight' => 99,
          ];
        }
      }
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
      '#weight' => 100,
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($decision_plugin = $form_state->getValue('decision_type')) {
      $plugin_manager = \Drupal::service('plugin.manager.niobi_app_decision');
      $plugin_definitions = $plugin_manager->getDefinitions();
      $application_id = $form_state->getValue('application_id');
      $application = NiobiApplication::load($application_id);

      foreach ($plugin_definitions as $pd) {
        if (!empty($pd['id']) && $pd['id'] === $decision_plugin) {
          $pd['class']::processDecision($form_state, $application);
        }
      }
    }
    // Since we don't have an action plugin, we just close the application.
    else {
      $application_id = $form_state->getValue('application_id');
      $application = NiobiApplication::load($application_id);
      $application->set('field_decision', $form_state->getValue('decision'));
      $application->set('field_decision_notes', $form_state->getValue('decision_notes'));
      $application->set('field_decision_notes', $form_state->getValue('decision_notes'));
      $application->set('field_application_status', 'complete');
      $application->save();

      $url = $application->toUrl('canonical');
      $response = new RedirectResponse($url->toString());
      $request = \Drupal::request();

      // Save the session so things like messages get saved.
      $request->getSession()->save();
      $response->prepare($request);

      return $response;
    }
  }

}
