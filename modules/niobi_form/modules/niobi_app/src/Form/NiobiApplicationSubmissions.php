<?php

namespace Drupal\niobi_app\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\Entity\NiobiApplication;

/**
 * Renders all application submissions in a form.
 *
 * @ingroup niobi_app
 */
class NiobiApplicationSubmissions extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'niobi_application_submissions';
  }

  public static function buildRenderArray($application_id) {
    $form = [];
    $application = NiobiApplication::load($application_id);

    if ($application) {
      $workflow = $application->getApplicationWorkflow();
      $admin = $application->isApplicationAdmin();
      $applicant = $application->isApplicant();
      $nominator = $application->isNominator();
      $reviewer = $application->isReviewer();
      $isOnWorkflowViewAccessTeam = $workflow->isOnViewAccessTeam();

      $webformSubmissionViewBuilder = \Drupal::entityTypeManager()->getViewBuilder('webform_submission');

      $a_vals = $workflow->get('field_data_access_options')->getValue();
      $access_vals = [];
      foreach ($a_vals as $val) {
        $access_vals[] = $val['value'];
      }

      // Is the person qualified to view anything?
      if (in_array(TRUE, [$admin, $applicant, $nominator, $reviewer, $isOnWorkflowViewAccessTeam])) {
        $form['niobi_app_submissions'] = [
          '#type' => 'vertical_tabs',
          '#title' => '<h4>Application to review</h4>'
        ];
        // Show Nominations
        if (in_array(TRUE, [$admin, $nominator, $reviewer, $isOnWorkflowViewAccessTeam])) {
          $nominations = $application->getNominationSubmissions();
          if ($nominations) {
            $form['nominations'] = [
              '#type' => 'details',
              '#group' => 'niobi_app_submissions',
              '#title' => 'Nominations',
              '#open' => TRUE,
            ];
            /* @var $nomination \Drupal\webform\Entity\WebformSubmission */
            foreach ($nominations as $nomination) {
              if ($nomination && $nomination->id()) {
                $webform = $nomination->getWebform();
                $form['nominations']['nomination_' . $application->id()] = [
                  '#type' => 'details',
                  '#open' => FALSE,
                  '#title' => t('Nomination Submission for %webform (Click to Open)', ['%webform' => $webform->label()]),
                  'sub' => $webformSubmissionViewBuilder->view($nomination),
                ];
              }
            }
          }
        }
        // Show Applications
        if (in_array(TRUE, [$admin, $applicant, $reviewer, $isOnWorkflowViewAccessTeam])) {
          $applications = $application->getApplicationSubmissions();
          if ($applications) {
            $form['applications'] = [
              '#type' => 'details',
              '#group' => 'niobi_app_submissions',
              '#title' => 'Application Submissions',
              '#open' => TRUE,
            ];
            /* @var $app \Drupal\webform\Entity\WebformSubmission */
            foreach ($applications as $app) {
              if ($app && $app->id()) {
                $webform = $app->getWebform();
                $form['applications']['application_' . $app->id()] = [
                  '#type' => 'details',
                  '#open' => FALSE,
                  '#title' => t('Application Submission for %webform (Click to Open)', ['%webform' => $webform->label()]),
                  'sub' => $webformSubmissionViewBuilder->view($app),
                ];
              }
            }
          }
        }
        // Show Review
        if (in_array(TRUE, [$admin, $reviewer, $isOnWorkflowViewAccessTeam])) {
          $reviews = $application->getReviewSubmissions();
          if ($reviews) {
            $form['reviews'] = [
              '#type' => 'details',
              '#group' => 'niobi_app_submissions',
              '#title' => 'Review Submissions',
              '#open' => TRUE,
            ];
            /* @var $review \Drupal\webform\Entity\WebformSubmission */
            foreach ($reviews as $review) {
              if ($review && $review->id()) {
                $webform = $review->getWebform();
                $form['reviews']['review_' . $review->id()] = [
                  '#type' => 'details',
                  '#open' => FALSE,
                  '#title' => t('Review Submission for %webform (Click to Open)', ['%webform' => $webform->label()]),
                  'sub' => $webformSubmissionViewBuilder->view($review),
                ];
              }
            }
          }
        }
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $application_id = NULL) {
    return NiobiApplicationSubmissions::buildRenderArray($application_id);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Do nothing.
  }

}
