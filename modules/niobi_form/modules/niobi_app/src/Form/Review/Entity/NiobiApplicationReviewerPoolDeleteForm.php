<?php

namespace Drupal\niobi_app\Form\Review\Entity;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Niobi application reviewer pool entities.
 *
 * @ingroup niobi_app
 */
class NiobiApplicationReviewerPoolDeleteForm extends ContentEntityDeleteForm {


}
