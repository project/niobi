<?php

namespace Drupal\niobi_app\Form\Review\Entity;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Niobi application reviewer pool edit forms.
 *
 * @ingroup niobi_app
 */
class NiobiApplicationReviewerPoolForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\niobi_app\Entity\NiobiApplicationReviewerPool */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Niobi application reviewer pool.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Niobi application reviewer pool.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.niobi_application_reviewer_pool.canonical', ['niobi_application_reviewer_pool' => $entity->id()]);
  }

}
