<?php

namespace Drupal\niobi_app\Form\Review;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;
use Drupal\niobi_app\NiobiAppUtilities;
use Drupal\niobi_form\Entity\NiobiForm;

/**
 * Form controller for Niobi Application launch checklists.
 *
 * @ingroup niobi_app
 */
class NiobiAppReviewChecklist extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'niobi_app_email_checklist';
  }

  /**
   * @return array
   */
  private function printHeader() {
    $ret = [];
    $ret['title-line'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => t('About this report'),
    ];
    $ret['header-description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => t('This report lists all review forms and reviewer pools, so that you can determine whether
      the review process has been fully set up.'),
    ];

    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NiobiApplicationWorkflow $niobi_application_workflow = NULL) {
    if ($niobi_application_workflow) {
      $stages = $niobi_application_workflow->getStages();

      $form['header'] = $this->printHeader();

      foreach ($stages as $stage) {
        /* @var $stage \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage */
        $stage_id = 'stage_' . $stage->id();
        $form[$stage_id] = [
          '#type' => 'fieldset',
          '#title' => $stage->label(),
        ];
        $form[$stage_id]['stage_header'] = [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => t('Workflow Stage Settings'),
        ];
        $opts = [
          'attributes' => [
            'class' => 'btn btn-info button'
          ],
          'query' => [
            'destination' => \Drupal::service('path.current')->getPath(),
            'workflow' => $stage->getParentWorkflow()->id(),
          ],
        ];
        $link = $stage->toLink($stage->label(), 'edit-form', $opts);
        $form[$stage_id]['stage_header_button'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $link->toString()->getGeneratedLink(),
        ];

        $form[$stage_id]['stage_forms_header'] = [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => t('Review Forms'),
        ];
        $form[$stage_id]['stage_forms_header_info'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => t('Reviewers may be assigned or auto-assigned the following forms.'),
        ];
        $stage_auto_form = $stage->getAutoAssignForm();
        $stage_forms = $stage->getReviewForms();
        if (!empty($stage_auto_form)) {
          $form[$stage_id]['stage_forms_auto_header'] = [
            '#type' => 'html_tag',
            '#tag' => 'h4',
            '#value' => t('Auto-Assignment'),
          ];
          $form[$stage_id]['stage_forms_auto_link'] = [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => $stage_auto_form->toLink()->toString()->getGeneratedLink(),
          ];
        }
        if (!empty($stage_forms)) {
          $form[$stage_id]['stage_forms_manual_header'] = [
            '#type' => 'html_tag',
            '#tag' => 'h4',
            '#value' => t('Manual Assignment'),
          ];
          foreach($stage_forms as $sf) {
            if ($sf) {
              $form[$stage_id]['stage_forms_' . $sf->id()] = [
                '#type' => 'html_tag',
                '#tag' => 'p',
                '#value' => $sf->toLink()->toString()->getGeneratedLink(),
              ];
            }
          }
        }
        elseif (empty($stage_auto_form) && empty($stage_forms)) {
          $form[$stage_id]['stage_forms_empty'] = [
            '#type' => 'html_tag',
            '#tag' => 'h4',
            '#value' => '',
            'em' => [
              '#type' => 'html_tag',
              '#tag' => 'em',
              '#value' => t('No forms have been added'),
            ],
          ];
        }


        $form[$stage_id]['stage_pools_header'] = [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => t('Reviewer Pools'),
        ];
        $form[$stage_id]['stage_pools_header_info'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => t('Reviewers in the following pools may be assigned or auto-assigned.'),
        ];
        $stage_auto_pool = $stage->getAutoAssignReviewerPool();
        $stage_pools = $stage->getReviewerPools();
        if (!empty($stage_auto_pool)) {
          $form[$stage_id]['stage_pools_auto_header'] = [
            '#type' => 'html_tag',
            '#tag' => 'h4',
            '#value' => t('Auto-Assignment'),
          ];
          $form[$stage_id]['stage_pools_auto_link'] = [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => $stage_auto_pool->toLink()->toString()->getGeneratedLink(),
          ];
        }
        if (!empty($stage_pools)) {
          $form[$stage_id]['stage_pools_manual_header'] = [
            '#type' => 'html_tag',
            '#tag' => 'h4',
            '#value' => t('Manual Assignment'),
          ];
          foreach($stage_pools as $sp) {
            if ($sp) {
              $form[$stage_id]['stage_pools_' . $sp->id()] = [
                '#type' => 'html_tag',
                '#tag' => 'p',
                '#value' => $sp->toLink()->toString()->getGeneratedLink(),
              ];
            }
          }
        }
        elseif (empty($stage_auto_pool) && empty($stage_pools)) {
          $form[$stage_id]['stage_pools_empty'] = [
            '#type' => 'html_tag',
            '#tag' => 'h4',
            '#value' => '',
            'em' => [
              '#type' => 'html_tag',
              '#tag' => 'em',
              '#value' => t('No reviewer pools have been added'),
            ],
          ];
        }
      }

    }
    return $form;
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Do nothing.
  }

}
