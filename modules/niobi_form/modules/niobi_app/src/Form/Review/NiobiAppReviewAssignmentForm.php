<?php
/**
 * @file
 * Contains \Drupal\niobi_app\Form\Review\NiobiAppReviewAssignmentForm.
 */

namespace Drupal\niobi_app\Form\Review;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\NiobiAppUtilities;
use Drupal\node\Entity\Node;
use Drupal\task\TaskUtilities;
use Drupal\user\Entity\User;
use Drupal\niobi_app\Entity\NiobiApplication;

class NiobiAppReviewAssignmentForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'niobi_app_review_assignment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $niobi_application = NULL) {
    /* @var $niobi_application \Drupal\niobi_app\Entity\NiobiApplication */
    if ($niobi_application) {
      $workflow_stage = $niobi_application->getCurrentApplicationStage();
      /* @var $workflow \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage */
      $pools = $workflow_stage->getReviewerPools();
      $form['application_label'] = [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => t('Application: %app', ['%app' => $niobi_application->label()])
      ];
      $form['application'] = [
        '#type' => 'hidden',
        '#value' => $niobi_application->id(),
      ];
      $form['workflow_stage'] = [
        '#type' => 'hidden',
        '#value' => $workflow_stage->id(),
      ];
      $form['show_application'] = self::applicationFieldsForReviewDashboard($niobi_application);

      // Divide review assignment and email into two sections.
      $form['review_assignment'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['col-md-6 col-sm-12']
        ],
      ];
      $review_forms = $workflow_stage->getReviewFormsAsNames();
      $rfkeys = array_keys($review_forms);
      $form['review_assignment']['review_form'] = [
        '#title' => 'Select Review Form',
        '#type' => 'select',
        '#options' => $review_forms,
        '#required' => TRUE,
        '#empty_option' => !empty($review_forms) ? '- Select -' : 'No review forms have been attached to this workflow stage.',
        '#default_value' => count($review_forms) === 1 ? array_shift($rfkeys) : NULL,
      ];
      $form['review_assignment']['reviewer'] = [
        '#title' => 'Select Reviewer',
        '#type' => 'select',
        '#options' => [],
        '#required' => TRUE,
        '#multiple' => TRUE,
        '#size' => 10,
        '#empty_option' => !empty($pools) ? '- Select -' : 'No pools have been attached to this workflow stage.',
      ];
      foreach ($pools as $pool) {
        /* @var $pool \Drupal\niobi_app\Entity\NiobiApplicationReviewerPool */
        if (!empty($pool->getReviewersAsNames($niobi_application, $workflow_stage)) ) {
          $form['review_assignment']['reviewer']['#options'][$pool->label()] = $pool->getReviewersAsNames($niobi_application, $workflow_stage);
        }
        else {
          $form['review_assignment']['reviewer']['#options'][$pool->label()] = ['none' => t('This pool is empty, or all reviewers are excluded.')];
        }

      }
      if (count($pools) === 1) {
        $pool = array_shift($pools);
        $names = $pool->getReviewersAsNames($niobi_application, $workflow_stage);
        if (count($names) === 1) {
          $keys = array_keys($names);
          $form['review_assignment']['reviewer']['#default_value'] = array_shift($keys);
        }
        else if (count($names) === 0) {
          $form['review_assignment']['reviewer']['#default_value'] = ['none' => t('This pool is empty, or all reviewers are excluded.')];
        }
      }
      // Set up review message section.
      $form['review_message'] = $this->reviewMessageSection($workflow_stage->id());



      // Submit Button
      $form['submit_container'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['col-md-12 col-sm-12']
        ],
      ];
      $form['submit_container']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Create Review Assignment(s)'),
        '#button_type' => 'primary',
      ];
    }

    return $form;
  }

  public function reviewMessageSection($workflow_stage_id) {
    $form = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['col-md-6 col-sm-12']
      ],
      '#prefix' => '<div id="review-assignment-email-section">',
      '#suffix' => '</div>'
    ];
    $templates = NiobiAppUtilities::getEmailTemplates($workflow_stage_id, 'review');
    $template_options = [];
    foreach ($templates as $id => $template_node) {
      $id = implode('_', ['node', $id]);
      $template_options[$id] = $template_node->label();
    }
    // Give the template options
    $form['template'] = [
      '#type' => 'select',
      '#title' => 'Use a Template',
      '#options' => array_merge(['unset' => '- Select -'], $template_options),
    ];
    $form['template']['#ajax'] = [
      'callback' => '::reviewMessageSectionRebuild',
      'event' => 'change',
      'wrapper' => 'review-assignment-email-section',
    ];

    // Email fields
    $form['send'] = [
      '#type' => 'checkbox',
      '#title' => t('Send an email for this assignment'),
      '#description' => t('Send an email for this assignment'),
      '#weight' => -10,
      '#default_value' => TRUE,
    ];

    $form['cc'] = [
      '#type' => 'textfield',
      '#title' => t('CC addresses'),
      '#description' => t('Comma-separated list of email addresses'),
    ];
    $form['bcc'] = [
      '#type' => 'textfield',
      '#title' => t('BCC addresses'),
      '#description' => t('Comma-separated list of email addresses'),
    ];
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => t('Email subject'),
      '#description' => t('Comma-separated list of email addresses'),
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => t('Email message'),
    ];
    return $form;
  }

  public function reviewMessageSectionRebuild(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $template = str_replace('node_', '', $values['template']);

    if ($template !== 'unset') {
      $template_node = Node::load($template);
      if ($template_node) {
        $cc = $template_node->get('field_niobi_app_cc')->getValue();
        $form['review_message']['cc']['#value'] = isset($cc[0]['value']) ? $cc[0]['value'] : '';
        $bcc = $template_node->get('field_niobi_app_bcc')->getValue();
        $form['review_message']['bcc']['#value'] = isset($bcc[0]['value']) ? $bcc[0]['value'] : '';
        $subject = $template_node->get('field_niobi_app_email_subject')->getValue();
        $form['review_message']['subject']['#value'] = isset($subject[0]['value']) ? $subject[0]['value'] : '';
        $message = $template_node->get('field_niobi_app_email_message')->getValue();
        $form['review_message']['message']['#value'] = isset($message[0]['value']) ? $message[0]['value'] : '';
      }
    }
    else {
      $form['review_message']['cc']['#value'] = '';
      $form['review_message']['bcc']['#value'] = '';
      $config = \Drupal::config('system.site');
      $form['review_message']['subject']['#value'] = 'You have been assigned to review an application on '. $config->get('name');
      $form['review_message']['message']['#value'] = implode(' ',['Please go to', \Drupal::request()->getHost(), 'to get a list of your review assignments.']);
    }

    $form_state->setRebuild(TRUE);
    return $form['review_message'];
  }

  public static function applicationFieldsForReviewDashboard(NiobiApplication $application, $report_type = 'key_value') {
    $ret = [];
    $status = $application->getCurrentApplicationStatus();

    if (in_array($status, ['decision', 'review'])) {
      $stage = $application->getCurrentApplicationStage();
      $application_form = $stage->getApplicationForm();
      if ($application_form) {
        $app_webform = $application_form->getWebform();

        $app_sub = NULL;
        foreach ($application->getApplicationSubmissions() as $sub) {
          /* @var $sub \Drupal\webform\Entity\WebformSubmission */
          if ($sub && $sub->getWebform()->id() === $app_webform->id()) {
            $app_sub = $sub;
          }
        }
        if ($app_sub) {
          /* @var $app_sub \Drupal\webform\Entity\WebformSubmission */
          $ret = [
            '#type' => 'html_tag',
            '#tag' => 'ul',
          ];
          $field = $stage->get('field_app_fields_for_review')->getValue();
          if (is_array($field)) {
            foreach($field as $i => $f) {
              $ret[$f['value'] . '_' . $application->id()] = [
                '#type' => 'html_tag',
                '#tag' => 'li',
              ];
              switch ($report_type) {
                case 'value':
                  $ret[$f['value'] . '_' . $application->id()]['#value'] = $app_sub->getElementData($f['value']);
                  break;
                case 'key_value':
                  $ret[$f['value'] . '_' . $application->id()]['#value'] = $f['value'] . ': ' . $app_sub->getElementData($f['value']);
                  break;
              }
            }
          }
        }
      }

      return $ret;
    }
    else {
      return ['#type' => 'markup', '#markup' => ''];
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $vals = $form_state->getValues();

    $reviewers = User::loadMultiple($vals['reviewer']);
    foreach ($reviewers as $reviewer) {
      /* @var $reviewer User */
      $niobi_application = NiobiApplication::load($vals['application']);
      // Get data for application owner
      $label_info = [
        '@reviewer' => $reviewer->label(),
        '@app' => $niobi_application->label(),
      ];
      $data = [
        'type' => 'application_review_task',
        'status' => 'active',
        'name' => t('Assign @reviewer to @app', $label_info),
        'assigned_by_type' => 'system',
        'assigned_by' => '0',
        'assigned_to' => $reviewer->id(),
        'assigned_to_type' => 'user',
        'field_application' => $niobi_application->id(),
        'field_review_form' => $vals['review_form'],
      ];

      $task = TaskUtilities::createTask($data);
      if (!empty($task)) {
        \Drupal::messenger()->addMessage(t('Review assignment @label created', ['@label' => $data['name']]));
      }
      // Send email if info is set.
      $send = $form_state->getValue('send');
      if ($send) {
        $to = $reviewer->getEmail();
        if ($to) {
          $cc = $form_state->getValue('cc');
          $cc = !empty($cc) ? $cc : NULL;
          $bcc = $form_state->getValue('bcc');
          $bcc = !empty($bcc) ? $bcc : NULL;

          $config = \Drupal::config('system.site');
          $subject = $form_state->getValue('subject');
          $subject = !empty($subject) ? $subject : 'You have been assigned to review an application on '. $config->get('name');
          $message = $form_state->getValue('message');
          $message = !empty($message) ? $message : implode(' ',['Please go to', \Drupal::request()->getHost(), 'to get a list of your review assignments.']);
          $params = [];
          $params['headers'] = [];
          $cc ? $params['headers']['Cc'] = $cc : NULL;
          $bcc ? $params['headers']['Bcc'] = $bcc : NULL;

          $module = 'niobi_app';
          $key = 'niobi_app_mail';
          $langcode = $reviewer->getPreferredLangcode();

          $params['from'] = \Drupal::config('system.site')->get('mail');
          $params['subject'] = $subject;
          $params['message'] = $message;

          $reply = NULL;
          $send = TRUE;
          $mailManager = \Drupal::service('plugin.manager.mail');

          $result = $mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);
        }
      }
    }
  }
}
