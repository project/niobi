<?php

namespace Drupal\niobi_app\Form\LaunchChecklist;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;
use Drupal\niobi_app\NiobiAppUtilities;
use Drupal\niobi_form\Entity\NiobiForm;

/**
 * Form controller for Niobi Application launch checklists.
 *
 * @ingroup niobi_app
 */
class NiobiAppEmailChecklist extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'niobi_app_email_checklist';
  }

  /**
   * @param NiobiForm $niobi_form
   * @return array
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  private function printFormStatus(NiobiForm $niobi_form) {
    $webform = $niobi_form->getWebform();
    if (!empty($webform)) {
      $ret = [];
      /* @var $webform \Drupal\webform\Entity\Webform */
      $handlers = $webform->getHandlers();
      foreach($handlers->getIterator()->getArrayCopy() as $key => $item) {
        /* @var $item \Drupal\webform\Plugin\WebformHandler\EmailWebformHandler */
        if (is_object($item) && method_exists($item, 'getEmailConfiguration')) {
          $item_config = $item->getEmailConfiguration();
          $ret[] = [
            '#type' => 'html_tag',
            '#tag' => 'h4',
            '#value' => '',
            'em' => [
              '#type' => 'html_tag',
              '#tag' => 'em',
              '#value' => $item->getLabel(),
            ]
          ];
          $data_to_get = [
            'from_mail' => t('From'),
            'from_name' => t('From Name'),
            'to_mail' => t('To'),
            'cc_mail' => t('Cc'),
            'bcc_mail' => t('Bcc'),
            'subject' => t('Subject'),
            'body' => t('Body'),
          ];
          foreach($data_to_get as $k => $val) {
            $ret[] = [
              '#type' => 'html_tag',
              '#tag' => 'p',
              '#value' => '',
              'strong' => [
                '#type' => 'html_tag',
                '#tag' => 'strong',
                '#value' => $val,
              ]
            ];
            $ret[] = [
              '#type' => 'html_tag',
              '#tag' => 'p',
              '#value' => $item_config[$k] ? $item_config[$k] : t('None'),
            ];
          }
          $ret[] = [
            '#type' => 'html_tag',
            '#tag' => 'hr',
            '#value' => '',
          ];
        }
      }
      if (empty($ret)) {
        $ret[] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => 'No email handlers found.',
        ];
      }
      return $ret;
    }
    else {
      return [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => t('No form was found.'),
      ];
    }
  }

  /**
   * @return array
   */
  private function printHeader() {
    $ret = [];
    $ret['title-line'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => t('About this report'),
    ];
    $ret['header-description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => t('This is a report showing all email handlers that are being used in this workflow.'),
    ];

    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NiobiApplicationWorkflow $niobi_application_workflow = NULL) {
    if ($niobi_application_workflow) {
      $stages = $niobi_application_workflow->getStages();

      $form['header'] = $this->printHeader();

      foreach ($stages as $stage) {
        /* @var $stage \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage */
        $stage_id = 'stage_' . $stage->id();
        $form[$stage_id] = [
          '#type' => 'fieldset',
          '#title' => $stage->label(),
        ];
        /*
         * Note: getAllForms() returns a keyed array, wither each key pointing
         * to an array of NiobiForm entities:
         * nomination, application, required_addenda, optional_addenda,
         * support, support_request, review.
         */
        $stage_forms = $stage->getAllForms();
        $labels = NiobiAppUtilities::generateStageListForChecklists($stage);
        foreach ($stage_forms as $key => $sf) {
          $form[$stage_id][$key] = [
            '#type' => 'container',
          ];
          $form[$stage_id][$key]['header'] = [
            '#type' => 'html_tag',
            '#tag' => 'h3',
            '#value' => !empty($labels[$key])?$labels[$key]:NULL,
          ];
          /* @var $f \Drupal\niobi_form\Entity\NiobiForm */
          foreach ($sf as $f) {
            if ($f) {
              if (is_array($f)) {
                foreach ($f as $frm) {
                  $fkey = 'form_' . $frm->id();
                  $form[$stage_id][$key][$fkey] = $this->printFormStatus($frm);
                }
              }
              else {
                $fkey = 'form_' . $f->id();
                $form[$stage_id][$key][$fkey] = $this->printFormStatus($f);
              }
            }
            else {
              // If a form was deleted, an empty step might leave a [0 => NULL] behind.
              if ($sf !== [0 => NULL]) {
                $form[$stage_id][$key]['form'] = [
                  '#type' => 'html_tag',
                  '#tag' => 'em',
                  '#value' => t('No form for this step.'),
                ];
              }
            }
          }
        }
      }

    }
    return $form;
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Do nothing.
  }

}
