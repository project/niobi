<?php

namespace Drupal\niobi_app\Form\LaunchChecklist;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;
use Drupal\niobi_app\NiobiAppUtilities;
use Drupal\niobi_form\Entity\NiobiForm;
use Drupal\Core\Url;

/**
 * Form controller for Niobi Application launch checklists.
 *
 * @ingroup niobi_app
 */
class NiobiAppFormCompleteChecklist extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'niobi_app_form_complete_checklist';
  }

  /**
   * @param NiobiForm $niobi_form
   * @return array
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  private function printFormStatus(NiobiForm $niobi_form) {
    /* @var $webform \Drupal\webform\Entity\Webform */
    $webform = $niobi_form->getWebform();
    $class = !empty($webform) ? 'btn btn-success' : 'btn btn-danger';
    $icon = !empty($webform) ? 'glyphicon glyphicon-ok' : 'glyphicon glyphicon-remove';
    $markup = Markup::create('<span class="' . $icon . '"></span> ' . $niobi_form->label());
    $link = $niobi_form->toLink($markup,'canonical', ['attributes' => ['class' => $class]]);
    $ret = [];
    $ret[] = [
      '#type' => 'html_tag',
      '#tag' => 'h4',
      '#value' => t('Form'),
    ];
    $ret[] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $link->toString()->getGeneratedLink(),
    ];
    if (!empty($webform)) {
      $ret[] = [
        '#type' => 'html_tag',
        '#tag' => 'h4',
        '#value' => t('Redirect'),
      ];
      $ret[] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => '',
        'em' => [
          '#type' => 'html_tag',
          '#tag' => 'em',
          '#value' => t('Application and Nomination forms should redirect to the application page.'),
        ],
      ];
      $data_to_get = [
        'confirmation_type' => t('Confirmation Type'),
        'confirmation_url' => t('Confirmation URL'),
        'confirmation_message' => t('Confirmation Message'),
      ];
      foreach ($data_to_get as $key => $setting_label) {
        $ret[] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => t('%setting: %value', ['%setting' => $setting_label, '%value' => $webform->getSetting($key)]),
        ];
      }
      $attrs = ['class' => 'btn btn-info btn-sm button'];
      $confirm_url = Url::fromRoute('entity.webform.settings_confirmation', ['webform' => $webform->id()], ['attributes' => $attrs]);
      $confirm_link = Link::fromTextAndUrl('Confirmation Settings', $confirm_url)->toString()->getGeneratedLink();
      $ret[] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $confirm_link,
      ];
    }

    return $ret;
  }

  /**
   * @return array
   */
  private function printHeader() {
    $ret = [];
    $ret['title-line'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => t('About this report'),
    ];
    $ret['header-description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => t('This is a report which shows the forms you have created so far, and
    whether they have been set up with a corresponding webform. Each button is a
    link to the form in question.'),
    ];

    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NiobiApplicationWorkflow $niobi_application_workflow = NULL) {
    if ($niobi_application_workflow) {
      $stages = $niobi_application_workflow->getStages();

      $form['header'] = $this->printHeader();

      foreach ($stages as $stage) {
        /* @var $stage \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage */
        $stage_id = 'stage_' . $stage->id();
        $form[$stage_id] = [
          '#type' => 'fieldset',
          '#title' => $stage->label(),
        ];
        /*
         * Note: getAllForms() returns a keyed array, wither each key pointing
         * to an array of NiobiForm entities:
         * nomination, application, required_addenda, optional_addenda,
         * support, support_request, review.
         */
        $stage_forms = $stage->getAllForms();
        $labels = NiobiAppUtilities::generateStageListForChecklists($stage);
        foreach ($stage_forms as $key => $sf) {
          $form[$stage_id][$key] = [
            '#type' => 'container',
          ];
          $form[$stage_id][$key]['header'] = [
            '#type' => 'html_tag',
            '#tag' => 'h3',
            '#value' => !empty($labels[$key])?$labels[$key]:NULL,
          ];
          /* @var $f \Drupal\niobi_form\Entity\NiobiForm */
          foreach ($sf as $f) {
            if ($f) {
              if (is_array($f)) {
                foreach ($f as $frm) {
                  $fkey = 'form_' . $frm->id();
                  $form[$stage_id][$key][$fkey] = $this->printFormStatus($frm);
                }
              }
              else {
                $fkey = 'form_' . $f->id();
                $form[$stage_id][$key][$fkey] = $this->printFormStatus($f);
              }
            }
            else {
              // If a form was deleted, an empty step might leave a [0 => NULL] behind.
              if ($sf !== [0 => NULL]) {
                $form[$stage_id][$key]['form'] = [
                  '#type' => 'html_tag',
                  '#tag' => 'em',
                  '#value' => t('No form for this step.'),
                ];
              }
            }
          }
        }
      }

    }
    return $form;
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Do nothing.
  }

}
