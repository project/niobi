<?php

namespace Drupal\niobi_app\Form\Entity;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Niobi Application entities.
 *
 * @ingroup niobi_app
 */
class NiobiApplicationDeleteForm extends ContentEntityDeleteForm {


}
