<?php

namespace Drupal\niobi_app\Form\Entity;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Niobi Application Workflow Stage entities.
 *
 * @ingroup niobi_app
 */
class NiobiApplicationWorkflowStageDeleteForm extends ContentEntityDeleteForm {


}
