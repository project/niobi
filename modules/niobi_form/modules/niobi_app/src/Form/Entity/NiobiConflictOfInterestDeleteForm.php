<?php

namespace Drupal\niobi_app\Form\Entity;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Conflict of Interest entities.
 *
 * @ingroup niobi_app
 */
class NiobiConflictOfInterestDeleteForm extends ContentEntityDeleteForm {


}
