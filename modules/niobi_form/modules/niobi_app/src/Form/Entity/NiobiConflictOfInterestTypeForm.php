<?php

namespace Drupal\niobi_app\Form\Entity;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class NiobiConflictOfInterestTypeForm.
 */
class NiobiConflictOfInterestTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $niobi_conflict_of_interest_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $niobi_conflict_of_interest_type->label(),
      '#description' => $this->t("Label for the Conflict of Interest type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $niobi_conflict_of_interest_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\niobi_app\Entity\NiobiConflictOfInterestType::load',
      ],
      '#disabled' => !$niobi_conflict_of_interest_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $niobi_conflict_of_interest_type = $this->entity;
    $status = $niobi_conflict_of_interest_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Conflict of Interest type.', [
          '%label' => $niobi_conflict_of_interest_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Conflict of Interest type.', [
          '%label' => $niobi_conflict_of_interest_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($niobi_conflict_of_interest_type->toUrl('collection'));
  }

}
