<?php

namespace Drupal\niobi_app\Form\Entity;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Niobi Application edit forms.
 *
 * @ingroup niobi_app
 */
class NiobiApplicationForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\niobi_app\Entity\NiobiApplication */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Niobi Application.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Niobi Application.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.niobi_application.canonical', ['niobi_application' => $entity->id()]);
  }

}
