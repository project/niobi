<?php

namespace Drupal\niobi_app;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Niobi Application Workflow entities.
 *
 * @ingroup niobi_app
 */
class NiobiApplicationWorkflowListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Niobi Application Workflow ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\niobi_app\Entity\NiobiApplicationWorkflow */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.niobi_application_workflow.edit_form',
      ['niobi_application_workflow' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
