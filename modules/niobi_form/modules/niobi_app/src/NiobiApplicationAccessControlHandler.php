<?php

namespace Drupal\niobi_app;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Niobi Application entity.
 *
 * @see \Drupal\niobi_app\Entity\NiobiApplication.
 */
class NiobiApplicationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\niobi_app\Entity\NiobiApplication $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished niobi application entities');
        }

        if ($account->isAuthenticated() && $account->hasPermission('view published niobi application entities')) {
          // if user is applicant
          if ($entity->isApplicant($account->id())) {
            return AccessResult::allowed();
          }

          // if user is a workflow admin or in view access team
          $workflow = current($entity->field_application_workflow->referencedEntities());
          if (!empty($workflow)) {
            if ($entity->isApplicationAdmin()) {
              return AccessResult::allowed();
            }
            if ($workflow->isOnViewAccessTeam($account->id())) {
              $conflicts = \Drupal::entityTypeManager()->getStorage('niobi_conflict_of_interest')
                ->loadByProperties([
                  'type' => 'conflict_with_another_user',
                  'field_applicant' => $entity->getOwnerId(),
                  'field_reviewer' => $account->id()
                ]);
              if (empty($conflicts)) {
                return AccessResult::allowed();
              } else {
                return AccessResult::forbidden();
              }
            }
          }

          // if user is a reviewer for this application
          if ($entity->isReviewer($account)) {
            return AccessResult::allowed();
          }
        }

        return AccessResult::neutral();

      case 'update':
        if ($account->isAuthenticated() && $account->hasPermission('edit niobi application entities')) {
          $workflow = current($entity->field_application_workflow->referencedEntities());
          if (NiobiAppUtilities::isWorkflowAdmin($workflow)) {
            return AccessResult::allowed();
          }
        }

        return AccessResult::neutral();

      case 'delete':
        if ($account->isAuthenticated() && $account->hasPermission('delete niobi application entities')) {
          $workflow = current($entity->field_application_workflow->referencedEntities());
          if (NiobiAppUtilities::isWorkflowAdmin($workflow)) {
            return AccessResult::allowed();
          }
        }

        return AccessResult::neutral();
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add niobi application entities');
  }

}
