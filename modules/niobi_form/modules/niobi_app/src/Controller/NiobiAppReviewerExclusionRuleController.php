<?php

/**
 * @file
 */

namespace Drupal\niobi_app\Controller;

use Drupal\niobi_app\NiobiAppReviewerExclusionRuleManager;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NiobiAppReviewerExclusionRuleController
 *
 * Provides the route and API controller for niobi_app.
 */
class NiobiAppReviewerExclusionRuleController extends ControllerBase {

  protected $NiobiAppReviewerExclusionRuleManager; //The plugin manager.

  /**
   * Constructor.
   *
   * @param \Drupal\niobi_app\NiobiAppReviewerExclusionRuleManager $plugin_manager
   */

  public function __construct(NiobiAppReviewerExclusionRuleManager $plugin_manager) {
    $this->NiobiAppReviewerExclusionRuleManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Use the service container to instantiate a new instance of our controller.
    return new static($container->get('plugin.manager.niobi_app'));
  }
}