<?php
/**
 * @file
 */

namespace Drupal\niobi_app\Controller;

use Drupal\niobi_app\NiobiAppDecisionManager;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NiobiAppDecisionController
 *
 * Provides the route and API controller for niobi_app.
 */
class NiobiAppDecisionController extends ControllerBase
{

  protected $NiobiAppDecisionManager; //The plugin manager.

  /**
   * Constructor.
   *
   * @param \Drupal\niobi_app\NiobiAppDecisionManager $plugin_manager
   */

  public function __construct(NiobiAppDecisionManager $plugin_manager) {
    $this->NiobiAppDecisionManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Use the service container to instantiate a new instance of our controller.
    return new static($container->get('plugin.manager.niobi_app_decision'));
  }
}