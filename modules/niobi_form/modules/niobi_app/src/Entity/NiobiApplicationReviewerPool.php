<?php

namespace Drupal\niobi_app\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\niobi_app\NiobiAppUtilities;
use Drupal\user\UserInterface;
use Drupal\user\Entity\User;

/**
 * Defines the Niobi application reviewer pool entity.
 *
 * @ingroup niobi_app
 *
 * @ContentEntityType(
 *   id = "niobi_application_reviewer_pool",
 *   label = @Translation("Niobi application reviewer pool"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\niobi_app\NiobiApplicationReviewerPoolListBuilder",
 *     "views_data" = "Drupal\niobi_app\Entity\NiobiApplicationReviewerPoolViewsData",
 *     "translation" = "Drupal\niobi_app\NiobiApplicationReviewerPoolTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\niobi_app\Form\Review\Entity\NiobiApplicationReviewerPoolForm",
 *       "add" = "Drupal\niobi_app\Form\Review\Entity\NiobiApplicationReviewerPoolForm",
 *       "edit" = "Drupal\niobi_app\Form\Review\Entity\NiobiApplicationReviewerPoolForm",
 *       "delete" = "Drupal\niobi_app\Form\Review\Entity\NiobiApplicationReviewerPoolDeleteForm",
 *     },
 *     "access" = "Drupal\niobi_app\NiobiApplicationReviewerPoolAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\niobi_app\NiobiApplicationReviewerPoolHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "niobi_application_reviewer_pool",
 *   data_table = "niobi_application_reviewer_pool_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer niobi application reviewer pool entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/application_reviewer_pool/{niobi_application_reviewer_pool}",
 *     "add-form" = "/application_reviewer_pool/add",
 *     "edit-form" = "/application_reviewer_pool/{niobi_application_reviewer_pool}/edit",
 *     "delete-form" = "/application_reviewer_pool/{niobi_application_reviewer_pool}/delete",
 *     "collection" = "/admin/niobi/niobi_application_reviewer_pool",
 *   },
 *   field_ui_base_route = "niobi_application_reviewer_pool.settings"
 * )
 */
class NiobiApplicationReviewerPool extends ContentEntityBase implements NiobiApplicationReviewerPoolInterface {

  use EntityChangedTrait;

  public function getReviewersbyField($field = 'field_reviewers', NiobiApplication $niobi_application = NULL, NiobiApplicationWorkflowStage $workflow_stage = NULL) {
    $ret = [];
    $id_list = $this->get($field)->getValue();
    // If there is an application, run exclusion rules.
    if (!empty($niobi_application)) {

      foreach ($id_list as $item) {
        $id = $item['target_id'];
        $reviewer = User::load($id);
        if ($reviewer) {
          $excluded = NiobiAppUtilities::excludedAsReviewer($reviewer, $niobi_application);
          if (!$excluded) {
            $ret[$id] = $reviewer;
          }
        }
      }
      return $ret;
    }
    // If there is no application, we just want the list.
    else {
      foreach ($id_list as $item) {
        $id = $item['target_id'];
        $ret[$id] = User::load($id);
      }
      return $ret;
    }
  }

  /**
   * @param NiobiApplication|NULL $niobi_application
   * @param NiobiApplicationWorkflowStage|NULL $workflow_stage
   * @return array
   */
  public function getReviewers(NiobiApplication $niobi_application = NULL, NiobiApplicationWorkflowStage $workflow_stage = NULL) {
    return $this->getReviewersbyField('field_reviewers', $niobi_application, $workflow_stage);
  }

  /**
   * @param NiobiApplication|NULL $niobi_application
   * @param NiobiApplicationWorkflowStage|NULL $workflow_stage
   * @return array
   */
  public function getAutoAssignmentReviewers(NiobiApplication $niobi_application = NULL, NiobiApplicationWorkflowStage $workflow_stage = NULL) {
    return $this->getReviewersbyField('field_reviewers', $niobi_application, $workflow_stage);
  }

  /**
   * @param NiobiApplication|NULL $niobi_application
   * @param NiobiApplicationWorkflowStage|NULL $workflow_stage
   * @return array
   */
  public function getReviewersAsNames(NiobiApplication $niobi_application = NULL, NiobiApplicationWorkflowStage $workflow_stage = NULL) {
    $reviewers = $this->getReviewers($niobi_application, $workflow_stage);
    $ret = [];
    foreach($reviewers as $reviewer) {
      $ret[$reviewer->id()] = $reviewer->label();
    }
    return $ret;
  }

  public function getReferencingStages($include_auto = TRUE) {
    $query = \Drupal::entityQuery('niobi_application_workflow_stage');
    $group = $query->orConditionGroup();
    $group->condition('field_reviewer_pools', $this->id(), 'IN');
    $include_auto ? $group->condition('field_auto_assign_reviewer_pool', $this->id()) : NULL;
    $query->condition($group);
    $stage_ids = $query->execute();
    return NiobiApplicationWorkflowStage::loadMultiple($stage_ids);
  }

  /**
   * check a user exists in the reviewer pool
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return bool
   */
  public function containUser(AccountInterface $account) {
    $reviewers = $this->field_reviewers->getValue();
    foreach ($reviewers as $reviewer) {
      if ($reviewer['target_id'] === $account->id()) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Niobi application reviewer pool entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Niobi application reviewer pool entity.'))
      ->setSettings([
        'max_length' => 511,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Niobi application reviewer pool is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
