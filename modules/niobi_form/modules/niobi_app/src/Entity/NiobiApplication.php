<?php

namespace Drupal\niobi_app\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Defines the Niobi Application entity.
 *
 * @ingroup niobi_app
 *
 * @ContentEntityType(
 *   id = "niobi_application",
 *   label = @Translation("Niobi Application"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\niobi_app\NiobiApplicationListBuilder",
 *     "views_data" = "Drupal\niobi_app\Entity\NiobiApplicationViewsData",
 *     "translation" = "Drupal\niobi_app\NiobiApplicationTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\niobi_app\Form\Entity\NiobiApplicationForm",
 *       "add" = "Drupal\niobi_app\Form\Entity\NiobiApplicationForm",
 *       "edit" = "Drupal\niobi_app\Form\Entity\NiobiApplicationForm",
 *       "delete" = "Drupal\niobi_app\Form\Entity\NiobiApplicationDeleteForm",
 *     },
 *     "access" = "Drupal\niobi_app\NiobiApplicationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\niobi_app\NiobiApplicationHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "niobi_application",
 *   data_table = "niobi_application_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer niobi application entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/application/{niobi_application}",
 *     "add-form" = "/application/add",
 *     "edit-form" = "/application/{niobi_application}/edit",
 *     "delete-form" = "/application/{niobi_application}/delete",
 *     "collection" = "/admin/niobi/niobi_application",
 *   },
 *   field_ui_base_route = "niobi_application.settings"
 * )
 */
class NiobiApplication extends ContentEntityBase implements NiobiApplicationInterface {

  use EntityChangedTrait;

  /**
   * @return NiobiApplicationWorkflow
   */
  public function getApplicationWorkflow() {
    return $this->field_application_workflow->entity;
  }

  /**
   * @return NiobiApplicationWorkflowStage
   */
  public function getCurrentApplicationStage() {
    return $this->field_current_stage->entity;
  }

  /**
   * @return string
   */
  public function getCurrentApplicationStatus() {
    return $this->field_application_status->value;
  }

  /**
   * @return array
   */
  public function getNominationSubmissions() {
    $vals = $this->get('field_nomination_submissions')->getValue();
    $ret = [];
    foreach ($vals as $val) {
      $ret[$val['target_id']] = WebformSubmission::load($val['target_id']);
    }
    return $ret;
  }

  /**
   * @return array
   */
  public function getApplicationSubmissions() {
    $vals = $this->get('field_application_submissions')->getValue();
    $ret = [];
    foreach ($vals as $val) {
      $ret[$val['target_id']] = WebformSubmission::load($val['target_id']);
    }
    return $ret;
  }

  /**
   * @return array
   */
  public function getReviewSubmissions() {
    $vals = $this->get('field_review_submissions')->getValue();
    $ret = [];
    $canViewAllReviewSubmissions = $this->isApplicationAdmin() || $this->hasViewPermission();
    $uid = \Drupal::currentUser()->id();
    foreach ($vals as $val) {
      $isInserted = TRUE;
      $submission = WebformSubmission::load($val['target_id']);
      if (!$canViewAllReviewSubmissions) {
        $submissionOwnerId = $submission->getOwnerId();
        if ($uid != $submissionOwnerId) {
          $isInserted = FALSE;
        }
      }
      if ($isInserted) {
        $ret[$val['target_id']] = $submission;
      }
    }
    return $ret;
  }

  /**
   * @return array
   */
  public function getAllSubmissions() {
    $ret = [];
    $ret['nomination'] = $this->getNominationSubmissions();
    $ret['application'] = $this->getApplicationSubmissions();
    $ret['review'] = $this->getReviewSubmissions();
    return $ret;
  }

  /**
   * @return array
   */
  public function getAllSubmissionsbyWebform() {
    $subs = $this->getAllSubmissions();
    $ret = [];
    foreach ($subs as $category => $sub_list) {
      /* @var $sub \Drupal\webform\Entity\WebformSubmission */
      foreach ($sub_list as $sub) {
        if ($sub && $sub->id() && $sub->getWebform()) {
          $ret[$category][$sub->getWebform()->id()][$sub->id()] = $sub;
        }
      }
    }

    return $ret;
  }

  /**
   * @param AccountInterface|NULL $account
   * @return bool
   */
  public function isApplicationAdmin(AccountInterface $account = NULL) {
    if ($account === NULL) {
      $account = \Drupal::currentUser();
    }
    $uid = $account ? $account->id() : NULL;
    $admin = $this->getApplicationWorkflow()->getOwner();
    $admin_id = $admin->id();

    return $uid === $admin_id
      || ($account && $account->hasPermission('administer niobi_app applications'))
      || $this->getApplicationWorkflow()->isOnAdminTeam();
  }

  public function hasViewPermission(AccountInterface $account = NULL) {
    $workflow = $this->getApplicationWorkflow();
    return $workflow->isOnViewAccessTeam($account);
  }

  /**
   * @param null $account_id
   * @return bool
   */
  public function isApplicant($account_id = NULL) {
    $uid = $account_id ? $account_id : \Drupal::currentUser()->id();
    $applicant_id = $this->getOwner()->id();
    return $uid === $applicant_id;
  }

  /**
   * @param null $account_id
   * @return bool
   */
  public function isNominator($account_id = NULL) {
    $uid = $account_id ? $account_id : \Drupal::currentUser()->id();
    $subs = $this->getNominationSubmissions();
    /* @var $sub \Drupal\webform\Entity\WebformSubmission */
    $nominator_list = [];
    foreach ($subs as $sub) {
      $nominator_list[] = $sub->getOwner()->id();
    }
    return in_array($uid, $nominator_list);
  }

  /**
   * @param AccountInterface|NULL $account
   * @return bool
   */
  public function isReviewer(AccountInterface $account = NULL) {
    $uid = $account ? $account->id() : \Drupal::currentUser()->id();
    $query = \Drupal::entityQuery('task');
    $query->condition('type', 'application_review_task');
    $query->condition('assigned_to_type', 'user');
    $query->condition('assigned_to', $uid);
    $query->condition('field_application', $this->id());
    $result = $query->execute();
    return !empty($result);
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  public function hasConflictOfInterest($account) {

  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Niobi Application entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Niobi Application entity.'))
      ->setSettings([
        'max_length' => 511,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Niobi Application is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
