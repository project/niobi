<?php

namespace Drupal\niobi_app\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Conflict of Interest entity.
 *
 * @ingroup niobi_app
 *
 * @ContentEntityType(
 *   id = "niobi_conflict_of_interest",
 *   label = @Translation("Conflict of Interest"),
 *   bundle_label = @Translation("Conflict of Interest type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\niobi_app\NiobiConflictOfInterestListBuilder",
 *     "views_data" = "Drupal\niobi_app\Entity\NiobiConflictOfInterestViewsData",
 *     "translation" = "Drupal\niobi_app\NiobiConflictOfInterestTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\niobi_app\Form\Entity\NiobiConflictOfInterestForm",
 *       "add" = "Drupal\niobi_app\Form\Entity\NiobiConflictOfInterestForm",
 *       "edit" = "Drupal\niobi_app\Form\Entity\NiobiConflictOfInterestForm",
 *       "delete" = "Drupal\niobi_app\Form\Entity\NiobiConflictOfInterestDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\niobi_app\NiobiConflictOfInterestHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\niobi_app\NiobiConflictOfInterestAccessControlHandler",
 *   },
 *   base_table = "niobi_conflict_of_interest",
 *   data_table = "niobi_conflict_of_interest_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer conflict of interest entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/niobi/niobi_application_workflow/niobi_conflict_of_interest/{niobi_conflict_of_interest}",
 *     "add-page" = "/admin/niobi/niobi_application_workflow/niobi_conflict_of_interest/add",
 *     "add-form" = "/admin/niobi/niobi_application_workflow/niobi_conflict_of_interest/add/{niobi_conflict_of_interest_type}",
 *     "edit-form" = "/admin/niobi/niobi_application_workflow/niobi_conflict_of_interest/{niobi_conflict_of_interest}/edit",
 *     "delete-form" = "/admin/niobi/niobi_application_workflow/niobi_conflict_of_interest/{niobi_conflict_of_interest}/delete",
 *     "collection" = "/admin/niobi/niobi_application_workflow/niobi_conflict_of_interest",
 *   },
 *   bundle_entity_type = "niobi_conflict_of_interest_type",
 *   field_ui_base_route = "entity.niobi_conflict_of_interest_type.edit_form"
 * )
 */
class NiobiConflictOfInterest extends ContentEntityBase implements NiobiConflictOfInterestInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Conflict of Interest entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Conflict of Interest entity.'))
      ->setSettings([
        'max_length' => 511,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Conflict of Interest is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
