<?php

namespace Drupal\niobi_app\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Niobi Application Workflow entities.
 *
 * @ingroup niobi_app
 */
interface NiobiApplicationWorkflowInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Niobi Application Workflow name.
   *
   * @return string
   *   Name of the Niobi Application Workflow.
   */
  public function getName();

  /**
   * Sets the Niobi Application Workflow name.
   *
   * @param string $name
   *   The Niobi Application Workflow name.
   *
   * @return \Drupal\niobi_app\Entity\NiobiApplicationWorkflowInterface
   *   The called Niobi Application Workflow entity.
   */
  public function setName($name);

  /**
   * Gets the Niobi Application Workflow creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Niobi Application Workflow.
   */
  public function getCreatedTime();

  /**
   * Sets the Niobi Application Workflow creation timestamp.
   *
   * @param int $timestamp
   *   The Niobi Application Workflow creation timestamp.
   *
   * @return \Drupal\niobi_app\Entity\NiobiApplicationWorkflowInterface
   *   The called Niobi Application Workflow entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Niobi Application Workflow published status indicator.
   *
   * Unpublished Niobi Application Workflow are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Niobi Application Workflow is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Niobi Application Workflow.
   *
   * @param bool $published
   *   TRUE to set this Niobi Application Workflow to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\niobi_app\Entity\NiobiApplicationWorkflowInterface
   *   The called Niobi Application Workflow entity.
   */
  public function setPublished($published);

}
