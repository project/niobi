<?php

namespace Drupal\niobi_app\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Conflict of Interest type entity.
 *
 * @ConfigEntityType(
 *   id = "niobi_conflict_of_interest_type",
 *   label = @Translation("Conflict of Interest type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\niobi_app\NiobiConflictOfInterestTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\niobi_app\Form\Entity\NiobiConflictOfInterestTypeForm",
 *       "edit" = "Drupal\niobi_app\Form\Entity\NiobiConflictOfInterestTypeForm",
 *       "delete" = "Drupal\niobi_app\Form\Entity\NiobiConflictOfInterestTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\niobi_app\NiobiConflictOfInterestTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "niobi_conflict_of_interest_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "niobi_conflict_of_interest",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/niobi/niobi_application_workflow/niobi_conflict_of_interest_type/{niobi_conflict_of_interest_type}",
 *     "add-form" = "/admin/niobi/niobi_application_workflow/niobi_conflict_of_interest_type/add",
 *     "edit-form" = "/admin/niobi/niobi_application_workflow/niobi_conflict_of_interest_type/{niobi_conflict_of_interest_type}/edit",
 *     "delete-form" = "/admin/niobi/niobi_application_workflow/niobi_conflict_of_interest_type/{niobi_conflict_of_interest_type}/delete",
 *     "collection" = "/admin/niobi/niobi_application_workflow/niobi_conflict_of_interest_type"
 *   }
 * )
 */
class NiobiConflictOfInterestType extends ConfigEntityBundleBase implements NiobiConflictOfInterestTypeInterface {

  /**
   * The Conflict of Interest type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Conflict of Interest type label.
   *
   * @var string
   */
  protected $label;

}
