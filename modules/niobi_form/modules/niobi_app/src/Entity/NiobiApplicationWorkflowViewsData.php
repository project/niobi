<?php

namespace Drupal\niobi_app\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Niobi Application Workflow entities.
 */
class NiobiApplicationWorkflowViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
