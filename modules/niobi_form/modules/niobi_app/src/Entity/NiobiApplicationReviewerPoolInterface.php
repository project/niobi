<?php

namespace Drupal\niobi_app\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Niobi application reviewer pool entities.
 *
 * @ingroup niobi_app
 */
interface NiobiApplicationReviewerPoolInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Niobi application reviewer pool name.
   *
   * @return string
   *   Name of the Niobi application reviewer pool.
   */
  public function getName();

  /**
   * Sets the Niobi application reviewer pool name.
   *
   * @param string $name
   *   The Niobi application reviewer pool name.
   *
   * @return \Drupal\niobi_app\Entity\NiobiApplicationReviewerPoolInterface
   *   The called Niobi application reviewer pool entity.
   */
  public function setName($name);

  /**
   * Gets the Niobi application reviewer pool creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Niobi application reviewer pool.
   */
  public function getCreatedTime();

  /**
   * Sets the Niobi application reviewer pool creation timestamp.
   *
   * @param int $timestamp
   *   The Niobi application reviewer pool creation timestamp.
   *
   * @return \Drupal\niobi_app\Entity\NiobiApplicationReviewerPoolInterface
   *   The called Niobi application reviewer pool entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Niobi application reviewer pool published status indicator.
   *
   * Unpublished Niobi application reviewer pool are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Niobi application reviewer pool is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Niobi application reviewer pool.
   *
   * @param bool $published
   *   TRUE to set this Niobi application reviewer pool to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\niobi_app\Entity\NiobiApplicationReviewerPoolInterface
   *   The called Niobi application reviewer pool entity.
   */
  public function setPublished($published);

}
