<?php

namespace Drupal\niobi_app\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Conflict of Interest entities.
 *
 * @ingroup niobi_app
 */
interface NiobiConflictOfInterestInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Conflict of Interest name.
   *
   * @return string
   *   Name of the Conflict of Interest.
   */
  public function getName();

  /**
   * Sets the Conflict of Interest name.
   *
   * @param string $name
   *   The Conflict of Interest name.
   *
   * @return \Drupal\niobi_app\Entity\NiobiConflictOfInterestInterface
   *   The called Conflict of Interest entity.
   */
  public function setName($name);

  /**
   * Gets the Conflict of Interest creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Conflict of Interest.
   */
  public function getCreatedTime();

  /**
   * Sets the Conflict of Interest creation timestamp.
   *
   * @param int $timestamp
   *   The Conflict of Interest creation timestamp.
   *
   * @return \Drupal\niobi_app\Entity\NiobiConflictOfInterestInterface
   *   The called Conflict of Interest entity.
   */
  public function setCreatedTime($timestamp);


}
