<?php

namespace Drupal\niobi_app\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\niobi_form\Entity\NiobiForm;
use Drupal\user\UserInterface;

/**
 * Defines the Niobi Application Workflow Stage entity.
 *
 * @ingroup niobi_app
 *
 * @ContentEntityType(
 *   id = "niobi_application_workflow_stage",
 *   label = @Translation("Niobi Application Workflow Stage"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\niobi_app\NiobiApplicationWorkflowStageListBuilder",
 *     "views_data" = "Drupal\niobi_app\Entity\NiobiApplicationWorkflowStageViewsData",
 *     "translation" = "Drupal\niobi_app\NiobiApplicationWorkflowStageTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\niobi_app\Form\Entity\NiobiApplicationWorkflowStageForm",
 *       "add" = "Drupal\niobi_app\Form\Entity\NiobiApplicationWorkflowStageForm",
 *       "edit" = "Drupal\niobi_app\Form\Entity\NiobiApplicationWorkflowStageForm",
 *       "delete" = "Drupal\niobi_app\Form\Entity\NiobiApplicationWorkflowStageDeleteForm",
 *     },
 *     "access" = "Drupal\niobi_app\NiobiApplicationWorkflowStageAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\niobi_app\NiobiApplicationWorkflowStageHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "niobi_application_workflow_stage",
 *   data_table = "niobi_application_workflow_stage_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer niobi application workflow stage entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/application_workflow_stage/{niobi_application_workflow_stage}",
 *     "add-form" = "/application_workflow_stage/add",
 *     "edit-form" = "/application_workflow_stage/{niobi_application_workflow_stage}/edit",
 *     "delete-form" = "/application_workflow_stage/{niobi_application_workflow_stage}/delete",
 *     "collection" = "/admin/niobi/niobi_application_workflow_stage",
 *   },
 *   field_ui_base_route = "niobi_application_workflow_stage.settings"
 * )
 */
class NiobiApplicationWorkflowStage extends ContentEntityBase implements NiobiApplicationWorkflowStageInterface {

  use EntityChangedTrait;

  /**
   * @return mixed
   */
  public function getApplicationForm() {
    return $this->field_application->entity;
  }

  /**
   * @return mixed
   */
  public function getNominationForm() {
    return $this->field_nomination->entity;
  }

  /**
   * @return mixed
   */
  public function getRequiredAddendaForms() {
    $vals = $this->get('field_required_addenda')->getValue();
    $ret = [];
    foreach ($vals as $val) {
      $ret[$val['target_id']] = NiobiForm::load($val['target_id']);
    }
    return $ret;
  }

  /**
   * @return mixed
   */
  public function getOptionalAddendaForms() {
    $vals = $this->get('field_optional_addenda')->getValue();
    $ret = [];
    foreach ($vals as $val) {
      $ret[$val['target_id']] = NiobiForm::load($val['target_id']);
    }
    return $ret;
  }

  /**
   * @return mixed
   */
  public function getSupportLetterName() {
    return $this->field_support_letter_name->value;
  }

  /**
   * @return mixed
   */
  public function getSupportLetterForm() {
    return $this->field_support_letter_form->entity;
  }

  /**
   * @return mixed
   */
  public function getSupportLetterRequestForm() {
    return $this->field_support_letter_request->entity;
  }

  /**
   * @return array
   */
  public function getSupportLetterInformation() {
    $ret = [
      'name' => $this->field_support_letter_name->value,
      'count' => $this->field_number_of_support_letters->value,
      'form' => $this->field_support_letter_form->entity,
      'request_form' => $this->field_support_letter_request->entity,
    ];
    return $ret;
  }

  /**
   * @return mixed
   */
  public function getReviewForms($include_auto = FALSE) {
    $vals = $this->get('field_review')->getValue();
    $ret = [];
    foreach ($vals as $val) {
      $ret[$val['target_id']] = NiobiForm::load($val['target_id']);
    }
    if ($include_auto) {
      $vals = $this->get('field_auto_assign_review_form')->getValue();
      foreach ($vals as $val) {
        $ret[$val['target_id']] = NiobiForm::load($val['target_id']);
      }
    }

    return $ret;
  }

  /**
   * @return mixed
   */
  public function getReviewFormsAsNames() {
    $forms = $this->getReviewForms();
    foreach ($forms as $form) {
      $ret[$form->id()] = $form->label();
    }
    return $ret;
  }

  /**
   * @return mixed
   */
  public function getReviewerPools($include_auto = FALSE) {
    $vals = $this->get('field_reviewer_pools')->getValue();
    $ret = [];
    foreach ($vals as $val) {
      $ret[$val['target_id']] = NiobiApplicationReviewerPool::load($val['target_id']);
    }
    if ($include_auto) {
      $vals = $this->get('field_auto_assign_reviewer_pool')->getValue();
      foreach ($vals as $val) {
        $ret[$val['target_id']] = NiobiApplicationReviewerPool::load($val['target_id']);
      }
    }
    return $ret;
  }

  /**
   * @return bool|\Drupal\Core\Entity\EntityInterface|NiobiForm|null
   */
  public function getAutoAssignForm() {
    if(!empty($this->get('field_auto_assign_review_form')->getValue())) {
      return NiobiForm::load($this->get('field_auto_assign_review_form')->getValue()[0]['target_id']);
    }
    else {
      return FALSE;
    }
  }

  /**
   * @return bool|\Drupal\Core\Entity\EntityInterface|NiobiApplicationReviewerPool|null
   */
  public function getAutoAssignReviewerPool() {
    if(!empty($this->get('field_auto_assign_reviewer_pool')->getValue())) {
      return NiobiApplicationReviewerPool::load($this->get('field_auto_assign_reviewer_pool')->getValue()[0]['target_id']);
    }
    else {
      return FALSE;
    }
  }

  /**
   * @param NiobiApplication $niobi_application
   * @return array|bool
   */
  public function getReviewAutoAssignmentInfo(NiobiApplication $niobi_application) {
    if(!empty($this->get('field_auto_assign_review_form')->getValue()) && !empty($this->get('field_auto_assign_reviewer_pool')->getValue())) {
      $pools = $this->get('field_auto_assign_reviewer_pool')->getValue();
      if (isset($pools[0]['target_id'])) {
        $pool_id = $pools[0]['target_id'];
        $pool = NiobiApplicationReviewerPool::load($pool_id);
        return [
          'form' => NiobiForm::load($this->get('field_auto_assign_review_form')->getValue()[0]['target_id']),
          'pool' => $pool,
          'pool_count' => count($pool->getAutoAssignmentReviewers($niobi_application, $this)),
          'max' => $this->field_auto_assign_reivewer_max->value,
        ];
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * @param bool $assoc_array
   * @return array
   */
  public function getAllForms($assoc_array = TRUE) {
    $ret = [];
    if ($assoc_array) {
      $ret['nomination'] = [$this->getNominationForm()];
      $ret['application'] = [$this->getApplicationForm()];
      $ret['required_addenda'] = $this->getRequiredAddendaForms();
      $ret['optional_addenda'] = $this->getOptionalAddendaForms();
      $ret['support'] = [$this->getSupportLetterForm()];
      $ret['support_request'] = [$this->getSupportLetterRequestForm()];
      $ret['review'] = [$this->getReviewForms()];
      return $ret;
    }
    else {
      $ret = array_merge($ret, !empty($this->getNominationForm()) ? [$this->getNominationForm()] : []);
      $ret = array_merge($ret, !empty($this->getApplicationForm()) ? [$this->getApplicationForm()] : []);
      $ret = array_merge($ret, !empty($this->getSupportLetterForm()) ? [$this->getSupportLetterForm()] : []);
      $ret = array_merge($ret, !empty($this->getSupportLetterRequestForm()) ? [$this->getSupportLetterRequestForm()] : []);
      $ret = array_merge($ret, $this->getRequiredAddendaForms());
      $ret = array_merge($ret, $this->getOptionalAddendaForms());
      $ret = array_merge($ret, $this->getReviewForms());
      return $ret;
    }
  }

  /**
   * @return mixed
   */
  public function getAutoDecision() {
    $plugin_manager = \Drupal::service('plugin.manager.niobi_app_decision');
    $plugin_definitions = $plugin_manager->getDefinitions();
    $vals = $this->get('field_auto_decision')->getValue();
    if (isset($vals[0]['value'])) {
      return $plugin_definitions[$vals[0]['value']];
    }
    return FALSE;
  }

  /**
   * @return mixed
   */
  public function getDecisions() {
    $plugin_manager = \Drupal::service('plugin.manager.niobi_app_decision');
    $plugin_definitions = $plugin_manager->getDefinitions();
    $vals = $this->get('field_decisions_for_stage')->getValue();
    $ret = [];
    foreach ($vals as $val) {
      $ret[$val['value']] = $plugin_definitions[$val['value']];
    }
    return $ret;
  }

  /**
   * @return mixed
   */
  public function getDecisionsAsNames() {
    $decisions = $this->getDecisions();
    foreach ($decisions as $key => $decision) {
      $decisions[$key] = $decision['label'];
    }
    return $decisions;
  }

  /**
   * @return mixed
   */
  public function getParentWorkflow($load = TRUE) {
    $workflow_id = $this->get('field_parent_workflow')->getValue();
    $workflow_id =  isset($workflow_id[0]['target_id']) ? $workflow_id[0]['target_id'] : FALSE;
    if ($workflow_id) {
      return $load ? NiobiApplicationWorkflow::load($workflow_id) : $workflow_id;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Checks if application workflow meets minimum requirements for use.
   * @return bool
   */
  public function isReadyToUse() {
    $form_set = $this->getAllForms();
    foreach ($form_set as $step => $niobi_forms) {
      foreach ($niobi_forms as $niobi_form) {
        if (is_array($niobi_form)) {
          foreach ($niobi_form as $nf) {
            if (!empty($nf) && !$nf->getWebform()) {
              return FALSE;
            }
          }
        }
        elseif (!empty($niobi_form)) {
          if (!$niobi_form->getWebform()) {
            return FALSE;
          }
        }
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Niobi Application Workflow Stage entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Niobi Application Workflow Stage entity.'))
      ->setSettings([
        'max_length' => 511,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Niobi Application Workflow Stage is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
