<?php

namespace Drupal\niobi_app\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Niobi Application entities.
 *
 * @ingroup niobi_app
 */
interface NiobiApplicationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Niobi Application name.
   *
   * @return string
   *   Name of the Niobi Application.
   */
  public function getName();

  /**
   * Sets the Niobi Application name.
   *
   * @param string $name
   *   The Niobi Application name.
   *
   * @return \Drupal\niobi_app\Entity\NiobiApplicationInterface
   *   The called Niobi Application entity.
   */
  public function setName($name);

  /**
   * Gets the Niobi Application creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Niobi Application.
   */
  public function getCreatedTime();

  /**
   * Sets the Niobi Application creation timestamp.
   *
   * @param int $timestamp
   *   The Niobi Application creation timestamp.
   *
   * @return \Drupal\niobi_app\Entity\NiobiApplicationInterface
   *   The called Niobi Application entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Niobi Application published status indicator.
   *
   * Unpublished Niobi Application are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Niobi Application is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Niobi Application.
   *
   * @param bool $published
   *   TRUE to set this Niobi Application to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\niobi_app\Entity\NiobiApplicationInterface
   *   The called Niobi Application entity.
   */
  public function setPublished($published);

}
