<?php

namespace Drupal\niobi_app\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Conflict of Interest type entities.
 */
interface NiobiConflictOfInterestTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
