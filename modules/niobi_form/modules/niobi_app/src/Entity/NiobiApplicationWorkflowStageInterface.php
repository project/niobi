<?php

namespace Drupal\niobi_app\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Niobi Application Workflow Stage entities.
 *
 * @ingroup niobi_app
 */
interface NiobiApplicationWorkflowStageInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Niobi Application Workflow Stage name.
   *
   * @return string
   *   Name of the Niobi Application Workflow Stage.
   */
  public function getName();

  /**
   * Sets the Niobi Application Workflow Stage name.
   *
   * @param string $name
   *   The Niobi Application Workflow Stage name.
   *
   * @return \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStageInterface
   *   The called Niobi Application Workflow Stage entity.
   */
  public function setName($name);

  /**
   * Gets the Niobi Application Workflow Stage creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Niobi Application Workflow Stage.
   */
  public function getCreatedTime();

  /**
   * Sets the Niobi Application Workflow Stage creation timestamp.
   *
   * @param int $timestamp
   *   The Niobi Application Workflow Stage creation timestamp.
   *
   * @return \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStageInterface
   *   The called Niobi Application Workflow Stage entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Niobi Application Workflow Stage published status indicator.
   *
   * Unpublished Niobi Application Workflow Stage are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Niobi Application Workflow Stage is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Niobi Application Workflow Stage.
   *
   * @param bool $published
   *   TRUE to set this Niobi Application Workflow Stage to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStageInterface
   *   The called Niobi Application Workflow Stage entity.
   */
  public function setPublished($published);

}
