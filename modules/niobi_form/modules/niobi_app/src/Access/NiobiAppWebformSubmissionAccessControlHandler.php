<?php
namespace Drupal\niobi_app\Access;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform\WebformSubmissionAccessControlHandler;

class NiobiAppWebformSubmissionAccessControlHandler extends WebformSubmissionAccessControlHandler {
  public function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $accessResult = parent::checkAccess($entity, $operation, $account);

    // if user are not allowed to view the submission,
    // then check if user is able to view the application or not.
    if ($operation === 'view' && !$accessResult->isAllowed()) {
      $applicationId = $entity->getElementData('application_id');
      if (!empty($applicationId)) {
        $application = current(\Drupal::entityTypeManager()
          ->getStorage('niobi_application')
          ->loadByProperties(['uuid' => $applicationId]));
        if (!empty($application)) {
          return $application->access($operation, $account, TRUE);
        }
      }
    }

    return $accessResult;
  }
}
