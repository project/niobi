<?php

namespace Drupal\niobi_app\Access;

use Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage;
use Drupal\niobi_form\Entity\NiobiForm;

/**
 * Trait NiobiAppWebformAccessTrait
 * @package Drupal\niobi_app\Access
 */
trait NiobiAppWebformAccessTrait {

  /**
   * @param string $webform_id
   *   The webform ID
   * @return bool|int
   */
  public function getWorkflowForWebform($webform_id = '') {
    if ($webform_id) {
      // First, get the Niobi Form
      $query = \Drupal::entityQuery('niobi_form');
      $query->condition('field_form', $webform_id);
      $result = NiobiForm::loadMultiple($query->execute());
      $niobi_form = array_shift($result);

      if ($niobi_form) {
        $nfid = $niobi_form->id();
        $query = \Drupal::entityQuery('niobi_application_workflow_stage');
        $group = $query->orConditionGroup();
        $group->condition('field_nomination', $nfid);
        $group->condition('field_application', $nfid);
        $group->condition('field_required_addenda', $nfid);
        $group->condition('field_optional_addenda', $nfid);
        $group->condition('field_support_letter_form', $nfid);
        $group->condition('field_support_letter_request', $nfid);
        $group->condition('field_review', $nfid);
        $group->condition('field_auto_assign_review_form', $nfid);
        $query->condition($group);
        $result = $query->execute();
        $stages = NiobiApplicationWorkflowStage::loadMultiple($result);
        $stage = array_shift($stages);
        /** @var $stage NiobiApplicationWorkflowStage */
        return $stage ? $stage->getParentWorkflow(FALSE) : FALSE;
      }
    }
    // We failed one of the earlier if statements.
    return FALSE;
  }
}
