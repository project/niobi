<?php

namespace Drupal\niobi_app\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines whether a manual review can be created.
 * Class NiobiAppManualReview
 * @package Drupal\niobi_app\Access
 */
class NiobiAppManualReview implements AccessInterface {

  /**
   * @param Route $route
   * @param RouteMatchInterface $route_match
   * @param AccountInterface $account
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $application = $route_match->getParameter('niobi_application');
    if($application) {
      /* @var $application \Drupal\niobi_app\Entity\NiobiApplication */
      $status = $application->getCurrentApplicationStatus();
      return $status === 'review' ? AccessResult::allowed() : AccessResult::forbidden();
    }
    // If this isn't an application, this check doesn't apply.
    return AccessResult::allowed();
  }

}
