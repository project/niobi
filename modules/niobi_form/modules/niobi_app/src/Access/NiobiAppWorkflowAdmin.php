<?php

namespace Drupal\niobi_app\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\niobi_app\NiobiAppUtilities;
use Symfony\Component\Routing\Route;

/**
 * Class NiobiAppWorkflowAdmin
 * @package Drupal\niobi_app\Access
 */
class NiobiAppWorkflowAdmin implements AccessInterface {

  /**
   * @param Route $route
   * @param RouteMatchInterface $route_match
   * @param AccountInterface $account
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $workflow_id = $route_match->getParameter('niobi_application_workflow');
    // Handle when the object is a workflow entity.
    $workflow_id = is_object($workflow_id) ? $workflow_id->id() : $workflow_id;
    if (empty($workflow_id)) {
      $app = $route_match->getParameter('niobi_application');
      if ($app) {
        $app = is_object($app) ? $app : NiobiApplication::load($app);
        $workflow_id = $app->getApplicationWorkflow()->id();
      }
    }
    if($workflow_id) {
      $is_admin = NiobiAppUtilities::isWorkflowAdmin($workflow_id, $account);
      return $is_admin ? AccessResult::allowed() : AccessResult::forbidden();
    }
    // If this isn't an application workflow or related view, this check doesn't apply.
    return AccessResult::allowed();
  }

}
