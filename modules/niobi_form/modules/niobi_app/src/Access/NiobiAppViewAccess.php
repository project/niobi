<?php

namespace Drupal\niobi_app\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\niobi_app\NiobiAppUtilities;
use Symfony\Component\Routing\Route;

/**
 * Class NiobiAppViewAccess
 * @package Drupal\niobi_app\Access
 */
class NiobiAppViewAccess implements AccessInterface {

  use NiobiAppWebformAccessTrait;

  /**
   * @param Route $route
   * @param RouteMatchInterface $route_match
   * @param AccountInterface $account
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $workflow_id = $route_match->getParameter('niobi_application_workflow');
    $workflow_id = is_object($workflow_id) ? $workflow_id->id() : $workflow_id;
    // If the workflow isn't set, try the application.
    if (empty($workflow_id)) {
      $app = $route_match->getParameter('niobi_application');
      if ($app) {
        $app = is_object($app) ? $app : NiobiApplication::load($app);
        $workflow_id = $app->getApplicationWorkflow()->id();
      }
    }
    // Now try a webform argument.
    if (empty($workflow_id)) {
      $webform = $route_match->getParameter('webform');
      if ($webform) {
        $workflow_id = $this->getWorkflowForWebform($webform->id());
      }
    }
    // Proceed if we found a workflow ID.
    if($workflow_id) {
      // Handle when the object is a workflow entity.
      if (is_object($workflow_id)) {
        $workflow_id = $workflow_id->id();
      }
      $is_admin = NiobiAppUtilities::hasViewAccess($workflow_id, $account);
      return $is_admin ? AccessResult::allowed() : AccessResult::forbidden();
    }
    // If this isn't an application workflow or related view, this check doesn't apply.
    return AccessResult::allowed();
  }

}
