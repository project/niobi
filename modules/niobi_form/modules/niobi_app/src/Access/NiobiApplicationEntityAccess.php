<?php

namespace Drupal\niobi_app\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\task\Entity\Task;

class NiobiApplicationEntityAccess {
  public static function checkDoReviewAccess(Task $task) {
    $type = current($task->get('type')->getValue());
    if (!empty($type['target_id']) && $type['target_id'] === 'application_review_task') {
      $application = current($task->field_application->referencedEntities());
      if (!empty($application)) {
        return AccessResult::allowedIf($application->isReviewer() || $application->isApplicationAdmin());
      }
    }

    return AccessResult::neutral();
  }
}
