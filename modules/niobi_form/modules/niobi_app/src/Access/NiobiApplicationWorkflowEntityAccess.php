<?php

namespace Drupal\niobi_app\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\niobi_app\Entity\NiobiApplicationReviewerPool;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;

/**
 * Class NiobiApplicationWorkflowEntityAccess
 *
 * @package Drupal\niobi_app\Access
 */
class NiobiApplicationWorkflowEntityAccess {
  public static function checkDoReviewAccess(NiobiApplicationWorkflow $niobi_application_workflow) {
    // if current user is in reviewer pool of the workflow, then users can do review in the workflow
    $stages = \Drupal::entityTypeManager()->getStorage('niobi_application_workflow_stage')
      ->loadByProperties(['field_parent_workflow' => $niobi_application_workflow->id()]);
    $reviewerPoolIds = [];
    foreach ($stages as $stage) {
      $v1 = $stage->field_reviewer_pools->getValue();
      $v2 = $stage->field_auto_assign_reviewer_pool->getValue();
      foreach ($v1 + $v2 as $v) {
        $reviewerPoolIds[] = $v['target_id'];
      }
    }

    $reviewPools = NiobiApplicationReviewerPool::loadMultiple($reviewerPoolIds);
    $user = \Drupal::currentUser();
    foreach ($reviewPools as $pool) {
      if ($pool->containUser($user)) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::neutral();
  }
}
