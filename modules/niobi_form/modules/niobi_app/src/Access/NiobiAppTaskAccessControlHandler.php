<?php
namespace Drupal\niobi_app\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\task\TaskAccessControlHandler;

class NiobiAppTaskAccessControlHandler extends TaskAccessControlHandler {
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\task\Entity\TaskInterface $entity */
    $accessResult = parent::checkAccess($entity, $operation, $account);

    // only check application review task
    $type = current($entity->get('type')->getValue());
    if (!empty($type['target_id']) && $type['target_id'] === 'application_review_task' && $accessResult->isAllowed()) {
      // check if current user is a reviewer
      $assigneeId = current($entity->assigned_to->getValue());
      if (!empty($assigneeId['target_id']) && $assigneeId['target_id'] === $account->id()) {
        return AccessResult::allowed();
      }

      // check if current user is a workflow admin
      $application = current($entity->field_application->referencedEntities());
      if (!empty($application) && $application->isApplicationAdmin()) {
        return AccessResult::allowed();
      }

      return AccessResult::neutral();
    }

    return $accessResult;
  }
}
