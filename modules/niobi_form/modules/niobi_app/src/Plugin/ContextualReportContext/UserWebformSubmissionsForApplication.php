<?php

namespace Drupal\niobi_app\Plugin\ContextualReportContext;


/**
 * A user's webform submissions for an application workflow
 * @ContextualReportContext (
 *   id = "niobi_app_user_submissions",
 *   label = "User's Webform Submissions For Application Workflow",
 * )
 */
class UserWebformSubmissionsForApplication extends WorkflowSubmissionsBase {

  /**
   * @param $workflow
   * @return array
   */
  public static function getWorkflowSubmissions($workflow) {
    $stages = $workflow->getStages();
    foreach ($stages as $stage) {
      /* @var $stage \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage */
      $forms = $stage->getAllForms(FALSE);
      foreach ($forms as $form) {
        /* @var $form \Drupal\niobi_form\Entity\NiobiForm */
        $webform = $form->getWebform();
        $query = \Drupal::entityQuery('webform_submission');
        $query->condition('webform_id', $webform->id());
        $query->condition('uid', \Drupal::currentUser()->id());
        $query->sort('created', 'DESC');

        $result = $query->execute();
        return ['webform_submission' => $result];
      }
    }
  }

//  /**
//   * @param array $entity_ids
//   * @param array $params
//   * @return array
//   */
//  public static function generateReportData(array $entity_ids, array $params = []) {
//    return parent::generateReportData($entity_ids);
//  }

  /**
   * @param array $params
   * @return array
   */
  public static function getEntities(array $params = []) {
    return parent::getEntities($params);
  }

}
