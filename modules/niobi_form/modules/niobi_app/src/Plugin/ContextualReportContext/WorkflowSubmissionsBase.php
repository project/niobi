<?php

namespace Drupal\niobi_app\Plugin\ContextualReportContext;

use Drupal\contextual_reports\Plugin\ContextualReportContextBase;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;

/**
 * A user's webform submissions for an application workflow
 * @ContextualReportContext (
 *   id = "niobi_app_user_submissions",
 *   label = "User's Webform Submissions For Application Workflow",
 * )
 */
class WorkflowSubmissionsBase extends ContextualReportContextBase {

  /**
   * @param $workflow
   * @return array
   */
  public static function getWorkflowSubmissions($workflow) {
    return ['webform_submission' => []];
  }

  /**
   * @param array $params
   * @return array
   */
  public static function getEntities(array $params = []) {
    // A workflow should have been provided.
    if (!empty($params['application_workflow'])) {
      /* @var $workflow \Drupal\niobi_app\Entity\NiobiApplicationWorkflow */
      $workflow = NiobiApplicationWorkflow::load($params['application_workflow']);
      $submissions = get_called_class()::getWorkflowSubmissions($workflow);
      return $submissions;
    }
    else {
      return [];
    }
  }

}
