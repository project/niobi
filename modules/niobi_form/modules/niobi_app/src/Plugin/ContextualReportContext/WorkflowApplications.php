<?php

namespace Drupal\niobi_app\Plugin\ContextualReportContext;

use Drupal\contextual_reports\Plugin\ContextualReportContextBase;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;
use Drupal\webform\Entity\WebformSubmission;

/**
 * A user's webform submissions for an application workflow
 * @ContextualReportContext (
 *   id = "niobi_app_workflow_applications",
 *   label = "Applications for a given workflow",
 * )
 */
class WorkflowApplications extends ContextualReportContextBase {

  /**
   * @param array $params
   * @return array
   */
  public static function getEntities(array $params = []) {
    // A workflow should have been provided.
    if (!empty($params['application_workflow'])) {
      $query = \Drupal::entityQuery('niobi_application');
      $query->condition('field_application_workflow', $params['application_workflow']);
      if (!(isset($params['include_complete']) && $params['include_complete'])) {
        $query->condition('field_application_status', 'complete', '<>');
      }
      return ['niobi_application' => $query->execute()];
    }
    else {
      return [];
    }
  }

}
