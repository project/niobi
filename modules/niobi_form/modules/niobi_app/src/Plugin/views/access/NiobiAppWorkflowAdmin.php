<?php

namespace Drupal\niobi_app\Plugin\views\access;

use Drupal\Core\Session\AccountInterface;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;
use Drupal\niobi_app\NiobiAppUtilities;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Symfony\Component\Routing\Route;

/**
 * Access plugin that provides no access control at all.
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "niobi_app_workflow_admin",
 *   title = @Translation("User is an Application Workflow Admin"),
 *   help = @Translation("Access to reports")
 * )
 */
class NiobiAppWorkflowAdmin extends AccessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    return $this->t('User is an Application Workflow Admin');
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    $workflow_id = \Drupal::routeMatch()->getParameter('niobi_application_workflow');

    if ($workflow_id instanceof NiobiApplicationWorkflow) {
      $workflow_id = $workflow_id->id();
    }

    if($workflow_id) {
      return NiobiAppUtilities::isWorkflowAdmin($workflow_id, $account);
    }
    // If we don't have a workflow ID, then we can't grant access.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    // We don't have anything to change at this time.
    $route->setRequirement('_niobi_app_workflow_admin', 'TRUE');
  }

}
