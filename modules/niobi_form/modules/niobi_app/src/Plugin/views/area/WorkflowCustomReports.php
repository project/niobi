<?php

namespace Drupal\niobi_app\Plugin\views\area;

use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;
use Drupal\views\Plugin\views\area\TokenizeAreaPluginBase;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Defines a views area plugin.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("niobi_app_workflow_custom_reports")
 */
class WorkflowCustomReports extends TokenizeAreaPluginBase {

  /**
   * Define the available options.
   *
   * @return array
   *   Array of available options for views_add_button form.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['title'] = ['default' => ''];
    $options['workflow_id'] = ['default' => ''];
    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#description' => t('Enter HTML for a title. This field supports tokens if enabled.'),
      '#default_value' => $this->options['title'],
      '#weight' => -9,
    ];
    $form['workflow_id'] = [
      '#type' => 'textfield',
      '#title' => t('Workflow ID'),
      '#description' => t('Certain entities require a special context parameter. Set the context (or relevant 
      token) here. Check the help for the relevant Views Add Button module for further questions.'),
      '#default_value' => $this->options['workflow_id'],
      '#weight' => -9,
    ];
    $this->tokenForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {

    // Get fields.
    $workflow_id = $this->options['tokenize'] ? $this->tokenizeValue($this->options['workflow_id']) : $this->options['workflow_id'];
    if (is_numeric($workflow_id)) {
      $workflow = NiobiApplicationWorkflow::load($workflow_id);
      if ($workflow) {
        $fields = $workflow->get('field_custom_reports')->getValue();
        if (!empty($fields)) {
          $title = $this->options['tokenize'] ? $this->tokenizeValue($this->options['title']) : $this->options['title'];
          $ret = ['<div>'];
          $ret[] = $title;
          $ret_fields = [];
          // Turn each field into a link.
          foreach ($fields as $field) {
            $uri = $field['uri'];
            $title = !empty($field['title']) ? $field['title'] : $field['uri'];
            $options = !empty($field['options']) ? $field['options'] : [];
            $url = Url::fromUri($uri, $options);
            $link = Link::fromTextAndUrl($title, $url);
            $ret_fields[] = $link->toString()->getGeneratedLink();
          }
          // Implode array into a br separated list.
          // TODO add option for creating a ul or ol list.
          $ret_fields = implode('<br>', $ret_fields);
          $ret = array_merge($ret, [$ret_fields], ['</div>']);
          $ret = implode('', $ret);
          return ['#type' => 'markup', '#markup' => $ret];
        }
      }
    }
    return ['#type' => 'markup', '#markup' => ''];
  }

}
