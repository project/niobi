<?php

namespace Drupal\niobi_app\Plugin\views\area;

use Drupal\views\Plugin\views\area\TokenizeAreaPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;

/**
 * Defines a views area plugin.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("niobi_app_instructions")
 */
class NiobiApplicationInstructions extends TokenizeAreaPluginBase {

  public $incompleteSetupMessage = 'This application has not be set up fully, please contact the application administrator.';

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }


  /**
   * Define the available options.
   *
   * @return array
   *   Array of available options for niobi_app form.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $return = '';
    if($this->view->total_rows) {
      $workflow_id = !empty($this->view->args) ? $this->view->args[0] : NULL;
      if ($workflow_id) {
        $workflow = NiobiApplicationWorkflow::load($workflow_id);
        if ($workflow->isReadyToUse()) {
          $text = $workflow->get('field_applicant_instructions')->getValue();
          if (isset($text[0]['value'])) {
            return ['#markup' => $text[0]['value']];
          }
          else {
            return ['#markup' => ''];
          }
        }
      }
      \Drupal::messenger()->addError($this->incompleteSetupMessage);
    }
  }

}
