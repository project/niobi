<?php

/**
 * @file
 */

namespace Drupal\niobi_app\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\NiobiAppUtilities;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * @ViewsField("niobi_app_review_assignments")
 */
class NiobiApplicationReviewAssignments extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['view'] = ['default' => ''];
    $options['display'] = ['default' => 'default'];
    $options['arguments'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['niobi_app'] = [
      '#type' => 'details',
      '#title' => $this->t("View settings"),
      '#open' => TRUE,
    ];


  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->addAdditionalFields();
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $output = NULL;
    $application = $values->_entity;
    $workflow = $application->getApplicationWorkflow();
    /* @var $application \Drupal\niobi_app\Entity\NiobiApplication */
    return NiobiAppUtilities::generateReviewAssignmentRenderArray($application, $workflow);
  }

}
