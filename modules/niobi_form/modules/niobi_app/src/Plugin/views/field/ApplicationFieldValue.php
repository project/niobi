<?php

namespace Drupal\niobi_app\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a views field plugin.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("niobi_app_application_field_value")
 */
class ApplicationFieldValue extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * Define the available options.
   *
   * @return array
   *   Array of available options for views_add_button form.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['field_key'] = ['default' => ''];
    $options['duplicates'] = ['default' => FALSE];
    $options['allowed_forms'] = ['default' => ''];
    return $options;
  }

  /**
   * Provide the options form.
   * @param $form
   * @param FormStateInterface $form_state
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['field_key'] = [
      '#type' => 'textfield',
      '#title' => t('Field key to show'),
      '#description' => t('If this key is found in the application webform, it will be listed.'),
      '#default_value' => $this->options['field_key'],
    ];
    $form['duplicates'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow Duplicates'),
      '#description' => t('If multiple application webforms were submitted (which should not happen), show all values. Otherwise only the first non-empty value is shown'),
      '#default_value' => $this->options['duplicates'],
    ];
    $form['allowed_forms'] = [
      '#type' => 'textfield',
      '#title' => t('Use these forms instead'),
      '#description' => t('Instead of using the workflow stage entity reference, use the listed forms. Use webform form keys.'),
      '#default_value' => $this->options['allowed_forms'],
    ];
    $form['style_settings']['#attributes']['style'] = 'display:none;';
    $form['element_type_enable']['#attributes']['style'] = 'display:none;';
    $form['element_type']['#attributes']['style'] = 'display:none;';
    $form['element_class_enable']['#attributes']['style'] = 'display:none;';
    $form['element_class']['#attributes']['style'] = 'display:none;';
    $form['element_label_type_enable']['#attributes']['style'] = 'display:none;';
    $form['element_label_type']['#attributes']['style'] = 'display:none;';
    $form['element_label_class_enable']['#attributes']['style'] = 'display:none;';
    $form['element_label_class']['#attributes']['style'] = 'display:none;';
    $form['element_wrapper_type_enable']['#attributes']['style'] = 'display:none;';
    $form['element_wrapper_type']['#attributes']['style'] = 'display:none;';
    $form['element_wrapper_class_enable']['#attributes']['style'] = 'display:none;';
    $form['element_wrapper_class']['#attributes']['style'] = 'display:none;';
    $form['element_default_classes']['#attributes']['style'] = 'display:none;';
    $form['alter']['#attributes']['style'] = 'display:none;';
    $form['empty_field_behavior']['#attributes']['style'] = 'display:none;';
    $form['empty']['#attributes']['style'] = 'display:none;';
    $form['empty_zero']['#attributes']['style'] = 'display:none;';
    $form['hide_empty']['#attributes']['style'] = 'display:none;';
    $form['hide_alter_empty']['#attributes']['style'] = 'display:none;';
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $ret = '';
    /* @var $application \Drupal\niobi_app\Entity\NiobiApplication */
    $application = $values->_entity;
    $status = $application->getCurrentApplicationStatus();
    $app_forms = [$application->getCurrentApplicationStage()->getApplicationForm()];

    // Get the valid webforms for scoring.
    $webforms = [];
    if (!empty($this->options['allowed_forms'])) {
      $form_ids = explode(',', $this->options['allowed_forms']);
      foreach ($form_ids as $form_id) {
        $webforms[trim($form_id)] = trim($form_id);
      }
    }
    else {
      foreach ($app_forms as $app_form) {
        /* @var $webform \Drupal\webform\Entity\Webform */
        /* @var $app_form \Drupal\niobi_form\Entity\NiobiForm */
        $webform = $app_form->getWebform();
        if ($webform) {
          $webforms[$webform->id()] = $webform->id();
        }
      }
    }
    // Now, filter the submissions
    $app_subs = $application->getApplicationSubmissions();
    $report_subs = [];
    foreach ($app_subs as $sub) {
      /* @var $sub \Drupal\webform\Entity\WebformSubmission */
      $sub_form = !empty($sub) ? $sub->getWebform() : FALSE;
      $id = !empty($sub_form) ? $sub_form->id() : 0;
      if (isset($webforms[$id])) {
        $report_subs[$sub->id()] = $sub;
      }
    }

    $field_key = $this->options['field_key'];
    if (!empty($report_subs)) {
      foreach ($report_subs as $sub) {
        if ($sub) {
          $sub_data = $sub->getData();
          $author = $sub->getOwner();
          $webform = $sub->getWebform();
          $ret_data = $sub_data[$field_key];
          $ret_data = is_array($ret_data) ? implode(', ', $ret_data) : $ret_data;
          if ($this->options['duplicates']) {
            $ret .= $ret_data . '<br>';
          }
          else {
            $ret = empty($ret) ? $ret_data : $ret;
          }
        }
      }
    }
    elseif (empty($report_subs) && empty($ret)) {
      $ret = t('No data found for this application');
    }
    elseif (empty($ret)) {
      $ret = t('No valid data found for this application');
    }

    // Return the report
    return ['#markup' => $ret];
  }

}
