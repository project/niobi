<?php

namespace Drupal\niobi_app\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a views field plugin.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("niobi_app_review_field_value")
 */
class ReviewFieldValue extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * Define the available options.
   *
   * @return array
   *   Array of available options for views_add_button form.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['field_key'] = ['default' => ''];
    $options['allowed_forms'] = ['default' => ''];
    return $options;
  }

  /**
   * Provide the options form.
   * @param $form
   * @param FormStateInterface $form_state
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['field_key'] = [
      '#type' => 'textfield',
      '#title' => t('Field key to score'),
      '#description' => t('If this key is found in the webform we are using for scoring, it will be listed.'),
      '#default_value' => $this->options['field_key'],
    ];
    $form['allowed_forms'] = [
      '#type' => 'textfield',
      '#title' => t('Use these forms instead'),
      '#description' => t('Instead of using the workflow stage entity reference, use the listed forms. Use webform form keys.'),
      '#default_value' => $this->options['allowed_forms'],
    ];
    $form['style_settings']['#attributes']['style'] = 'display:none;';
    $form['element_type_enable']['#attributes']['style'] = 'display:none;';
    $form['element_type']['#attributes']['style'] = 'display:none;';
    $form['element_class_enable']['#attributes']['style'] = 'display:none;';
    $form['element_class']['#attributes']['style'] = 'display:none;';
    $form['element_label_type_enable']['#attributes']['style'] = 'display:none;';
    $form['element_label_type']['#attributes']['style'] = 'display:none;';
    $form['element_label_class_enable']['#attributes']['style'] = 'display:none;';
    $form['element_label_class']['#attributes']['style'] = 'display:none;';
    $form['element_wrapper_type_enable']['#attributes']['style'] = 'display:none;';
    $form['element_wrapper_type']['#attributes']['style'] = 'display:none;';
    $form['element_wrapper_class_enable']['#attributes']['style'] = 'display:none;';
    $form['element_wrapper_class']['#attributes']['style'] = 'display:none;';
    $form['element_default_classes']['#attributes']['style'] = 'display:none;';
    $form['alter']['#attributes']['style'] = 'display:none;';
    $form['empty_field_behavior']['#attributes']['style'] = 'display:none;';
    $form['empty']['#attributes']['style'] = 'display:none;';
    $form['empty_zero']['#attributes']['style'] = 'display:none;';
    $form['hide_empty']['#attributes']['style'] = 'display:none;';
    $form['hide_alter_empty']['#attributes']['style'] = 'display:none;';
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $ret = '';
    /* @var $application \Drupal\niobi_app\Entity\NiobiApplication */
    $application = $values->_entity;
    $status = $application->getCurrentApplicationStatus();
    $review_forms = $application->getCurrentApplicationStage()->getReviewForms(TRUE);

    if (in_array($status, ['decision', 'review'])) {
      // Get the valid webforms for scoring.
      $webforms = [];
      if (!empty($this->options['allowed_forms'])) {
        $form_ids = explode(',', $this->options['allowed_forms']);
        foreach ($form_ids as $form_id) {
          $webforms[trim($form_id)] = trim($form_id);
        }
      }
      else {
        foreach ($review_forms as $review_form) {
          /* @var $webform \Drupal\webform\Entity\Webform */
          /* @var $review_form \Drupal\niobi_form\Entity\NiobiForm */
          $webform = $review_form->getWebform();
          if ($webform) {
            $webforms[$webform->id()] = $webform->id();
          }
        }
      }
      // Now, filter the submissions
      $review_subs = $application->getReviewSubmissions();
      $score_subs = [];
      foreach ($review_subs as $sub) {
        /* @var $sub \Drupal\webform\Entity\WebformSubmission */
        $sub_form = !empty($sub) ? $sub->getWebform() : FALSE;
        $id = !empty($sub_form) ? $sub_form->id() : 0;
        if (isset($webforms[$id])) {
          $score_subs[$sub->id()] = $sub;
        }
      }

      $field_key = $this->options['field_key'];
      if (!empty($score_subs)) {
        foreach ($score_subs as $sub) {
          if ($sub) {
            $sub_data = $sub->getData();
            $author = $sub->getOwner();
            $webform = $sub->getWebform();
            $ret .= $author->getAccountName() . ', ' . $webform->label() . ': ' . $sub_data[$field_key] . '<br>';
          }
        }
      }
      elseif (empty($score_subs) && empty($ret)) {
        $ret = t('No reviews found for this application');
      }
      elseif (empty($ret)) {
        $ret = t('No valid reviews found for this application');
      }

    }
    else {
      $ret = t('The application must have an "In Review" or "Decision Pending" status to have a reviewer score');
    }

    // Return the report
    return ['#markup' => $ret];
  }

}
