<?php

/**
 * @file
 * Contains \Drupal\niobi_app\Plugin\views\field\View.
 */

namespace Drupal\niobi_app\Plugin\views\field;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\webform\Entity\WebformSubmission;

/**
 * @ViewsField("niobi_app_admin_status_actions")
 */
class NiobiApplicationAdminStatusActions extends FieldPluginBase {

  /**
   * @param $route
   * @param array $params
   * @return mixed
   */
  public static function checkRouteAccess($route, array $params) {
    $accessManager = \Drupal::service('access_manager');
    return $accessManager->checkNamedRoute($route, $params, \Drupal::currentUser());
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['view'] = ['default' => ''];
    $options['display'] = ['default' => 'default'];
    $options['arguments'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['niobi_app'] = [
      '#type' => 'details',
      '#title' => $this->t("View settings"),
      '#open' => TRUE,
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->addAdditionalFields();
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $application = $values->_entity;
    $workflow = $application->getApplicationWorkflow();

    if ($workflow->isReadyToUse()) {
      $status = $application->field_application_status->value;
      if (!empty($status)) {
        // based on the status of the current workflow render different template
        switch ($status) {
          case 'nomination':
            $submissions = $application->field_nomination_submissions->referencedEntities();
            $complete_status = false;
            if (!empty($submissions)) {
              $complete_status = true;
              foreach ($submissions as $sub) {
                if ($sub->isDraft()) {
                  $complete_status = false;
                }
              }
            }

            return [
              '#theme' => 'niobi_application_admin_status_actions_nomination',
              '#complete_status' => $complete_status
            ];

          case 'nomination_claim':
            return [
              '#theme' => 'niobi_application_admin_status_actions_nomination_claim'
            ];

          case 'review':
            $params = ['niobi_application' => $application->id()];
            $route = 'niobi_app.move_to_decision';
            $canAccess = $this->checkRouteAccess($route, $params);

            return [
              '#theme' => 'niobi_application_admin_status_actions_review',
              '#application' => $application,
              '#canAccess' => !empty($canAccess) ? $canAccess : FALSE
            ];

          case 'decision':
            $decisions = $application->getCurrentApplicationStage()->getDecisionsAsNames();
            $arrCanAccess = [];
            foreach ($decisions as $key => $decision) {
              $params = ['niobi_application' => $application->id(), 'decision_plugin' => $key];
              $route = 'niobi_app.decision_form';
              $arrCanAccess[] = $this->checkRouteAccess($route, $params);
            }

            return [
              '#theme' => 'niobi_application_admin_status_actions_decision',
              '#application' => $application,
              '#decisions' => $decisions,
              '#arrCanAccess' => $arrCanAccess
            ];

          case 'complete':
            $decision = $application->field_decision->value;

            return [
              '#theme' => 'niobi_application_admin_status_actions_complete',
              '#decision' => $decision
            ];

          case 'application':
            $completeStatus = $this->applicationCompleteStatus($application);
            if ($completeStatus) {
              $params = [
                'niobi_application_workflow' => $workflow->id(),
                'niobi_application' => $application->id(),
              ];
              $route = 'niobi_app.move_to_review';
              $canAccess = $this->checkRouteAccess($route, $params);
            }
            else {
              $stage = $application->getCurrentApplicationStage();
              $stage_forms = $stage->getAllForms();
              $subs = $application->getAllSubmissionsbyWebform();
              $isApplicationCompleted = $this->checkApplicationCompleteStatus($application, $stage, $stage_forms, $subs);
              $isRequiredAddendaCompleted = $this->checkRequiredAddendaCompleteStatus($application, $stage, $stage_forms, $subs);
              $isSupportLetterCompleted = $this->checkSupportLetterCompleteStatus($application, $stage, $stage_forms, $subs);

              if (!$isRequiredAddendaCompleted) {
                $requiredAddendaForms = [];
                foreach ($stage_forms['required_addenda'] as $f) {
                  $webformId = $f->getWebform()->id();
                  $status = FALSE;
                  if (!empty($subs['application'])) {
                    foreach ($subs['application'] as $key => $value) {
                      if ($key === $webformId) {
                        $sub = current($value);
                        if ($sub->isCompleted()) {
                          $status = TRUE;
                          break;
                        }
                      }
                    }
                  }
                  $requiredAddendaForms[] = [
                    'form' => $f,
                    'status' => $status
                  ];
                }
              }

              if (!$isSupportLetterCompleted) {
                $info = $stage->getSupportLetterInformation();
                if ($info['form'] && $info['count'] > 0) {
                  $name = !empty($info['name']) ? $info['name'] : t('Support Letters');
                  $count_req = !empty($info['count']) ? $info['count'] : 1;
                  $count = $this->getSupportLetterCompleteCount($application, $stage, $stage_forms, $subs);
                  if (isset($subs['application'][$info['form']->id()])) {
                    foreach ($subs['application'][$info['form']->id()] as $item) {
                      if (!$item->isDraft()) {
                        $count += 1;
                      }
                    }
                  }
                  $supportLetterInfo = [
                    'name' => $name,
                    'count' => $count,
                    'total' => $count_req
                  ];
                }
              }
            }

            return [
              '#theme' => 'niobi_application_admin_status_actions_application',
              '#application' => $application,
              '#workflow' => $workflow,
              '#completeStatus' => !empty($completeStatus) ? $completeStatus : FALSE,
              '#canAccess' => !empty($canAccess) ? $canAccess : FALSE,
              '#isApplicationCompleted' => !empty($isApplicationCompleted) ? $isApplicationCompleted : NULL,
              '#isRequiredAddendaCompleted' => !empty($isRequiredAddendaCompleted) ? $isRequiredAddendaCompleted : NULL,
              '#stageForms' => !empty($stage_forms) ? $stage_forms : NULL,
              '#requiredAddendaForms' => !empty($requiredAddendaForms) ? $requiredAddendaForms : NULL,
              '#isSupportLetterCompleted' => !empty($isSupportLetterCompleted) ? $isSupportLetterCompleted : NULL,
              '#supportLetterInfo' => !empty($supportLetterInfo) ? $supportLetterInfo : NULL
            ];
        }
      }
    }
    else {
      return [
        '#type' => 'markup',
        '#markup' => '<p><em>' . t('The application system is not fully set up.') . '</em></p>'
      ];
    }
  }

  /**
   * @param NiobiApplication $application
   * @param NiobiApplicationWorkflowStage $stage
   * @param $stage_forms
   * @param $subs
   * @return bool
   */
  public function checkApplicationCompleteStatus($application, $stage, $stage_forms, $subs) {
    if (!empty($stage_forms['application']) && empty($subs['application'])) {
      return FALSE;
    }
    // Next, check the application submission is not a draft.
    elseif(!empty($stage_forms['application']) && !empty($subs['application'])) {
      $app_form = array_shift($stage_forms['application']);
      foreach ($subs['application'] as $sub_form => $sub_list) {
        if($app_form->getWebform()->id() === $sub_form) {
          // There should only be one application submission.
          $sub = array_shift($sub_list);
          if ($sub->isDraft()) {
            return FALSE;
          }
        }
      }
    }
    // We are in fact complete. Return TRUE.
    return TRUE;
  }

  /**
   * @param NiobiApplication $application
   * @param NiobiApplicationWorkflowStage $stage
   * @param $stage_forms
   * @param $subs
   * @return bool
   */
  public function checkRequiredAddendaCompleteStatus($application, $stage, $stage_forms, $subs) {
    if(!empty($stage_forms['required_addenda']) && empty($subs['application'])) {
      return FALSE;
    }
    elseif(!empty($stage_forms['required_addenda']) && !empty($subs['application'])) {
      foreach($stage_forms['required_addenda'] as $addenda_form) {
        $addenda_form_id = $addenda_form->getWebform()->id();
        // A required form is missing a submission.
        if (!isset($subs['application'][$addenda_form_id])) {
          return FALSE;
        }
        else {
          $sub = array_shift($subs['application'][$addenda_form_id]);
          if ($sub->isDraft()) {
            return FALSE;
          }
        }
      }
    }
    // We are in fact complete. Return TRUE.
    return TRUE;
  }

  /**
   * @param NiobiApplication $application
   * @param NiobiApplicationWorkflowStage $stage
   * @param $stage_forms
   * @param $subs
   * @return int
   */
  public function getSupportLetterCompleteCount($application, $stage, $stage_forms, $subs) {
    if(!empty($stage_forms['support']) && !empty($subs['application'])) {
      foreach($stage_forms['support'] as $support_form) {
        if ($support_form) {
          $support_form_id = $support_form->getWebform()->id();
          // A required form is missing a submission.
          if (isset($subs['application'][$support_form_id])) {
            $count = 0;
            $info = $stage->getSupportLetterInformation();
            if ($info['form']) {
              $count = 0;
              if (isset($subs['application'][$support_form_id])) {
                foreach ($subs['application'][$support_form_id] as $item) {
                  if (!$item->isDraft()) {
                    $count += 1;
                  }
                }
              }
            }
            return $count;
          }
        }
      }
    }
    // We have a condition that prevents counting, so return zero.
    return 0;
  }

  /**
   * @param $application
   * @param $stage
   * @param $stage_forms
   * @param $subs
   * @return bool
   */
  public function checkSupportLetterCompleteStatus($application, $stage, $stage_forms, $subs) {
    if(!empty($stage_forms['support']) && empty($subs['application'])) {
      return FALSE;
    }
    elseif(!empty($stage_forms['support']) && !empty($subs['application'])) {
      foreach($stage_forms['support'] as $support_form) {
        if ($support_form) {
          $support_form_id = $support_form->getWebform()->id();
          // A required form is missing a submission.
          if (!isset($subs['application'][$support_form_id])) {
            return FALSE;
          }
          else {
            $count = 0;
            $info = $stage->getSupportLetterInformation();
            if ($info['form']) {
              $count_req = !empty($info['count']) ? $info['count'] : 1;
              $count = $this->getSupportLetterCompleteCount($application, $stage, $stage_forms, $subs);
            }
            if ($count < $count_req) {
              return FALSE;
            }
          }
        }
      }
    }
    // We are in fact complete. Return TRUE.
    return TRUE;
  }

  /**
   * @param NiobiApplication $application
   */
  public function applicationCompleteStatus($application) {
    $stage = $application->getCurrentApplicationStage();
    $stage_forms = $stage->getAllForms();
    $subs = $application->getAllSubmissionsbyWebform();

    // First, the application form must exist, and be completed.
    if (!$this->checkApplicationCompleteStatus($application, $stage, $stage_forms, $subs)) {
      return FALSE;
    }

    // Now we check required addenda
    if (!$this->checkRequiredAddendaCompleteStatus($application, $stage, $stage_forms, $subs)) {
      return FALSE;
    }

    // Finally, check the support letters.
    if (!$this->checkSupportLetterCompleteStatus($application, $stage, $stage_forms, $subs)) {
      return FALSE;
    }

    // We are in fact complete. Return TRUE.
    return TRUE;
  }
}
