<?php

/**
 * @file
 * Contains \Drupal\niobi_app\Plugin\views\field\View.
 */

namespace Drupal\niobi_app\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\niobi_app\Utilities\NiobiAppPromoteUtilities;
use Drupal\niobi_app\NiobiAppUtilities;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\webform\Entity\WebformSubmission;

/**
 * @ViewsField("niobi_app_status_actions")
 */
class NiobiApplicationStatusActions extends FieldPluginBase {

  /**
   * @param $route
   * @param array $params
   * @return mixed
   */
  public static function checkRouteAccess($route, array $params) {
    $accessManager = \Drupal::service('access_manager');
    return $accessManager->checkNamedRoute($route, $params, \Drupal::currentUser());
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['view'] = ['default' => ''];
    $options['display'] = ['default' => 'default'];
    $options['arguments'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['niobi_app'] = [
      '#type' => 'details',
      '#title' => $this->t("View settings"),
      '#open' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->addAdditionalFields();
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $output = NULL;
    $application = $values->_entity;
    $workflow = $application->getApplicationWorkflow();
    if ($workflow->isReadyToUse()) {
      $status = $application->get('field_application_status')->getValue();
      if (!empty($status)) {
        $status = $status[0]['value'];
        $output .= NiobiAppUtilities::generateStageList($status, $application);
        switch ($status) {
          case 'nomination':
            $output .= $this->addNominationLinks($application);
            break;
          case 'nomination_claim':
            $output .= $this->addNominationClaimLinks($application);
            break;
          case 'application':
            $output .= $this->addApplicationLinks($application);
            break;
          case 'review':
            $output .= $this->addReviewLinks($application);
            break;
          case 'decision':
            $output .= $this->addDecisionLinks($application);
            break;
          case 'complete':
            $output .= $this->addCompleteLinks($application);
            break;
        }
      }
    }
    else {
      \Drupal::messenger()->addError('The application has not been fully set up. Please contact the form administrator.');
      $output .= '<p><em>' . t('The application system is not fully set up.') . '</em></p>';
    }

    return ['#type' => 'markup', '#markup' => $output];
  }

  /**
   * @param $application
   * @return string
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function addNominationLinks($application) {
    $ret = '';

    // Get the submitted form information
    $nomination_sub_ids = $application->get('field_nomination_submissions')->getValue();
    $sub = FALSE;
    $complete_str = '';
    $complete_status = FALSE;
    if (!empty($nomination_sub_ids)) {
      $item = array_shift($nomination_sub_ids);
      $sub = WebformSubmission::load($item['target_id']);
      if ($sub) {
        $complete_status = $sub->isDraft() ? FALSE : TRUE;
        $complete_str = $complete_status ? t('Complete') : t('Draft');
        $complete_class = $complete_status ? 'btn btn-success': 'btn btn-warning';
        $complete_str = t('Nomination') . ' (' . $complete_str . ')';
      }
    }

    // Render Actions
    $ret .= '<p><strong>Actions</strong></p>';

    if ($complete_status) {
      $workflow = $application->getApplicationWorkflow();
      $params = [
        'niobi_application_workflow' => $workflow->id(),
        'niobi_application' => $application->id(),
      ];
      $options = [
        'attributes' => [
          'class' => 'btn btn-lg btn-success'
        ],
      ];
      $route = 'niobi_application.submit_nomination';
      $access = $this->checkRouteAccess($route, $params);
      if ($access) {
        $url = Url::fromRoute($route, $params, $options);
        $link = Link::fromTextAndUrl('Submit Nomination >', $url);
        $ret .= '<p>' . $link->toString() . '</p>';
      }
    }
    else {
      $ret .= '<p>Please submit a completed Nomination Form to continue with the nomination process.</p>';
    }


    // Render Submission
    $ret .= '<p><strong>Submitted Nominations</strong></p>';
    if ($sub) {
      $link_type = $complete_status ? 'canonical': 'edit-form';
      $ret .= $sub->toLink($complete_str , $link_type, ['attributes' => ['class' => $complete_class]])->toString();
    }
    else {
      $nomination_form = $application->getCurrentApplicationStage()->getNominationForm();
      if (!empty($nomination_form)) {
        $params = [
          'niobi_form' => $nomination_form->id(),
        ];
        $options = [
          'attributes' => [
            'class' => 'btn btn-danger'
          ],
          'query' => [
            'application_id' => $application->uuid->value,
          ],
        ];
        $route = 'entity.niobi_form.canonical';
        $access = $this->checkRouteAccess($route, $params);
        if ($access) {
          $url = Url::fromRoute($route, $params, $options);
          $link = Link::fromTextAndUrl('Nomination (Not Started)', $url);
          $ret .= '<p>' . $link->toString() . '</p>';
        }
      }
      else {
        \Drupal::messenger()->addError('An application has an incorrectly set status. Please contact the site administrator for help.');
      }
    }
    return $ret;
  }

  /**
   * @param $application
   * @return string
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function addNominationClaimLinks($application) {
    $ret = '<p><strong>' . t('Please share these links with your nominee so that they may begin the application process, or decline the nomination.') . '</strong></p>';
    $params = [
      'uuid' => $application->uuid->value,
    ];
    $url = Url::fromRoute('niobi_application.accept_nomination', $params);
    $url->setAbsolute(TRUE);
    $ret .= t('Accept') . ': <pre>' . $url->toString() . '</pre>';
    $url = Url::fromRoute('niobi_application.decline_nomination', $params);
    $url->setAbsolute(TRUE);
    $ret .= t('Decline') .  ': <pre>' . $url->toString() . '</pre>';
    // Render the links to existing nomination submissions.
    $nomination_sub_ids = $application->get('field_nomination_submissions')->getValue();
    $ret .= '<p><strong>Submitted Nominations</strong></p>';
    foreach ($nomination_sub_ids as $item) {
      $sub = WebformSubmission::load($item['target_id']);
      $ret .= $sub->toLink('Nomination', 'canonical',['attributes' => ['class' => 'btn btn-primary']])->toString();
    }
    return $ret;
  }

  /**
   * @param $application
   * @param $stage
   * @param $stage_forms
   * @param $subs
   * @return bool
   */
  public function checkApplicationCompleteStatus($application, $stage, $stage_forms, $subs) {
    return NiobiAppPromoteUtilities::applicationCompleteStatus ($application, $stage, $stage_forms, $subs);
//    if (!empty($stage_forms['application']) && empty($subs['application'])) {
//      return FALSE;
//    }
//    // Next, check the application submission is not a draft.
//    elseif(!empty($stage_forms['application']) && !empty($subs['application'])) {
//      $app_form = array_shift($stage_forms['application']);
//      foreach ($subs['application'] as $sub_form => $sub_list) {
//        if($app_form->getWebform()->id() === $sub_form) {
//          // There should only be one application submission.
//          $sub = array_shift($sub_list);
//          if ($sub->isDraft()) {
//            return FALSE;
//          }
//        }
//      }
//    }
//    // We are in fact complete. Return TRUE.
//    return TRUE;
  }

  /**
   * @param $application
   * @param $stage
   * @param $stage_forms
   * @param $subs
   * @return bool
   */
  public function checkRequiredAddendaCompleteStatus($application, $stage, $stage_forms, $subs) {
    return NiobiAppPromoteUtilities::applicationRequiredAddendaCompleteStatus ($application, $stage, $stage_forms, $subs);
//    if(!empty($stage_forms['required_addenda']) && empty($subs['application'])) {
//      return FALSE;
//    }
//    elseif(!empty($stage_forms['required_addenda']) && !empty($subs['application'])) {
//      foreach($stage_forms['required_addenda'] as $addenda_form) {
//        $addenda_form_id = $addenda_form->getWebform()->id();
//        // A required form is missing a submission.
//        if (!isset($subs['application'][$addenda_form_id])) {
//          return FALSE;
//        }
//        else {
//          $sub = array_shift($subs['application'][$addenda_form_id]);
//          if ($sub->isDraft()) {
//            return FALSE;
//          }
//        }
//      }
//    }
//    // We are in fact complete. Return TRUE.
//    return TRUE;
  }

  /**
   * @param $application
   * @param $stage
   * @param $stage_forms
   * @param $subs
   * @return int
   */
  public function getSupportLetterCompleteCount($application, $stage, $stage_forms, $subs) {
    return NiobiAppPromoteUtilities::applicationSupportLetterCompleteCount($application, $stage, $stage_forms, $subs);
//    if(!empty($stage_forms['support']) && !empty($subs['application'])) {
//      foreach($stage_forms['support'] as $support_form) {
//        if ($support_form) {
//          $support_form_id = $support_form->getWebform()->id();
//          // A required form is missing a submission.
//          if (isset($subs['application'][$support_form_id])) {
//            $count = 0;
//            $info = $stage->getSupportLetterInformation();
//            if ($info['form']) {
//              $count = 0;
//              if (isset($subs['application'][$support_form_id])) {
//                foreach ($subs['application'][$support_form_id] as $item) {
//                  if (!$item->isDraft()) {
//                    $count += 1;
//                  }
//                }
//              }
//            }
//            return $count;
//          }
//        }
//      }
//    }
//    // We have a condition that prevents counting, so return zero.
//    return 0;
  }

  /**
   * @param $application
   * @param $stage
   * @param $stage_forms
   * @param $subs
   * @return bool
   */
  public function checkSupportLetterCompleteStatus($application, $stage, $stage_forms, $subs) {
    return NiobiAppPromoteUtilities::applicationSupportLetterCompleteStatus($application, $stage, $stage_forms, $subs);
//    if(!empty($stage_forms['support']) && empty($subs['application'])) {
//      return FALSE;
//    }
//    elseif(!empty($stage_forms['support']) && !empty($subs['application'])) {
//      foreach($stage_forms['support'] as $support_form) {
//        if ($support_form) {
//          $support_form_id = $support_form->getWebform()->id();
//          // A required form is missing a submission.
//          if (!isset($subs['application'][$support_form_id])) {
//            return FALSE;
//          }
//          else {
//            $count = 0;
//            $info = $stage->getSupportLetterInformation();
//            if ($info['form']) {
//              $count_req = !empty($info['count']) ? $info['count'] : 1;
//              $count = $this->getSupportLetterCompleteCount($application, $stage, $stage_forms, $subs);
//            }
//            if ($count < $count_req) {
//              return FALSE;
//            }
//          }
//        }
//      }
//    }
//    // We are in fact complete. Return TRUE.
//    return TRUE;
  }

  /**
   * @param $application
   * @return bool
   */
  public function applicationCompleteStatus($application) {
    return NiobiAppPromoteUtilities::applicationReadyToSubmit($application);

//    $stage = $application->getCurrentApplicationStage();
//    $stage_forms = $stage->getAllForms();
//    $subs = $application->getAllSubmissionsbyWebform();
//
//    // First, the application form must exist, and be completed.
//    if (!$this->checkApplicationCompleteStatus($application, $stage, $stage_forms, $subs)) {
//      return FALSE;
//    }
//
//    // Now we check required addenda
//    if (!$this->checkRequiredAddendaCompleteStatus($application, $stage, $stage_forms, $subs)) {
//      return FALSE;
//    }
//
//    // Finally, check the support letters.
//    if (!$this->checkSupportLetterCompleteStatus($application, $stage, $stage_forms, $subs)) {
//      return FALSE;
//    }
//
//    // We are in fact complete. Return TRUE.
//    return TRUE;
  }

  /**
   * @param $application
   * @return string
   */
  public function addApplicationLinks($application) {
    $ret = '';

    $complete_status = NiobiAppPromoteUtilities::applicationReadyToSubmit($application);

    $workflow = $application->getApplicationWorkflow();
    $stage = $application->getCurrentApplicationStage();
    $stage_forms = $stage->getAllForms();
    $subs = $application->getAllSubmissionsbyWebform();

    // Render the submit action
    $ret .= '<p><strong>Actions</strong></p>';

    if ($complete_status) {
      $params = [
        'niobi_application_workflow' => $workflow->id(),
        'niobi_application' => $application->id(),
      ];
      $options = [
        'attributes' => [
          'class' => 'btn btn-lg btn-success'
        ],
      ];
      $route = 'niobi_application.submit_application';
      $access = $this->checkRouteAccess($route, $params);
      if ($access) {
        $url = Url::fromRoute($route, $params, $options);
        $link = Link::fromTextAndUrl('Submit Application >', $url);
        $ret .= '<p>' . $link->toString() . '</p>';
      }
    }
    else {
      $ret .= '<p>' . t('Please complete the following required items:') . '</p>';
      $ret .= '<ul>';
      if (!$this->checkApplicationCompleteStatus($application, $stage, $stage_forms, $subs)) {
        $ret .= '<li>' . t('Application') . '</li>';
      }
      if (!$this->checkRequiredAddendaCompleteStatus($application, $stage, $stage_forms, $subs)) {
        $ret .= '<li>' . t('Required Addenda') . '</li>';
      }
      if (!$this->checkSupportLetterCompleteStatus($application, $stage, $stage_forms, $subs)) {
        $info = $stage->getSupportLetterInformation();
        if ($info['form'] && $info['count'] > 0) {
          $name = !empty($info['name']) ? $info['name'] : t('Support Letters');
          $count_req = !empty($info['count']) ? $info['count'] : 1;
          $count = $this->getSupportLetterCompleteCount($application, $stage, $stage_forms, $subs);
          if (isset($subs['application'][$info['form']->id()])) {
            foreach($subs['application'][$info['form']->id()] as $item) {
              if (!$item->isDraft()) {
                $count += 1;
              }
            }
          }
          $ret .= '<li>' . t('%s: %count of %total required',
              [
                '%s' => $name,
                '%count' => $count,
                '%total' => $count_req,
              ]
            ) . '</li>';
        }
      }
      $ret .= '</ul>';
    }

    /*
     * Application Form
     */
    $ret .= '<p><strong>Application</strong></p>';
    // First, the application form must exist, and be completed.
    if (!empty($stage_forms['application'])) {
      $app_form = array_shift($stage_forms['application']);
      $app_webform = $app_form->getWebform();
      // There is no application form
      if (!isset($subs['application'][$app_webform->id()])) {
        $params = [
          'niobi_form' => $app_form->id(),
        ];
        $options = [
          'attributes' => [
            'class' => 'btn btn-danger'
          ],
          'query' => [
            'application_id' => $application->uuid->value,
          ],
        ];
        $route = 'entity.niobi_form.canonical';
        $access = $this->checkRouteAccess($route, $params);
        if ($access) {
          $url = Url::fromRoute($route, $params, $options);
          $link = Link::fromTextAndUrl('Application (Not Started)', $url);
          $ret .= '<p>' . $link->toString() . '</p>';
        }
      }
      else {
        $sub = array_shift($subs['application'][$app_webform->id()]);
        $draft_class = $sub->isDraft() ? 'btn btn-warning' : 'btn btn-success';
        $draft_message = $sub->isDraft() ? 'Application (Draft)' : 'Application (Complete)';
        $link_type = $sub->isDraft() ? 'edit-form' : 'canonical';
        $link_attrs = [
          'attributes' => [
            'class' => $draft_class
          ],
          'query' => [
            'application_id' => $application->uuid->value,
          ],
        ];
        $ret .= '<p>' . $sub->toLink($draft_message, $link_type, $link_attrs)->toString() . '</p>';
      }
    }

    /*
     * Addenda
     */
    $addenda_types = [
      'required_addenda' => t('Required Addenda'),
      'optional_addenda' => t('Optional Addenda'),
    ];
    foreach ($addenda_types as $add_key => $add_value) {
      // First, the application form must exist, and be completed.
      if (!empty($stage_forms[$add_key])) {
        $ret .= '<p><strong>' . $add_value . '</strong></p>';

        $add_forms = $stage_forms[$add_key];
        foreach ($add_forms as $add_form) {
          $add_webform = $add_form->getWebform();
          // There is no application form
          if (!isset($subs['application'][$add_webform->id()])) {
            $params = [
              'niobi_form' => $add_form->id(),
            ];
            $options = [
              'attributes' => [
                'class' => 'btn btn-danger'
              ],
              'query' => [
                'application_id' => $application->uuid->value,
              ],
            ];
            $route = 'entity.niobi_form.canonical';
            $access = $this->checkRouteAccess($route, $params);
            if ($access) {
              $url = Url::fromRoute($route, $params, $options);
              $link = Link::fromTextAndUrl($add_form->label() . ' (Not Started)', $url);
              $ret .= '<p>' . $link->toString() . '</p>';
            }
          }
          else {
            $sub = array_shift($subs['application'][$add_webform->id()]);
            if ($sub) {
              $draft_class = $sub->isDraft() ? 'btn btn-warning' : 'btn btn-success';
              $draft_message = $sub->isDraft() ? $add_form->label() . ' (Draft)' : $add_form->label() . ' (Complete)';
              $link_type = $sub->isDraft() ? 'edit-form' : 'canonical';
              $link_attrs = [
                'attributes' => [
                  'class' => $draft_class
                ],
                'query' => [
                  'application_id' => $application->uuid->value,
                ],
              ];
              $ret .= '<p>' . $sub->toLink($draft_message, $link_type, $link_attrs)->toString() . '</p>';
            }
          }
        }
      }
    }

    /*
     * Support Letters
     */
    $info = $stage->getSupportLetterInformation();
    if ($info['form']) {
      $name = empty($info['name']) ? t('Support Letters') : $info['name'];
      $ret .= '<p><strong>' . $name . '</strong></p>';
      // Render the request form if we have it. Otherwise, show a link to the form for support letters.
      if ($info['request_form']) {
        $attrs = [
          'attributes' => [
            'class' => 'btn btn-info',
          ],
            'query' => [
              'application_id' => $application->uuid->value,
            ],
        ];
        $link_text = t('Send Invite for %s', ['%s' => $name]);
        $ret .= '<p>' . $info['request_form']->toLink($link_text, 'canonical', $attrs)->toString() . '</p>';
      }
      else {
        $ret .= '<p><em>' . t('Please share this link with users who will enter the %s.',['%s' => $name]) . '</em></p>';
        $params = [
          'query' => [
            'application_id' => $application->uuid->value,
          ],
        ];
        $url = $info['form']->toUrl('canonical',$params)->setAbsolute(TRUE)->toString();
        $ret .= '<p><pre>' . $url . '</pre></p>';
      }
      // If users have filled the form, list submissions here. Do not link.
      $form_id = $info['form']->getWebform()->id();
      if (isset($subs['application'][$form_id])) {
        $ret .= '<p><em>' . t('%s submitted for this application', ['%s' => $name]) . '</em></p>';
        foreach ($subs['application'][$form_id] as $sub) {
          if ($sub->isDraft()) {
            $date = date('Y-m-d H:i', $sub->changed->value);
            $ret .= '<p><a class="btn btn-success btn-sm">' . t('%s submitted as draft on %d', ['%s' => $name, '%d' => $date]) . '</a></p>';
          }
          else {
            $date = date('Y-m-d H:i', $sub->changed->value);
            $ret .= '<p><a class="btn btn-success btn-sm">' . t('%s submitted on %d', ['%s' => $name, '%d' => $date]) . '</a></p>';
          }
        }
      }
    }

    return $ret;
  }

  /**
   * @param $application
   * @return string
   */
  public function addReviewLinks($application) {
    return '<p><strong>' . t('The application is now in review, and you will be informed when the review is complete.') . '</strong></p>';
  }

  /**
   * @param $application
   * @return string
   */
  public function addDecisionLinks($application) {
    return '<p><strong>' . t('The application review is complete for this stage, and is pending a decision.') . '</strong></p>';
  }

  /**
   * @param $application
   * @return string
   */
  public function addCompleteLinks($application) {
    $decision = $application->get('field_decision')->getValue();
    $decision = !empty($decision) ? $decision[0]['value'] : '';
    $decision_notes = $application->get('field_decision_notes')->getValue();
    $decision_notes = !empty($decision_notes) ? $decision_notes[0]['value'] : '';
    $ret = '<h3>' . $decision . '</h3>';
    $ret .= '<div>' . $decision_notes . '</div>';
    return $ret;
  }

}
