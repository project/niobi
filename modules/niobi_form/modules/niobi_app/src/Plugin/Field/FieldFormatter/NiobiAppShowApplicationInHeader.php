<?php

namespace Drupal\niobi_app\Plugin\Field\FieldFormatter;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\niobi_app\Form\NiobiApplicationSubmissions;
use Drupal\webform\Plugin\WebformSourceEntityManager;
use Drupal\webform\WebformMessageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Niobi-modified Plugin implementation of the 'Webform rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "niobi_app_show_application_in_header",
 *   label = @Translation("Niobi Application"),
 *   description = @Translation("Display a summery of the application."),
 *   field_types = {
 *     "boolean"
 *   },
 *   weight = 100
 * )
 */
class NiobiAppShowApplicationInHeader extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    return $summary;
  }

  /**
   * Gets the available format options.
   *
   * @return array|string
   *   A list of output formats. Each entry is keyed by the machine name of the
   *   format. The value is an array, of which the first item is the result for
   *   boolean TRUE, the second is for boolean FALSE. The value can be also an
   *   array, but this is just the case for the custom format.
   */
  protected function getOutputFormats() {
    $formats = [
      'default' => [$this->getFieldSetting('on_label'), $this->getFieldSetting('off_label')],
    ];
    return $formats;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {

      if ($item->value) {
        $app_id = 0;
        $uuid = Request::createFromGlobals()->get('application_id');
        if (is_string($uuid) && (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $uuid) === 1)) {
          $s = \Drupal::entityTypeManager()->getStorage('niobi_application')->loadByProperties(['uuid' => $uuid]);
          if (!empty($s)) {
            $app_id = array_keys($s)[0];
          }
        }
        if ($app_id) {
          $form = \Drupal::formBuilder()->getForm(NiobiApplicationSubmissions::class, $app_id);
          $renderer = \Drupal::service('renderer');

          // Get the tabs string and set as the element markup.
          $elements[$delta]['#markup'] = $renderer->render($form)->__toString();

        }
        else {
          $elements[$delta]['#type'] = 'markup';
          $elements[$delta]['#markup'] = '';
        }
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity) {
    return AccessResult::allowed();
  }

}
