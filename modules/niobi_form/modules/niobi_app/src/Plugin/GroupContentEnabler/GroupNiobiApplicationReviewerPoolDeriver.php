<?php

namespace Drupal\niobi_app\Plugin\GroupContentEnabler;

use Drupal\Component\Plugin\Derivative\DeriverBase;

class GroupNiobiApplicationReviewerPoolDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives['niobi_application_reviewer_pool'] = [
        'entity_bundle' => 'niobi_application_reviewer_pool',
        'label' => t('Niobi Application Reviewer Pool'),
        'description' => t('Adds niobi_application_reviewer_pool entities to groups.'),
      ] + $base_plugin_definition;
    return $this->derivatives;
  }

}
