<?php

namespace Drupal\niobi_app\Plugin\GroupContentEnabler;

use Drupal\Component\Plugin\Derivative\DeriverBase;

class GroupNiobiApplicationDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives['niobi_application'] = [
        'entity_bundle' => 'niobi_application',
        'label' => t('Niobi Application'),
        'description' => t('Adds niobi_application entities to groups.'),
      ] + $base_plugin_definition;
    return $this->derivatives;
  }

}
