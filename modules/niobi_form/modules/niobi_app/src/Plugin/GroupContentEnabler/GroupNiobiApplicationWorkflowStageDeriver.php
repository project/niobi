<?php

namespace Drupal\niobi_app\Plugin\GroupContentEnabler;

use Drupal\Component\Plugin\Derivative\DeriverBase;

class GroupNiobiApplicationWorkflowStageDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives['niobi_application_workflow_stage'] = [
        'entity_bundle' => 'niobi_application_workflow_stage',
        'label' => t('Niobi Application Workflow Stage'),
        'description' => t('Adds niobi_application_workflow_stage entities to groups.'),
      ] + $base_plugin_definition;
    return $this->derivatives;
  }

}
