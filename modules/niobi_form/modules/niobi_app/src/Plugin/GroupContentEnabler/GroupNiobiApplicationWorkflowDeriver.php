<?php

namespace Drupal\niobi_app\Plugin\GroupContentEnabler;

use Drupal\Component\Plugin\Derivative\DeriverBase;

class GroupNiobiApplicationWorkflowDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives['niobi_application_workflow'] = [
        'entity_bundle' => 'niobi_application_workflow',
        'label' => t('Niobi Application Workflow'),
        'description' => t('Adds niobi_application_workflow entities to groups.'),
      ] + $base_plugin_definition;
    return $this->derivatives;
  }

}
