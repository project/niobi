<?php

namespace Drupal\niobi_app\Plugin\views_add_button;

use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;
use Drupal\views_add_button\ViewsAddButtonInterface;

/**
 * Integrates Views Add Button with group_content entities.
 *
 * @ViewsAddButton(
 *   id = "application_add_button",
 *   label = @Translation("ApplicationAddButton"),
 *   target_entity = "niobi_application"
 * )
 */
class NiobiApplicationAddButton extends PluginBase implements ViewsAddButtonInterface {

  /**
   * Plugin Description.
   *
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('Views Add Button URL Generator for Niobi Application entities');
  }

  /**
   * Get the machine name of the entity bundle from a hashed string.
   *
   * @param string $bundle_string
   *   The hashed group_content bundle string to parse.
   *
   * @return string
   *   The bundle machine name.
   */
  public static function getBundle($bundle_string) {
    $storage_config = \Drupal::configFactory()->getEditable('group.content_type.' . $bundle_string);
    $plugin_id = $storage_config->getOriginal('content_plugin');
    return $plugin_id;
  }

  /**
   * Check for access to the appropriate "add" route.
   *
   * @param string $entity_type
   *   Entity id as a machine name.
   * @param string $bundle
   *   The bundle string.
   * @param string $context
   *   Entity context string, a comma-separated list of values.
   *
   * @return bool
   *   Whether we have access.
   */
  public static function checkAccess($entity_type, $bundle, $context) {
    $c = explode(',', $context);
    if (!isset($c[0]) || !is_numeric($c[0])) {
      return FALSE;
    }

    $workflow_id = $c[0];
    $workflow = NiobiApplicationWorkflow::load($workflow_id);

    return $workflow->mayCreateApplications();
  }

  /**
   * Generate the Add Button Url.
   *
   * @param string $entity_type
   *   Entity id as a machine name.
   * @param string $bundle
   *   The bundle string.
   * @param array $options
   *   Array of options to be used when building the Url, and Link.
   * @param string $context
   *   Entity context string, a comma-separated list of values.
   *
   * @return \Drupal\Core\Url
   *   The Url to use in the Add Button link.
   */
  public static function generateUrl($entity_type, $bundle, array $options, $context = '') {
    $c = explode(',', $context);
    if (!isset($c[0]) || !is_numeric($c[0])) {
      return FALSE;
    }

    $route = 'niobi_application.create_application';
    $workflow_id = $c[0];
    $workflow = NiobiApplicationWorkflow::load($workflow_id);
    $stage = $workflow->getFirstStage();
    if ($stage) {
      // Check if we have a nomination or application form.
      $nomination = $stage->getNominationForm();
      if ($nomination) {
        $route = 'niobi_application.create_nomination';
      }
    }

    $url = Url::fromRoute($route, ['niobi_application_workflow' => $workflow_id], $options);

    return $url;
  }

  /**
   * Generate the add button link.
   * Overrides the entered text if the workflow has a text specified.
   *
   * @param $text
   *   The link text.
   * @param Url $url
   *   The Url for constructing the link.
   * @param array $options
   *   Array of options from the VAB settings.
   *
   * @return \Drupal\Core\Link
   *   Link object which is used to construct the add button.
   */
  public static function generateLink($text, Url $url, array $options = []) {
    $params = $url->getRouteParameters();
    if (isset($params['niobi_application_workflow']) && !empty($params['niobi_application_workflow'])){
      $workflow = NiobiApplicationWorkflow::load($params['niobi_application_workflow']);
      $workflow_arr = $workflow->toArray();
      if (isset($workflow_arr['field_new_item_button_text'][0]['value']) && !empty($workflow_arr['field_new_item_button_text'][0]['value'])) {
        $text = $workflow_arr['field_new_item_button_text'][0]['value'];
      }
    }
    return Link::fromTextAndUrl($text, $url);
  }

}
