<?php

namespace Drupal\niobi_app\Plugin\views_add_button;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupContent;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;
use Drupal\views_add_button\ViewsAddButtonInterface;

/**
 * Integrates Views Add Button with group_content entities.
 *
 * @ViewsAddButton(
 *   id = "niobi_workflow_stage_manager_add_button",
 *   label = @Translation("NiobiWorkflowStageManagerAddButton"),
 *   target_entity = "niobi_application_workflow_stage",
 *   manual_select = true
 * )
 */
class NiobiWorkflowStageManagerAddButton extends PluginBase implements ViewsAddButtonInterface {

  /**
   * Plugin Description.
   *
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('Views Add Button URL Generator for Group Content entities');
  }

  /**
   * Get the machine name of the entity bundle from a hashed string.
   *
   * @param string $bundle_string
   *   The hashed group_content bundle string to parse.
   *
   * @return string
   *   The bundle machine name.
   */
  public static function getBundle($bundle_string) {
    $storage_config = \Drupal::configFactory()->getEditable('group.content_type.' . $bundle_string);
    $plugin_id = $storage_config->getOriginal('content_plugin');
    return $plugin_id;
  }

  /**
   * Check for access to the appropriate "add" route.
   *
   * @param string $entity_type
   *   Entity id as a machine name.
   * @param string $bundle
   *   The bundle string.
   * @param string $context
   *   Entity context string, a comma-separated list of values.
   *
   * @return bool
   *   Whether we have access.
   */
  public static function checkAccess($entity_type, $bundle, $context) {
    $c = explode(',', $context);
    if (!isset($c[0]) || !is_numeric($c[0])) {
      return FALSE;
    }

    $workflow = NiobiApplicationWorkflow::load($c[0]);
    $gc = GroupContent::loadByEntity($workflow);
    $gc = array_shift($gc);
    $group_id = $gc->getGroup()->id();

    $route = 'entity.group_content.create_form';
    $plugin_id = 'group_niobi_application_workflow_stage:niobi_application_workflow_stage';
    // At this point we should have a route. If not, deny access.
    $accessManager = \Drupal::service('access_manager');
    return $accessManager->checkNamedRoute($route, ['group' => $group_id, 'plugin_id' => $plugin_id], \Drupal::currentUser());
  }

  /**
   * Generate the Add Button Url.
   *
   * @param string $entity_type
   *   Entity id as a machine name.
   * @param string $bundle
   *   The bundle string.
   * @param array $options
   *   Array of options to be used when building the Url, and Link.
   * @param string $context
   *   Entity context string, a comma-separated list of values.
   *
   * @return \Drupal\Core\Url|bool
   *   The Url to use in the Add Button link.
   */
  public static function generateUrl($entity_type, $bundle, array $options, $context = '') {
    $c = explode(',', $context);
    if (!isset($c[0]) || !is_numeric($c[0])) {
      return FALSE;
    }

    $workflow = NiobiApplicationWorkflow::load($c[0]);
    $gc = GroupContent::loadByEntity($workflow);
    $gc = array_shift($gc);
    $group_id = $gc->getGroup()->id();

    $route = 'entity.group_content.create_form';
    $plugin_id = 'group_niobi_application_workflow_stage:niobi_application_workflow_stage';
    // Create URL from the data above.
    $url = Url::fromRoute($route, ['group' => $group_id, 'plugin_id' => $plugin_id], $options);
    return $url;
  }

}
