<?php

namespace Drupal\niobi_app\Plugin\task\Action;

use Drupal\message\Entity\Message;
use Drupal\Core\Plugin\PluginBase;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\niobi_app\Entity\NiobiConflictOfInterest;
use Drupal\task\TaskActionInterface;
use Drupal\task\Entity\TaskInterface;
use Drupal\user\Entity\User;

/**
 * @TaskAction(
 *   id = "niobi_app_dismiss_as_conflict",
 *   label = @Translation("DismissAsConflict"),
 *   system_task = FALSE,
 * )
 */
class DismissAsConflict extends PluginBase implements TaskActionInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description()
  {
    return $this->t('This is a description of the default plugin.');
  }

  /**
   * Since this is a default, just return what we have.
   */
  public static function doAction(TaskInterface $task, $data = []) {
    $assignee = $task->get('assigned_to')->getValue();
    $application = $task->get('field_application')->getValue();
    if ($assignee[0]['target_id'] && $application[0]['target_id']) {
      $assignee = User::load($assignee[0]['target_id']);
      $application = NiobiApplication::load($application[0]['target_id']);
      /* @var $workflow \Drupal\niobi_app\Entity\NiobiApplicationWorkflow */
      /* @var $workflow_admin \Drupal\user\Entity\User */
      $workflow = $application->getApplicationWorkflow();
      $workflow_admin = $workflow->getOwner();
      $workflow_admin_team = $workflow->getAdminTeam(TRUE);
      $module = isset($data['module']) ? $data['module'] : 'task';
      $key = isset($data['key']) ? $data['key'] : 'task_mail';
      $to = $workflow_admin->getEmail();
      if ($workflow_admin_team) {
        foreach ($workflow_admin_team as $person) {
          $to = implode(',', [$to, $person->getEmail()]);
        }
      }

      $langcode = \Drupal::currentUser()->getPreferredLangcode();
      $params = isset($data['params']) ? $data['params'] : [
        'from' => \Drupal::config('system.site')->get('mail'),
        'subject' => t('Review assignment was declined by %reviewer', ['%reviewer' => $assignee->getAccountName()]),
        'message' => ['<p>' . t('The reviewer %reviewer has declined an assignment to review %assignment . They have also marked themselves as having a conflict of interest with the applicant. You may want to create a new assignment.',
            [
              '%reviewer' => $assignee->getAccountName(),
              '%assignment' => $application->getName(),
            ])
          . '</p>']
      ];
      $reply = isset($data['reply']) ? $data['reply'] : NULL;
      $send = TRUE;
      $mailManager = \Drupal::service('plugin.manager.mail');
      $result = $mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);

      \Drupal::messenger()->addStatus('You have declined this assignment. The workflow administrator has been informed via e-mail.');

      // Create the COI
      $coi_params = [
        'name' => t('COI: %reviewer - %applicant', ['%applicant' => $application->getOwner()->getAccountName(), '%reviewer' => $assignee->getAccountName()]),
        'type' => 'conflict_with_another_user',
        'field_applicant' => $application->getOwner()->id(),
        'field_reviewer' => $assignee->id(),
      ];
      $coi = NiobiConflictOfInterest::create($coi_params);
      $coi->save();

      // Mark the task closed
      $task->set('status', 'closed');
      $task->set('close_date', time());
      $task->set('close_type', 'assignment declined');
      $task->save();

      // Delete the assignment message
      $query = \Drupal::entityQuery('message');
      $query->condition('template', 'niobi_app_assigned_task');
      $query->condition('field_assignment_task', $task->id());
      $messages = $query->execute();
      if (!empty($messages)) {
        $message = Message::load(array_shift($messages));
        $message->delete();
      }
    }
  }
}