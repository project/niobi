<?php

namespace Drupal\niobi_app\Plugin\task\Bundle;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\message\Entity\Message;
use Drupal\niobi_app\NiobiAppUtilities;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\niobi_form\Entity\NiobiForm;
use Drupal\task\Entity\Task;
use Drupal\task\Entity\TaskInterface;
use Drupal\task\TaskBundleInterface;
use Drupal\user\Entity\User;
use Drupal\Component\Render\FormattableMarkup;

/**
 * @TaskBundle(
 *   id = "application_review_task",
 *   label = @Translation("Application Review Task"),
 *   bundle = "application_review_task",
 *   system_task = TRUE
 * )
 */
class ApplicationReviewTask extends PluginBase implements TaskBundleInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description()
  {
    return $this->t('Actions for Application Review Task');
  }

  /**
   * @param $task_data
   * @return \Drupal\Core\Entity\EntityInterface|Task
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function createTask($task_data) {
    $reviewer = User::load($task_data['assigned_to']);
    $application = NiobiApplication::load($task_data['field_application']);
    $review_form = NiobiForm::load($task_data['field_review_form']);
    if (!NiobiAppUtilities::excludedAsReviewer($reviewer, $application, $review_form)) {
      $task = Task::create($task_data);
      $data = $task->get('task_data')->getValue();
      $task->save();

      // With the task created, let's add an assignment message.
      $message_data = [
        'template' => 'niobi_app_assigned_task',
        'field_assignment_verb' => 'review',
        'field_assignment_item' => $application->label(),
        'field_assignment_task' => $task,
        'field_assignment_user' => $reviewer,
      ];
      $message = Message::create($message_data);
      $message->save();

      // Now run the usual task create actions.
      if (isset($data[0]['actions']['create'])) {
        $action_data = $data[0]['actions']['create'];
        foreach ($action_data as $id => $data) {
          $plugin_manager = \Drupal::service('plugin.manager.task_action');
          $plugin_definitions = $plugin_manager->getDefinitions();
          if(isset($plugin_definitions[$id])) {
            $plugin_definitions[$id]['class']::doAction($task, $data);
          }
        }
      }
      return $task;
    }
    else {
      if (\Drupal::currentUser()->id() !== $application->getOwnerId()) {
        \Drupal::messenger()->addMessage(t('Application review task was not created, as the assignment triggered an exclusion rule.'), 'error');
      }
    }
  }

  /**
   * @param $task
   * @return mixed
   */
  public static function expireTask($task) {
    $data = $task->get('task_data')->getValue();
    if (isset($data[0]['actions']['expire'])) {
      $action_data = $data[0]['actions']['expire'];
      foreach ($action_data as $id => $data) {
        $plugin_manager = \Drupal::service('plugin.manager.task_action');
        $plugin_definitions = $plugin_manager->getDefinitions();
        if(isset($plugin_definitions[$id])) {
          $plugin_definitions[$id]['class']::doAction($task, $data);
        }
      }
    }
    $task->set('status', 'closed');
    $task->set('close_date', time());
    $task->set('close_type', 'completed');
    $task->save();
    return $task;
  }

  /**
   * @param TaskInterface $task
   * @return array
   */
  public static function getTaskOptions(TaskInterface $task) {
    $application = ApplicationReviewTask::getReviewTaskApplication($task);
    if ($application) {
      $application_uuid = $application->uuid();

      $status = ApplicationReviewTask::getReviewTaskStatus($task, $application);
      $review_sub = FALSE;
      if ($status !== 'not started') {
        $review_sub =  ApplicationReviewTask::getReviewTaskSubmission($task, $application);
      }
      switch ($status) {
        case 'not under review':
          $message = '<p>' . t('This application is not under review at this time, and reviews cannot be added.') . '</p>';
          $message .= '<p>' . t('Application Status: %status', ['%status' => $application->getCurrentApplicationStatus()]) . '</p>';
          $url_dismiss = Url::fromRoute('task.dismiss', ['task' => $task->id()], ['attributes' => ['class' => 'btn btn-danger button']]);
          $link_dismiss = Link::fromTextAndUrl('Dismiss', $url_dismiss);
          return ['#type' => 'markup', '#markup' => new FormattableMarkup(implode(' ', [$message, $link_dismiss->toString()]), [])];
          break;
        case 'not started':
          $review_id = $task->get('field_review_form')->getvalue()[0]['target_id'];
          $review_form = NiobiForm::load($review_id);

          $task_uuid = $task->uuid();
          $query_params = ['application_id' => $application_uuid, 'review_assignment_id' => $task_uuid];

          $url_review = $review_form->toUrl('canonical', ['attributes' => ['class' => 'btn btn-success button'], 'query' => $query_params]);
          $link_review = Link::fromTextAndUrl('Review This Application', $url_review);
          $url_decline = Url::fromRoute('niobi_app.review_decline', ['task' => $task->id()], ['attributes' => ['class' => 'btn btn-warning button']]);
          $link_decline = Link::fromTextAndUrl('Decline', $url_decline);
          $url_coi = Url::fromRoute('niobi_app.review_mark_coi', ['task' => $task->id()], ['attributes' => ['class' => 'btn btn-danger button']]);
          $link_coi = Link::fromTextAndUrl('I have a conflict of interest', $url_coi);

          return ['#type' => 'markup', '#markup' => new FormattableMarkup(implode(' ', [$link_review->toString(), $link_decline->toString(), $link_coi->toString()]), [])];
          break;
        case 'draft':
          $url = $review_sub->toUrl('edit-form', ['attributes' => ['class' => 'btn btn-warning button']]);
          $link = Link::fromTextAndUrl('Finish Review', $url);
          return ['#type' => 'markup', '#markup' => new FormattableMarkup($link->toString(), [])];
          break;
        case 'complete':
          $url = $review_sub->toUrl('canonical', ['attributes' => ['class' => 'btn btn-success button']]);
          $link = Link::fromTextAndUrl('Review Complete (Click to View/Edit)', $url);
          $url_dismiss = Url::fromRoute('task.dismiss', ['task' => $task->id()], ['attributes' => ['class' => 'btn btn-danger button']]);
          $link_dismiss = Link::fromTextAndUrl('Dismiss', $url_dismiss);
          return ['#type' => 'markup', '#markup' => new FormattableMarkup(implode(' ', [$link->toString(), $link_dismiss->toString()]), [])];
          break;
      }
    }
  }

  /**
   * @param TaskInterface $task
   * @return NiobiApplication
   */
  public static function getReviewTaskApplication(TaskInterface $task) {
    $app_id = $task->get('field_application')->getvalue()[0]['target_id'];
    $application = NiobiApplication::load($app_id);
    return $application;
  }

  /**
   * @param TaskInterface $task
   * @param NiobiApplication $application
   * @return mixed
   */
  public static function getReviewTaskSubmission(TaskInterface $task, NiobiApplication $application) {
    $task_uuid = $task->uuid();
    $review_subs = $application->getReviewSubmissions();
    foreach ($review_subs as $sub) {
      /* @var $sub \Drupal\webform\Entity\WebformSubmission */
      if ($sub) {
        $data = $sub->getElementData('review_assignment_id');
        if ($task_uuid === $data) {
          return $sub;
        }
      }
    }

    // We didn't find a matching item.
    return FALSE;
  }

  /**
   * @param TaskInterface $task
   * @return bool
   */
  public static function getReviewTaskStatus(TaskInterface $task, NiobiApplication $application) {
    $task_submission = ApplicationReviewTask::getReviewTaskSubmission($task, $application);
    // First check the application is under a review status
    $status = $application->getCurrentApplicationStatus();
    if ($status === 'review') {
      /* @var $task_submission \Drupal\webform\Entity\WebformSubmission */
      if ($task_submission && $task_submission->isCompleted()) {
        return 'complete';
      }
      elseif ($task_submission) {
        return 'draft';
      }
      else {
        return 'not started';
      }
    }
    else {
      return 'not under review';
    }

  }

}
