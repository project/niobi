<?php

namespace Drupal\niobi_app\Plugin\niobi_app\ApplicationDecision;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\Entity\NiobiApplication;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @NiobiAppDecision (
 *   id = "return_to_review",
 *   label = @Translation("Move back to review status"),
 * )
 */
class ReturnToReview extends DecisionBase {

  /**
   * @param FormStateInterface $form_state
   * @param NiobiApplication $application
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function processDecision(FormStateInterface $form_state, NiobiApplication $application) {
    $application->set('field_application_status', 'review');
    $application->save();

    $url = $application->toUrl('canonical');
    $form_state->setRedirectUrl($url);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @param NiobiApplication $application
   * @return array
   */
  public static function alterDecisionForm(array $form, FormStateInterface $form_state, NiobiApplication $application) {
    return $form;
  }

}