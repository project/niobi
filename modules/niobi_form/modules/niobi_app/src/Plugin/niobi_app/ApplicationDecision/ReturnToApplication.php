<?php

namespace Drupal\niobi_app\Plugin\niobi_app\ApplicationDecision;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\Entity\NiobiApplication;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @NiobiAppDecision (
 *   id = "return_to_application",
 *   label = @Translation("Move back to application status, and possibly send the applicant a message"),
 * )
 */
class ReturnToApplication extends DecisionBase {

  public static function processDecision(FormStateInterface $form_state, NiobiApplication $application) {
    $application->set('field_decision', $form_state->getValue('decision'));
    $application->set('field_decision_notes', $form_state->getValue('decision_notes'));
    $application->set('field_application_status', 'application');
    $application->save();

    // Send mail.
    parent::processDecision($form_state, $application);
  }

  public static function alterDecisionForm(array $form, FormStateInterface $form_state, NiobiApplication $application) {
    // Email logic from DecisionBase.
    $form = parent::alterDecisionForm($form, $form_state, $application);
    return $form;
  }

}
