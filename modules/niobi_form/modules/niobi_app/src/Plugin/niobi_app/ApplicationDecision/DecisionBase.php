<?php

namespace Drupal\niobi_app\Plugin\niobi_app\ApplicationDecision;

use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\Entity\NiobiApplication;

/**
 * Class DecisionBase
 * @package Drupal\niobi_app\Plugin\niobi_app\ApplicationDecision
 */
class DecisionBase {

  /**
   * Sends an email if the info is set.
   * @param FormStateInterface $form_state
   * @param NiobiApplication $application
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public static function processDecision(FormStateInterface $form_state, NiobiApplication $application) {
    // Send email if info is set.
    $send = $form_state->getValue('send_email');
    if (isset($send['send']) && $send['send'] === 'send') {
      $to = $application->getOwner()->getEmail();
      if ($to) {
        $bcc = FALSE;
        $params = [];
        if (isset($send['bcc_owner']) && $send['bcc_owner'] === 'bcc_owner') {
          $bcc = $application->getApplicationWorkflow()->getOwner()->getEmail();
          $params['headers'] = ['Bcc' => $bcc];
        }

        $module = 'niobi_app';
        $key = 'niobi_app_mail';
        $langcode = \Drupal::currentUser()->getPreferredLangcode();

        $message = $form_state->getValue('decision_notes');
        $message = isset($message['value']) ? $message['value'] : $message;

        $params['from'] = \Drupal::config('system.site')->get('mail');
        $params['subject'] = $form_state->getValue('decision');
        $params['message'] = $message;

        $reply = NULL;
        $send = TRUE;
        $mailManager = \Drupal::service('plugin.manager.mail');
        
        $result = $mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);
      }
    }

    $url = $application->toUrl('canonical');
    $form_state->setRedirectUrl($url);
  }

  /**
   * Email send logic
   * @param array $form
   * @param FormStateInterface $form_state
   * @param NiobiApplication $application
   * @return array
   */
  public static function alterDecisionForm(array $form, FormStateInterface $form_state, NiobiApplication $application) {
    $owner = $application->getApplicationWorkflow()->getOwner()->label();
    $form['send_email'] = [
      '#type' => 'checkboxes',
      '#title' => t('Send decision to applicant by email'),
      '#description' => t('An email will be sent to the applicant with the decision as subject and decision notes as the body.'),
      '#options' => [
        'send' => t('Send'),
        'bcc_owner' => t('BCC Application System Owner: %name', ['%name' => $owner]),
      ],
      '#weight' => 70
    ];
    $form['decision'] = [
      '#type' => 'textfield',
      '#title' => t('Decision'),
      '#placeholder' => t('A brief summary statement of the decision, i.e. "accept without reservation."'),
      '#weight' => 80
    ];
    $form['decision_notes'] = [
      '#type' => 'text_format',
      '#title' => t('Decision Notes'),
      '#weight' => 90
    ];

    return $form;
  }

}