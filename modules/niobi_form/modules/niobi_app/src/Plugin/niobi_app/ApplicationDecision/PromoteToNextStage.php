<?php

namespace Drupal\niobi_app\Plugin\niobi_app\ApplicationDecision;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\Controller\NiobiAppController;
use Drupal\niobi_app\Entity\NiobiApplication;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @NiobiAppDecision (
 *   id = "promote_to_next_stage",
 *   label = @Translation("Promote to next stage"),
 *   auto_decision = TRUE
 * )
 */
class PromoteToNextStage extends DecisionBase {

  /**
   * Advance the application.
   * @param NiobiApplication $application
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function advanceApplication(NiobiApplication $application) {
    // Update application status
    $current_stage = $application->getCurrentApplicationStage();
    if ($current_stage) {
      /* @var $workflow \Drupal\niobi_app\Entity\NiobiApplicationWorkflow */
      $workflow = $application->getApplicationWorkflow();
      $stages = $workflow->getStages();
      // Find the next stage.
      while (!empty($stages)) {
        $stage = array_shift($stages);
        if ($stage->id() === $current_stage->id()) {
          if (count($stages) > 0) {
            /* @var $next_stage \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage */
            $next_stage = array_shift($stages);

            // We have found the stage, now determine the status.
            $forms = $next_stage->getAllForms();
            // Assume that if the stage is empty, we are going direct to the next decision.
            $status = 'decision';
            foreach (['review', 'application', 'nomination'] as $stat) {
              if (!empty($forms[$stat]) && !empty(array_shift($forms[$stat]))) {
                $status = $stat;
              }
            }

            $application->set('field_current_stage', $next_stage->id());
            switch($status) {
              case 'nomination':
              case 'application':
              case 'decision':
                $application->set('field_application_status', $status);
                $application->save();
                break;
              case 'review':
                $application->set('field_application_status', $status);
                $application->save();
                NiobiAppController::runAutoAssignment($application);
                break;
            }
          }
          else {
            \Drupal::messenger()->addError('There are no additional stages to which we can promote this application.');
          }
        }
      }
    }
  }

  /**
   * Advance the application and send mail if specified email.
   * @param FormStateInterface $form_state
   * @param NiobiApplication $application
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function processDecision(FormStateInterface $form_state, NiobiApplication $application) {
    PromoteToNextStage::advanceApplication($application);
    // Send mail.
    parent::processDecision($form_state, $application);
  }

  /**
   * Advance the application without sending email.
   * @param NiobiApplication $application
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function autoDecision(NiobiApplication $application) {
    PromoteToNextStage::advanceApplication($application);
    \Drupal::messenger()->addMessage(t('Application %label has been promoted to the next stage.', ['%label' => $application->label()]));
  }

  /**
   * We are just doing the base extension.
   * @param array $form
   * @param FormStateInterface $form_state
   * @param NiobiApplication $application
   * @return array
   */
  public static function alterDecisionForm(array $form, FormStateInterface $form_state, NiobiApplication $application) {
    // Email logic from DecisionBase.
    $form = parent::alterDecisionForm($form, $form_state, $application);
    return $form;
  }

}