<?php

namespace Drupal\niobi_app\Plugin\niobi_app\ReviewerExclusionRule;

use Drupal\Core\Plugin\PluginBase;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage;
use Drupal\niobi_app\NiobiAppReviewerExclusionRuleInterface;
use Drupal\niobi_form\Entity\NiobiForm;
use Drupal\user\Entity\User;

/**
 * @NiobiAppReviewerExclusionRule(
 *   id = "duplicate_assignment",
 *   label = @Translation("Duplicate: Reviewer is already assigned"),
 * )
 */
class DuplicateAssignment extends PluginBase implements NiobiAppReviewerExclusionRuleInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('A reviewer may be assigned to an application for a particular review form just one time..');
  }

  /**
   * @param User $reviewer
   * @param NiobiApplication $application
   * @param NiobiForm|NULL $review_form
   * @return bool
   */
  public static function isExcluded(User $reviewer, NiobiApplication $application, NiobiForm $review_form = NULL) {
    if (!empty($review_form)) {
      $query = \Drupal::entityQuery('task');
      $query->condition('type', 'application_review_task');
      $query->condition('assigned_to', $reviewer->id());
      $query->condition('field_application', $application->id());
      $query->condition('field_review_form', $review_form->id());
      $result = $query->execute();
      if (!empty($result) && \Drupal::currentUser()->id() !== $application->getOwnerId()) {
        \Drupal::messenger()->addMessage(t('Duplicate assignment detected.'), 'error');
      }
      return !empty($result);
    }
    // We don't have a form to complete the duplicate check.
    else {
      return FALSE;
    }
  }
}