<?php

namespace Drupal\niobi_app\Plugin\niobi_app\ReviewerExclusionRule;

use Drupal\Core\Plugin\PluginBase;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage;
use Drupal\niobi_app\NiobiAppReviewerExclusionRuleInterface;
use Drupal\niobi_form\Entity\NiobiForm;
use Drupal\user\Entity\User;

/**
 * @NiobiAppReviewerExclusionRule(
 *   id = "is_applicant",
 *   label = @Translation("Reviewer is the Applicant"),
 * )
 */
class IsApplicant extends PluginBase implements NiobiAppReviewerExclusionRuleInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('The applicant may not review their own form.');
  }

  /**
   * @param User $reviewer
   * @param NiobiApplication $application
   * @param NiobiForm|NULL $review_form
   * @return bool
   */
  public static function isExcluded(User $reviewer, NiobiApplication $application, NiobiForm $review_form = NULL) {
    return $application->isApplicant($reviewer->id());
  }
}