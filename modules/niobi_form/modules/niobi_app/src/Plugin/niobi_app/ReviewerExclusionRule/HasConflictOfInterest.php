<?php

namespace Drupal\niobi_app\Plugin\niobi_app\ReviewerExclusionRule;

use Drupal\Core\Plugin\PluginBase;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage;
use Drupal\niobi_app\NiobiAppReviewerExclusionRuleInterface;
use Drupal\niobi_form\Entity\NiobiForm;
use Drupal\user\Entity\User;

/**
 * @NiobiAppReviewerExclusionRule(
 *   id = "has_conflict_of_interest",
 *   label = @Translation("Has Conflict of Interest"),
 * )
 */
class HasConflictOfInterest extends PluginBase implements NiobiAppReviewerExclusionRuleInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description()
  {
    return $this->t('The reviewer has a conflict of interest with the applicant.');
  }

  /**
   * @param User $reviewer
   * @param NiobiApplication $application
   * @param NiobiForm|NULL $review_form
   * @return bool
   */
  public static function isExcluded(User $reviewer, NiobiApplication $application, NiobiForm $review_form = NULL) {
    $applicant = $application->getOwner();

    $query = \Drupal::entityQuery('niobi_conflict_of_interest');
    $query->condition('type', 'conflict_with_another_user');
    $query->condition('field_reviewer', [$applicant->id(), $reviewer->id()], 'IN');
    $query->condition('field_applicant', [$applicant->id(), $reviewer->id()], 'IN');

    $result = $query->execute();

    return !empty($result);
  }
}