<?php

namespace Drupal\niobi_app\Plugin\niobi_app\ReviewerExclusionRule;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage;
use Drupal\niobi_app\NiobiAppReviewerExclusionRuleInterface;
use Drupal\niobi_form\Entity\NiobiForm;
use Drupal\user\Entity\User;

/**
 * @NiobiAppReviewerExclusionRule(
 *   id = "is_nominator",
 *   label = @Translation("Reviewer is the Nominator"),
 * )
 */
class IsNominator extends PluginBase implements NiobiAppReviewerExclusionRuleInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('The nominator may not review an application for an applicant they nominated.');
  }

  /**
   * @param User $reviewer
   * @param NiobiApplication $application
   * @return bool
   */
  public static function isExcluded(User $reviewer, NiobiApplication $application, NiobiForm $review_form = NULL) {
    return $application->isNominator($reviewer->id());
  }
}