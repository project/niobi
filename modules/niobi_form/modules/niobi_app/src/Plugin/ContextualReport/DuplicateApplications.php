<?php

namespace Drupal\niobi_app\Plugin\ContextualReport;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\contextual_reports\Plugin\ContextualReportBase;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\user\Entity\User;

/**
 * Orphan Submissions Report
 * @ContextualReport (
 *   id = "niobi_app_duplicate_applications",
 *   label = "Duplicate Applications",
 *   category = "niobi_app_troubleshooting"
 * )
 */
class DuplicateApplications extends ContextualReportBase  {


  /**
   * @param mixed $data
   * @param array $params
   * @return array
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public static function generateReport($data, array $params = []) {

    $owners = $data;
    $duplicate_owners = [];
    foreach($owners as $uid => $apps) {
      if (count($apps) > 1) {
        $duplicate_owners[$uid] = $apps;
      }
    }


    $page = [];
    $page['header'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => t('Potential Duplicate Applications'),
    ];
    $page['table'] = [
      '#type' => 'table',
      '#header' => [
        t('User'),
        t('Applications'),
      ],
    ];
    $page['table']['#rows'] = [];

    $page['#duplicates_count'] = 0;
    foreach ($duplicate_owners as $uid => $apps) {
      $user = User::load($uid);
      $row = [
        'user' => $user->toLink($user->label(),'canonical'),
        'applications' => [],
      ];
      /* @var $apps NiobiApplication[] */
      foreach ($apps as $app) {
        $row['applications'][] = $app->toLink($app->label(), 'canonical')->toString()->getGeneratedLink();
        $page['#duplicates_count'] += 1;
      }
      $row['applications'] = implode(', ', $row['applications']);
      $row['applications'] = new FormattableMarkup($row['applications'],[]);
      $page['table']['#rows'][] = $row;

    }

    return $page;
  }

}
