<?php

namespace Drupal\niobi_app\Plugin\ContextualReport;

use Drupal\contextual_reports\ContextualReportsUtilities;
use Drupal\contextual_reports\Plugin\ContextualReportBase;

/**
 * Orphan Submissions Report
 * @ContextualReport (
 *   id = "niobi_app_app_stats",
 *   label = "Application Statistics",
 *   category = "niobi_app_reports"
 * )
 */
class ApplicationStatistics extends ContextualReportBase  {

  /**
   * @param mixed $data
   * @param array $params
   * @return array
   */
  public static function generateReport($data, array $params = []) {
    $page = [];
    $page['header'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => t('Application Statistics'),
    ];

    $app_count_report = ContextualReportsUtilities::generateReport('niobi_app_workflow_applications', 'niobi_app_applications_by_status', 'niobi_app_apps_by_status', $params);
    $page['counts'] = [
      '#type' => 'details',
      '#title' => t('Applications by status: %count total', ['%count' => $app_count_report['#application_count_data']['total']]),
      '#open' => TRUE,
      'report' => $app_count_report
    ];

    $duplicate_report = ContextualReportsUtilities::generateReport('niobi_app_workflow_applications', 'niobi_app_applications_by_user', 'niobi_app_duplicate_applications', $params);
    $page['duplicates'] = [
      '#type' => 'details',
      '#title' => t('Potential Duplicate Applications: %count', ['%count' => $duplicate_report['#duplicates_count']]),
      '#open' => FALSE,
      'report' => $duplicate_report
    ];

    $orphan_report = OrphanSubmissions::generateReport($data, $params);
    $orphans = count($orphan_report['table']['#rows']);
    $page['orphans'] = [
      '#type' => 'details',
      '#title' => t('Orphan Submissions: %count', ['%count' => $orphans]),
      '#open' => FALSE,
      'report' => $orphan_report
    ];

    return $page;
  }

}
