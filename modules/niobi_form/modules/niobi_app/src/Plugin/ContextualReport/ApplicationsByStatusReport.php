<?php

namespace Drupal\niobi_app\Plugin\ContextualReport;

use Drupal\contextual_reports\Plugin\ContextualReportBase;

/**
 * Orphan Submissions Report
 * @ContextualReport (
 *   id = "niobi_app_apps_by_status",
 *   label = "Applications By Status",
 *   category = "niobi_app_troubleshooting"
 * )
 */
class ApplicationsByStatusReport extends ContextualReportBase  {


  /**
   * @param mixed $data
   * @param array $params
   * @return array
   */
  public static function generateReport($data, array $params = []) {

    $applications = $data;
    $app_counts = ['total' => 0];
    $users = 0;
    foreach($applications as $status => $app_list) {
      $app_counts[$status] = count($applications[$status]);
      $app_counts['total'] += count($applications[$status]);
    }

    $status_labels = [
      'nomination' => t('Nomination'),
      'nomination_claim' => t('Nomination Ready to Claim'),
      'application' => t('Application In Progress'),
      'review' => t('In Review'),
      'decision' => t('Decision Pending'),
      'complete' => t('Complete'),
      'total' => t('Total'),
    ];

    $page = [];
    $page['header'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => t('Applications by Status'),
    ];
    $page['table'] = [
      '#type' => 'table',
      '#header' => [
        t('Status'),
        t('Count'),
      ],
    ];
    $page['table']['#rows'] = [];

    foreach ($status_labels as $status => $label) {
      if (isset($app_counts[$status])) {
        $page['table']['#rows'][] = [
          'status' => $label,
          'count' => $app_counts[$status],
        ];
      }
    }

    $page['#application_count_data'] = $app_counts;

    return $page;
  }

}
