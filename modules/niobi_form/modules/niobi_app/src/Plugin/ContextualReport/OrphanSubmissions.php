<?php

namespace Drupal\niobi_app\Plugin\ContextualReport;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\contextual_reports\Plugin\ContextualReportBase;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Orphan Submissions Report
 * @ContextualReport (
 *   id = "niobi_app_orphan_submissions",
 *   label = "Orphan Submissions",
 *   category = "niobi_app_troubleshooting"
 * )
 */
class OrphanSubmissions extends ContextualReportBase  {

  /**
   * @param $application_id
   * @param $submission
   * @return bool
   */
  public static function isOrphan($application_id, WebformSubmission $submission) {
    // Look for an application with a given UUID.
    $query = \Drupal::entityQuery('niobi_application');
    $query->condition('uuid', $application_id);
    // Also check that the application has the submission.
    $query->orConditionGroup()
      ->condition('field_nomination_submissions', $submission->id(), 'IN')
      ->condition('field_application_submissions', $submission->id(), 'IN')
      ->condition('field_review_submissions', $submission->id(), 'IN');
    $result = $query->execute();

    // If we couldn't find an application, then the submission is an orphan.
    return empty($result);
  }

  public static function getPotentialMatches($application_id, WebformSubmission $submission) {
    // Check for matching authors.
    $query = \Drupal::entityQuery('niobi_application')
      ->condition('user_id', $submission->getOwnerId());
    $result = $query->execute();
    if ($result) {
      return NiobiApplication::loadMultiple($result);
    }

    // If we couldn't find an application, then we will need to manual search.
    return [];
  }

  /**
   * @param mixed $data
   * @param array $params
   * @return array
   */
  public static function generateReport($data, array $params = []) {

    $entities = $data;

    $data = [];
    foreach ($entities as $sub_id => $submission) {
      /* @var $submission \Drupal\webform\Entity\WebformSubmission */
      $is_orphan = self::isOrphan($submission->getElementData('application_id'), $submission);
      if ($is_orphan) {
        $data[$sub_id] = [
          'application_id' => $submission->getElementData('application_id'),
          'webform' => $submission->getWebform(),
          'submission' => $submission,
          'user' => $submission->getOwner(),
          'uuid' => $submission->uuid(),
          'date' => $submission->getCreatedTime(),
        ];
        // Look for potential matches
        $data[$sub_id]['potential_matches'] = self::getPotentialMatches($submission->getElementData('application_id'), $submission);
      }
    }

    $page = [];
    $page['header'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => 'Orphan Webform submissions',
    ];
    $page['description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'These are webform submissions which are not attached to an application. If the author has submitted an application, it will be listed as a potential match.',
    ];
    $page['table'] = [
      '#type' => 'table',
      '#header' => [
        t('Submission'),
        t('Form'),
        t('User'),
        t('Potential Matches'),
      ],
    ];
    $page['table']['#rows'] = [];

    foreach ($data as $d) {
      $potential_matches = [];
      foreach ($d['potential_matches'] as $pm) {
        $potential_matches[] = $pm->toLink($pm->label(), 'canonical')->toString()->getGeneratedLink();
      }
      $page['table']['#rows'][] = [
        'submission' => $d['submission']->toLink($d['submission']->label(), 'canonical')->toString(),
        'webform' => $d['webform']->toLink($d['webform']->label(), 'canonical')->toString(),
        'user' => $d['user']->toLink($d['user']->label(), 'canonical')->toString(),
        'suggested' => !empty($potential_matches) ? ['data' => new FormattableMarkup(implode(',', $potential_matches), [])] : 'Search Required'
      ];
    }

    return $page;
  }

}
