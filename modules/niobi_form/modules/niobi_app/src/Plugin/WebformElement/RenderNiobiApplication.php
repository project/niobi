<?php

namespace Drupal\niobi_app\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_app\Form\NiobiApplicationSubmissions;
use Drupal\webform\Plugin\WebformElementBase;

use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a 'Niobi Application' element.
 *
 * @WebformElement(
 *   id = "render_niobi_application",
 *   label = @Translation("Niobi Application"),
 *   description = @Translation("Renders a Niobi application based on the 'application_id' parameter."),
 *   category = @Translation("Basic elements"),
 * )
 */
// *   api = "https://drupal.org/project/niobi",
class RenderNiobiApplication extends WebformElementBase {


  /**
   * {@inheritdoc}
   */
  public function isInput(array $element) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isContainer(array $element) {
    return FALSE;
  }

  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);
    //dpm($element);

    $app_id = 0;
    $uuid = Request::createFromGlobals()->get('application_id');
    if (is_string($uuid) && (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $uuid) === 1)) {
      $s = \Drupal::entityTypeManager()->getStorage('niobi_application')->loadByProperties(['uuid' => $uuid]);
      if (!empty($s)) {
        $app_id = array_keys($s)[0];
      }
    }
    if ($app_id) {
      $form = \Drupal::formBuilder()->getForm(NiobiApplicationSubmissions::class, $app_id);
      $renderer = \Drupal::service('renderer');

      // Get the tabs string and set as the element markup.
      $element['#markup'] = $renderer->render($form)->__toString();
      $element = $this->generateElements($element ,$app_id);

    }
    else {
      $element['#type'] = 'markup';
      $element['#markup'] = '';
    }

  }

  public function generateElements($element, $application_id) {
    $element['#access'] = TRUE;
    $element['#webform_element'] = TRUE;
    $element['#type'] = 'render_niobi_application';
    $element['#suffix'] = 'markup';
    return $element;
  }

  /**
   * @inheritdoc
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    return $form;
  }

}
