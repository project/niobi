<?php

namespace Drupal\niobi_app\Plugin\ContextualReportData;

use Drupal\contextual_reports\Plugin\ContextualReportDataBase;
use Drupal\niobi_app\Entity\NiobiApplication;

/**
 * Get webform submissions from workflow.
 *
 * @ContextualReportData (
 *   id = "niobi_app_applications_by_user",
 *   label = "Applications: Workflow submissions",
 *   description = "List of webform submissions by author ID",
 *   category = "niobi_app"
 * )
 */
class ApplicationsByUser extends ContextualReportDataBase {

  /**
   * Generates a list of user names.
   *
   * @param array $entity_ids
   *   List of user entity IDs.
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   *
   * @return array
   *   Array of data to be used by a report function.
   */
  public static function generateReportData(array $entity_ids, array $params = []) {
    $data = [];
    $apps = NiobiApplication::loadMultiple($entity_ids['niobi_application']);
    /* @var $apps \Drupal\niobi_app\Entity\NiobiApplication[] */
    foreach ($apps as $app) {
      $data[$app->getOwnerId()][] = $app;
    }
    return $data;
  }

}
