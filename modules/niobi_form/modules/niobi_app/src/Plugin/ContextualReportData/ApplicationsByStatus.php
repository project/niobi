<?php

namespace Drupal\niobi_app\Plugin\ContextualReportData;

use Drupal\contextual_reports\Plugin\ContextualReportDataBase;
use Drupal\niobi_app\Entity\NiobiApplication;

/**
 * Get webform submissions from workflow.
 *
 * @ContextualReportData (
 *   id = "niobi_app_applications_by_status",
 *   label = "Applications: Applications by Status",
 *   description = "List of webform submissions",
 *   category = "niobi_app"
 * )
 */
class ApplicationsByStatus extends ContextualReportDataBase {

  /**
   * Generates a list of user names.
   *
   * @param array $entity_ids
   *   List of user entity IDs.
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   *
   * @return array
   *   Array of data to be used by a report function.
   */
  public static function generateReportData(array $entity_ids, array $params = []) {
    $data = [];
    $apps = NiobiApplication::loadMultiple($entity_ids['niobi_application']);
    foreach ($apps as $app) {
      $data[$app->field_application_status->value][] = $app;
    }
    return $data;
  }

}
