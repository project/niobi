<?php

namespace Drupal\niobi_app\Plugin\ContextualReportData;

use Drupal\contextual_reports\Plugin\ContextualReportDataBase;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Get webform submissions from workflow.
 *
 * @ContextualReportData (
 *   id = "niobi_app_workflow_submissions",
 *   label = "Applications: Workflow submissions",
 *   description = "List of webform submissions",
 *   category = "niobi_app"
 * )
 */
class WorkflowSubmissionsData extends ContextualReportDataBase {

  /**
   * Generates a list of user names.
   *
   * @param array $entity_ids
   *   List of user entity IDs.
   * @param array $params
   *   Associative array of parameters for modifying behavior.
   *
   * @return array
   *   Array of data to be used by a report function.
   */
  public static function generateReportData(array $entity_ids, array $params = []) {
    return WebformSubmission::loadMultiple($entity_ids['webform_submission']);
  }

}
