<?php
namespace Drupal\niobi_app\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;

/**
 * Creates a Niobi Application Workflow Review Assignments Block
 * @Block(
 * id = "niobi_app_workflow_review_assignments",
 * admin_label = @Translation("Niobi Application Workflow Review Assignments Block"),
 * )
 */
class NiobiAppWorkflowReviewAssignmentsBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $user = \Drupal::currentUser();
    if ($user->isAuthenticated()) {
      // Create an object of type Select.
      $database = \Drupal::database();
      $query = $database->select('niobi_application__field_application_workflow', 'aw');
      $query->innerJoin('task__field_application', 'ta', 'ta.field_application_target_id = aw.entity_id');
      $query->innerJoin('task_field_data', 'td', 'td.id = ta.entity_id');
      $query->condition('td.type', 'application_review_task');
      $query->condition('td.assigned_to', $user->id());
      $query->condition('td.status', 'closed', '<>');
      $query->fields('aw', ['field_application_workflow_target_id']);
      $workflowIds = $query->execute()->fetchCol();
      if (!empty($workflowIds)) {
        $workflows = NiobiApplicationWorkflow::loadMultiple($workflowIds);
        $items = [];
        foreach ($workflows as $workflow) {
          $items[] = Link::createFromRoute($workflow->label(), 'niobi_application.manage_user_review_assignments',
            ['niobi_application_workflow' => $workflow->id()]);
        }
        return [
          'header' => [
            '#type' => 'html_tag',
            '#tag' => 'h3',
            '#value' => t('Review Assignments'),
          ],
          'items' => [
            '#theme' => 'item_list',
            '#items' => $items,
          ],
        ];
      }
    }
  }

  public function getCacheMaxAge() {
    return 0;
  }
}
