<?php

namespace Drupal\niobi_app\Utilities;

use Drupal\niobi_app\Controller\NiobiAppController;
use Drupal\niobi_app\Entity\NiobiApplicationInterface;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\niobi_message\Plugin\task\Bundle\NotificationTask;
use Drupal\task\Entity\Task;

class NiobiAppPromoteUtilities {

  /**
   * @param NiobiApplication $application
   * @return bool
   */
  public static function applicationReadyToSubmit(NiobiApplication $application) {
    $stage = $application->getCurrentApplicationStage();
    $stage_forms = $stage->getAllForms();
    $subs = $application->getAllSubmissionsbyWebform();

    // First, the application form must exist, and be completed.
    if (!NiobiAppPromoteUtilities::applicationCompleteStatus($application, $stage, $stage_forms, $subs)) {
      return FALSE;
    }

    // Now we check required addenda
    if (!NiobiAppPromoteUtilities::applicationRequiredAddendaCompleteStatus($application, $stage, $stage_forms, $subs)) {
      return FALSE;
    }

    // Finally, check the support letters.
    if (!NiobiAppPromoteUtilities::applicationSupportLetterCompleteStatus($application, $stage, $stage_forms, $subs)) {
      return FALSE;
    }

    // We are in fact complete. Return TRUE.
    return TRUE;
  }

  /**
   * @param $application
   * @param $stage
   * @param $stage_forms
   * @param $subs
   * @return bool
   */
  public static function applicationCompleteStatus ($application, $stage, $stage_forms, $subs) {
    if (!empty($stage_forms['application']) && empty($subs['application'])) {
      return FALSE;
    }
    // Next, check the application submission is not a draft.
    elseif(!empty($stage_forms['application']) && !empty($subs['application'])) {
      $app_form = array_shift($stage_forms['application']);
      foreach ($subs['application'] as $sub_form => $sub_list) {
        if($app_form->getWebform()->id() === $sub_form) {
          // There should only be one application submission.
          $sub = array_shift($sub_list);
          if ($sub->isDraft()) {
            return FALSE;
          }
        }
      }
    }
    // We are in fact complete. Return TRUE.
    return TRUE;
  }

  /**
   * @param $application
   * @param $stage
   * @param $stage_forms
   * @param $subs
   * @return bool
   */
  public static function applicationRequiredAddendaCompleteStatus ($application, $stage, $stage_forms, $subs) {
    if(!empty($stage_forms['required_addenda']) && empty($subs['application'])) {
      return FALSE;
    }
    elseif(!empty($stage_forms['required_addenda']) && !empty($subs['application'])) {
      foreach($stage_forms['required_addenda'] as $addenda_form) {
        $addenda_form_id = $addenda_form->getWebform()->id();
        // A required form is missing a submission.
        if (!isset($subs['application'][$addenda_form_id])) {
          return FALSE;
        }
        else {
          $sub = array_shift($subs['application'][$addenda_form_id]);
          if ($sub->isDraft()) {
            return FALSE;
          }
        }
      }
    }
    // We are in fact complete. Return TRUE.
    return TRUE;
  }

  /**
   * @param $application
   * @param $stage
   * @param $stage_forms
   * @param $subs
   * @return int
   */
  public static function applicationSupportLetterCompleteCount ($application, $stage, $stage_forms, $subs) {
    if(!empty($stage_forms['support']) && !empty($subs['application'])) {
      foreach($stage_forms['support'] as $support_form) {
        if ($support_form) {
          $support_form_id = $support_form->getWebform()->id();
          // A required form is missing a submission.
          if (isset($subs['application'][$support_form_id])) {
            $count = 0;
            $info = $stage->getSupportLetterInformation();
            if ($info['form']) {
              $count = 0;
              if (isset($subs['application'][$support_form_id])) {
                foreach ($subs['application'][$support_form_id] as $item) {
                  if (!$item->isDraft()) {
                    $count += 1;
                  }
                }
              }
            }
            return $count;
          }
        }
      }
    }
    // We have a condition that prevents counting, so return zero.
    return 0;
  }

  /**
   * @param $application
   * @param $stage
   * @param $stage_forms
   * @param $subs
   * @return bool
   */
  public static function applicationSupportLetterCompleteStatus ($application, $stage, $stage_forms, $subs) {
    if(!empty($stage_forms['support']) && empty($subs['application'])) {
      return FALSE;
    }
    elseif(!empty($stage_forms['support']) && !empty($subs['application'])) {
      foreach($stage_forms['support'] as $support_form) {
        if ($support_form) {
          $support_form_id = $support_form->getWebform()->id();
          // A required form is missing a submission.
          if (!isset($subs['application'][$support_form_id])) {
            return FALSE;
          }
          else {
            $count = 0;
            $info = $stage->getSupportLetterInformation();
            if ($info['form']) {
              $count_req = !empty($info['count']) ? $info['count'] : 1;
              $count = NiobiAppPromoteUtilities::applicationSupportLetterCompleteCount($application, $stage, $stage_forms, $subs);
            }
            if ($count < $count_req) {
              return FALSE;
            }
          }
        }
      }
    }
    // We are in fact complete. Return TRUE.
    return TRUE;
  }

  /**
   * @param NiobiApplicationInterface $niobi_application
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function submitApplication(NiobiApplicationInterface $niobi_application) {
    $niobi_application = NiobiApplication::load($niobi_application->id());
    $stage = $niobi_application->getCurrentApplicationStage();

    // Run any trigger tasks
    $query = \Drupal::entityQuery('task');
    $query->condition('type', 'niobi_notification_task');
    $query->condition('field_trigger_type', 'niobi_app_submit_application');
    $query->condition('status', 'active');
    $result = $query->execute();

    $tasks = Task::loadMultiple($result);

    foreach ($tasks as $task) {
      NotificationTask::triggerTask($task);
    }

    // Run review/decision logic.
    /* @var $stage \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage */
    $review_forms = $stage->getReviewForms(TRUE);

    if (!empty($review_forms)) {
      // Run auto-assignment
      NiobiAppController::runAutoAssignment($niobi_application);

      // Set as "in review" .
      $niobi_application->set('field_application_status', 'review');
      $niobi_application->save();
    }
    else {
      NiobiAppPromoteUtilities::moveToDecision($niobi_application);
    }
  }

  public static function moveToDecision(NiobiApplicationInterface $niobi_application) {
    $application = NiobiApplication::load($niobi_application->id());
    $stage = $application->getCurrentApplicationStage();
    /* @var $stage \Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage */
    $auto_decision = $stage->getAutoDecision();
    if ($auto_decision) {
      $auto_decision['class']::autoDecision($application);
    }
    else {
      $application->set('field_application_status', 'decision')->save();
    }
  }

}
