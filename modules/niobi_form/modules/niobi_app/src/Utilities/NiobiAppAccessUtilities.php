<?php

namespace Drupal\niobi_app\Utilities;

use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\niobi_app\Controller\NiobiAppController;
use Drupal\niobi_app\Entity\NiobiApplicationInterface;
use Drupal\niobi_app\Entity\NiobiApplicationReviewerPool;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflow;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflowInterface;
use Drupal\niobi_app\Entity\NiobiApplicationWorkflowStage;
use Drupal\niobi_app\Entity\NiobiApplication;
use Drupal\niobi_form\Entity\NiobiForm;
use Drupal\niobi_message\Plugin\task\Bundle\NotificationTask;
use Drupal\task\Entity\Task;
use Drupal\user\UserInterface;
use Drupal\webform\Entity\Webform;

/**
 * Class NiobiAppAccessUtilities
 * @package Drupal\niobi_app
 */
class NiobiAppAccessUtilities {

  /**
   * @param NiobiApplicationWorkflow $workflow
   * @return array
   */
  public static function getAdminTeams(NiobiApplicationWorkflow $workflow, $list_teams = FALSE) {
    $ret = [];
    $ret['owner'] = $workflow->getOwnerId();
    $ret['admin'] = $workflow->getAdminTeam();
    $ret['view'] = $workflow->getViewAccessTeam();
    if (!$list_teams) {
      $ret_flat = [];
      foreach ($ret as $key => $arr) {
        foreach ($arr as $val) {
          !in_array($val, $ret_flat) ? $ret_flat[] = $val : NULL;
        }
      }
      return $ret_flat;
    }

    return $ret;
  }

  /**
   * @param NiobiApplicationWorkflowStage $stage
   * @param bool $list_teams
   * @param bool $include_auto
   * @return array
   */
  public static function getReviewersForStage(NiobiApplicationWorkflowStage $stage, $list_teams = FALSE, $include_auto = TRUE) {
    $pools = $stage->getReviewerPools($include_auto);
    $ret = [];
    foreach ($pools as $pool) {
      /* @var $pool NiobiApplicationReviewerPool */
      $pool_reviewers = array_keys($pool->getReviewers());
      if ($list_teams) {
        $ret[$pool->id()] = $pool_reviewers;
      }
      else {
        foreach ($pool_reviewers as $pr) {
          if (!in_array($pr, $ret)) {
            $ret[] = $pr;
          }
        }
      }
    }
    return $ret;
  }

  public static function updateAccessForWorkflow(NiobiApplicationWorkflow $workflow) {
    $access_team = NiobiAppAccessUtilities::getAdminTeams($workflow);
    $stages = $workflow->getStages();
    self::updateWebformAccess($access_team, $stages);
  }

  public static function updateAccessForReviewerPool(NiobiApplicationReviewerPool $pool) {
    $access_team = array_keys($pool->getReviewers());
    $stages = $pool->getReferencingStages();
    self::updateWebformAccess($access_team, $stages);
  }

  public static function updateWebformAccess(array $access_team, array $stages) {
    foreach ($stages as $stage) {
      $stage_forms = $stage->getAllForms();
      foreach ($stage_forms as $form_set) {
        foreach ($form_set as $niobi_form) {
          /* @var $niobi_form \Drupal\niobi_form\Entity\NiobiForm */
          if (!empty($niobi_form) && !is_array($niobi_form)) {
            $webform = $niobi_form->getWebform();
            if (!empty($webform)) {
              /* @var $webform \Drupal\webform\Entity\Webform */
              self::applySettings($webform, $access_team);
            }
          }
          elseif (!empty($niobi_form) && is_array($niobi_form)) {
            foreach ($niobi_form as $nf) {
              $webform = $nf->getWebform();
              if (!empty($webform)) {
                /* @var $webform \Drupal\webform\Entity\Webform */
                self::applySettings($webform, $access_team);
              }
            }
          }
        }
      }
    }
  }

  /**
   * @param Webform $webform
   * @param array $access_team
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private static function applySettings(Webform $webform, array $access_team) {
    $access = $webform->getAccessRules();
    $access_team = array_merge($access['view_any']['users'], $access_team);
    // Cleanup, in case someone was double added in another context.
    $access_team = array_unique($access_team);

    $access['view_any']['users'] = $access_team;
    $webform->setAccessRules($access)->save();
  }

}
