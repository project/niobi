<?php

namespace Drupal\niobi_app\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RenderNiobiApplication
 * @package Drupal\niobi_app\Element
 * @FormElement("render_niobi_application")
 *
 * @see \Drupal\Core\Render\Element\FormElement
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement
 * @see \Drupal\Core\Render\Element\RenderElement
 * @see https://api.drupal.org/api/drupal/namespace/Drupal%21Core%21Render%21Element
 * @see \Drupal\render_niobi_application\Element\RenderNiobiApplication
 */
class RenderNiobiApplication extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#process' => [
        [$class, 'processRenderNiobiApplication'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateRenderNiobiApplication'],
      ],
      '#pre_render' => [
        [$class, 'preRenderRenderNiobiApplication'],
      ],
      '#theme' => 'input__render_niobi_application',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Processes a 'render_niobi_application' element.
   */
  public static function processRenderNiobiApplication(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here you can add and manipulate your element's properties and callbacks.
    $element['#type'] = 'markup';
    return $element;
  }

  /**
   * Webform element validation handler for #type 'render_niobi_application'.
   */
  public static function validateRenderNiobiApplication(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here you can add custom validation logic.
  }

  /**
   * Prepares a #type 'email_multiple' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderRenderNiobiApplication(array $element) {
    return $element;
  }

}
