<?php

namespace Drupal\niobi_app;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Niobi Application Workflow entity.
 *
 * @see \Drupal\niobi_app\Entity\NiobiApplicationWorkflow.
 */
class NiobiApplicationWorkflowAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\niobi_app\Entity\NiobiApplicationWorkflowInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished niobi application workflow entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published niobi application workflow entities');

      case 'update':
        // TODO: Add an access option that covers letting admins edit the workflow entity.
        if ($entity->isOnAdminTeam($account->id())) {
          return AccessResult::allowed();
        }
        return AccessResult::allowedIfHasPermission($account, 'edit niobi application workflow entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete niobi application workflow entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add niobi application workflow entities');
  }

}
