<?php

/**
 * @file
 * Contains niobi_application_workflow.page.inc.
 *
 * Page callback for Niobi Application Workflow entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Niobi Application Workflow templates.
 *
 * Default template: niobi_application_workflow.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_niobi_application_workflow(array &$variables) {
  // Fetch NiobiApplicationWorkflow Entity Object.
  $niobi_application_workflow = $variables['elements']['#niobi_application_workflow'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
