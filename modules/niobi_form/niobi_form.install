<?php

/**
 * @file
 * Install, update and uninstall functions for the niobi_form module.
 */

use Drupal\Core\Config\ConfigFactoryOverrideInterface;

/**
 * Implements hook_requirements().
 */
function niobi_form_requirements($phase) {
  $requirements = [];

  if ($phase == 'install') {
    $manager = \Drupal::entityTypeManager();
    if ($manager->hasDefinition('niobi_form_type') && ($niobi_form_type = $manager->getStorage('niobi_form_type')->load('webform'))) {
      $requirements['niobi_form'] = [
        'title' => t('Niobi Form'),
        'description' => t('%title content type already exists, please delete the %title content type before installing the Niobi Form module.', ['%title' => $niobi_form_type->label()]),
        'severity' => REQUIREMENT_ERROR,
      ];
    }

  }
  return $requirements;
}

/**
 * Implements hook_install().
 */
function niobi_form_install() {
  \Drupal::service('config.installer')->installDefaultConfig('module', 'niobi_form');
  /**
   * Install the custom configs for group & department dashboards
   */
  $group_config = \Drupal::configFactory()->getEditable('core.entity_view_display.group.group.default');
  $group_settings = $group_config->getOriginal('third_party_settings');
  $group_settings['field_group']['group_forms'] = array(
    'children' => array('niobi_form_forms_in_group_entity_view_1'),
    'label' => 'Forms',
    'parent_name' => 'group_group_tabs',
    'weight' => '22',
    'region' => 'content',
    'format_type' => 'tab',
    'format_settings' => array(
      'id' => '',
      'classes' => '',
      'formatter' => 'closed',
      'description' => '',
    ),
  );
  $group_settings['field_group']['group_group_tabs']['children'][] = 'group_forms';
  $group_config->set('third_party_settings', $group_settings)->save();

  /**
   * Install Single Form config on group dashboard
   */
  $group_storage_config = \Drupal::configFactory()->getEditable('views.view.niobi_form_forms_in_group');
  $group_settings = $group_storage_config->getOriginal('display');
  $group_settings['default']['display_options']['header']['views_add_button_single_form'] = array(
    'id' => 'views_add_button',
    'table' => 'views',
    'field' => 'views_add_button',
    'relationship' => 'none',
    'group_type' => 'group',
    'admin_label' => '',
    'empty' => TRUE,
    'tokenize' => TRUE,
    'type' => 'group_content+group_content_type_0f5fe3baa72a1',
    'context' => '{{ raw_arguments.gid }}',
    'button_text' => '+ Add Single Form',
    'button_classes' => 'btn btn-success button',
    'button_attributes' => '',
    'button_prefix' => [
      'value' => '',
      'format' => 'basic_html',
    ],
    'button_suffix' => [
      'value' => '&nbsp;&nbsp;',
      'format' => 'basic_html'
    ],
    'query_string' => '',
    'destination' => TRUE,
    'plugin_id' => 'views_add_button_area',
  );
  $group_storage_config->set('display', $group_settings)->save();
}

/**
 * Implements hook_uninstall().
 */
function niobi_form_uninstall() {
  /**
   * Uninstall custom configs for group dashboard
   */
  $group_config = \Drupal::configFactory()->getEditable('core.entity_view_display.group.group.default');
  $group_settings = $group_config->getOriginal('third_party_settings');
  unset($group_settings['field_group']['group_forms']);
  $group_settings['field_group']['group_group_tabs']['children'] = array_diff($group_settings['field_group']['group_group_tabs']['children'], ['group_forms']);
  $group_config->set('third_party_settings', $group_settings)->save();

  \Drupal::service('config.manager')->uninstall('module', 'niobi_form');
}