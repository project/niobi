<?php

/**
 * @file
 * Contains niobi_form.page.inc.
 *
 * Page callback for Niobi Form entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Niobi Form templates.
 *
 * Default template: niobi_form.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_niobi_form(array &$variables) {
  // Fetch NiobiForm Entity Object.
  $niobi_form = $variables['elements']['#niobi_form'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
