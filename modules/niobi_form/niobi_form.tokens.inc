<?php

/**
 * @file
 * Builds placeholder replacement tokens for webform Niobi forms.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\niobi_form\Entity\NiobiFormInterface;

/**
 * Implements hook_token_info().
 */
function niobi_form_token_info() {
  $types['webform_submission'] = [
    'name' => t('Webform submissions'),
    'description' => t('Tokens related to webform submission.'),
    'needs-data' => 'webform_submission',
  ];
  $webform_submission['niobi_form'] = [
    'name' => t('Niobi Form'),
    'description' => t("The niobi_form that the webform was submitted from."),
    'type' => 'niobi_form',
  ];

  return [
    'types' => $types,
    'tokens' => [
      'webform_submission' => $webform_submission,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function niobi_form_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $token_service = \Drupal::token();

  $replacements = [];

  if ($type == 'webform_submission' && !empty($data['webform_submission'])) {
    /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
    $webform_submission = $data['webform_submission'];
    $source_entity = $webform_submission->getSourceEntity(TRUE);
    if (!$source_entity || (!$source_entity instanceof NiobiFormInterface)) {
      return $replacements;
    }

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'niobi_form':
          $replacements[$original] = $source_entity->label();
          break;
      }
    }

    if ($entity_tokens = $token_service->findWithPrefix($tokens, 'niobi_form')) {
      $replacements += $token_service->generate('niobi_form', $entity_tokens, ['niobi_form' => $source_entity], $options, $bubbleable_metadata);
    }
  }

  return $replacements;
}
