<?php

namespace Drupal\niobi_message\Plugin\task\Bundle;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\message\Entity\Message;
use Drupal\task\Entity\Task;
use Drupal\task\Entity\TaskInterface;
use Drupal\task\TaskBundleInterface;
use Drupal\user\Entity\User;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Niobi Notification Task
 *
 * These do not expire in the normal sense, but instead are triggered.
 *
 * @TaskBundle(
 *   id = "niobi_notification_task",
 *   label = @Translation("Notification Task"),
 *   bundle = "niobi_notification_task",
 *   system_task = TRUE
 * )
 */
class NotificationTask extends PluginBase implements TaskBundleInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description()
  {
    return $this->t('Actions for Application Review Task');
  }

  /**
   * @param $task_data
   * @return \Drupal\Core\Entity\EntityInterface|Task
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function createTask($task_data) {
    $task = Task::create($task_data);
    $data = $task->get('task_data')->getValue();
    $task->save();

    // Now run the usual task create actions.
    if (isset($data[0]['actions']['create'])) {
      $action_data = $data[0]['actions']['create'];
      foreach ($action_data as $id => $data) {
        $plugin_manager = \Drupal::service('plugin.manager.task_action');
        $plugin_definitions = $plugin_manager->getDefinitions();
        if(isset($plugin_definitions[$id])) {
          $plugin_definitions[$id]['class']::doAction($task, $data);
        }
      }
    }
    return $task;
  }

  /**
   * @param $task
   * @return mixed
   */
  public static function expireTask($task) {
    // Do nothing.
    return $task;
  }

  /**
   * @param $task
   * @return mixed
   */
  public static function triggerTask($task) {
    $data = $task->get('task_data')->getValue();
    if (isset($data[0]['actions']['trigger'])) {
      $action_data = $data[0]['actions']['trigger'];
      foreach ($action_data as $id => $data) {
        $plugin_manager = \Drupal::service('plugin.manager.task_action');
        $plugin_definitions = $plugin_manager->getDefinitions();
        if(isset($plugin_definitions[$id])) {
          $plugin_definitions[$id]['class']::doAction($task, $data);
        }
      }
    }
    $task->set('status', 'closed');
    $task->set('close_date', time());
    $task->set('close_type', 'completed');
    $task->save();
    return $task;
  }

  /**
   * @param TaskInterface $task
   * @return array
   */
  public static function getTaskOptions(TaskInterface $task) {
    $url_dismiss = Url::fromRoute('task.dismiss', ['task' => $task->id()], ['attributes' => ['class' => 'btn btn-danger button']]);
    $link_dismiss = Link::fromTextAndUrl('Dismiss', $url_dismiss);
    return ['#type' => 'markup', '#markup' => new FormattableMarkup(implode(' ', [$link_dismiss->toString()]), [])];
  }

}
