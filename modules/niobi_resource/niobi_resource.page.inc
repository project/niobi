<?php

/**
 * @file
 * Contains niobi_resource.page.inc.
 *
 * Page callback for Niobi Resource entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Niobi Resource templates.
 *
 * Default template: niobi_resource.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_niobi_resource(array &$variables) {
  // Fetch NiobiResource Entity Object.
  $niobi_resource = $variables['elements']['#niobi_resource'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
