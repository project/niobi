<?php

namespace Drupal\niobi_resource\Plugin\GroupContentEnabler;

use Drupal\niobi_resource\Entity\NiobiResourceType;
use Drupal\Component\Plugin\Derivative\DeriverBase;

class GroupNiobiResourceDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach (NiobiResourceType::loadMultiple() as $name => $niobi_resource_type) {
      $label = $niobi_resource_type->label();
      $this->derivatives[$name] = [
        'entity_bundle' => $name,
        'label' => t('Resource (@type)', ['@type' => $label]),
        'description' => t('Adds %type content to groups both publicly and privately.', ['%type' => $label]),
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
