<?php

namespace Drupal\niobi_resource\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Niobi Resource type entities.
 */
interface NiobiResourceTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
