<?php

namespace Drupal\niobi_resource\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Niobi Resource entities.
 *
 * @ingroup niobi_resource
 */
interface NiobiResourceInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Niobi Resource name.
   *
   * @return string
   *   Name of the Niobi Resource.
   */
  public function getName();

  /**
   * Sets the Niobi Resource name.
   *
   * @param string $name
   *   The Niobi Resource name.
   *
   * @return \Drupal\niobi_resource\Entity\NiobiResourceInterface
   *   The called Niobi Resource entity.
   */
  public function setName($name);

  /**
   * Gets the Niobi Resource creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Niobi Resource.
   */
  public function getCreatedTime();

  /**
   * Sets the Niobi Resource creation timestamp.
   *
   * @param int $timestamp
   *   The Niobi Resource creation timestamp.
   *
   * @return \Drupal\niobi_resource\Entity\NiobiResourceInterface
   *   The called Niobi Resource entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Niobi Resource published status indicator.
   *
   * Unpublished Niobi Resource are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Niobi Resource is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Niobi Resource.
   *
   * @param bool $published
   *   TRUE to set this Niobi Resource to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\niobi_resource\Entity\NiobiResourceInterface
   *   The called Niobi Resource entity.
   */
  public function setPublished($published);

}
