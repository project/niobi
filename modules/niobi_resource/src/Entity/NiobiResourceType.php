<?php

namespace Drupal\niobi_resource\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Niobi Resource type entity.
 *
 * @ConfigEntityType(
 *   id = "niobi_resource_type",
 *   label = @Translation("Niobi Resource type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\niobi_resource\NiobiResourceTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\niobi_resource\Form\NiobiResourceTypeForm",
 *       "edit" = "Drupal\niobi_resource\Form\NiobiResourceTypeForm",
 *       "delete" = "Drupal\niobi_resource\Form\NiobiResourceTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\niobi_resource\NiobiResourceTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "niobi_resource",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/niobi/niobi_resource_type/{niobi_resource_type}",
 *     "add-form" = "/admin/niobi/niobi_resource_type/add",
 *     "edit-form" = "/admin/niobi/niobi_resource_type/{niobi_resource_type}/edit",
 *     "delete-form" = "/admin/niobi/niobi_resource_type/{niobi_resource_type}/delete",
 *     "collection" = "/admin/niobi/niobi_resource_type"
 *   }
 * )
 */
class NiobiResourceType extends ConfigEntityBundleBase implements NiobiResourceTypeInterface {

  /**
   * The Niobi Resource type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Niobi Resource type label.
   *
   * @var string
   */
  protected $label;

}
