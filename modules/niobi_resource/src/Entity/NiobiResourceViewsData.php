<?php

namespace Drupal\niobi_resource\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Niobi Resource entities.
 */
class NiobiResourceViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
