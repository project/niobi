<?php

namespace Drupal\niobi_resource\Routing;

use Drupal\niobi_resource\Entity\NiobiResourceType;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for group_niobi_resource group content.
 */
class GroupNiobiResourceRouteProvider {

  /**
   * Provides the shared collection route for group niobi_resource plugins.
   */
  public function getRoutes() {
    $routes = $plugin_ids = $permissions_add = $permissions_create = [];

    foreach (NiobiResourceType::loadMultiple() as $name => $niobi_resource_type) {
      $plugin_id = "group_niobi_resource:$name";

      $plugin_ids[] = $plugin_id;
      $permissions_add[] = "create $plugin_id content";
      $permissions_create[] = "create $plugin_id entity";
    }

    // If there are no niobi_resource types yet, we cannot have any plugin IDs and should
    // therefore exit early because we cannot have any routes for them either.
    if (empty($plugin_ids)) {
      return $routes;
    }

    $routes['entity.group_content.group_niobi_resource_relate_page'] = new Route('group/{group}/niobi_resource/add');
    $routes['entity.group_content.group_niobi_resource_relate_page']
      ->setDefaults([
        '_title' => 'Relate Resource Content',
        '_controller' => '\Drupal\niobi_resource\Controller\GroupNiobiResourceController::addPage',
      ])
      ->setRequirement('_group_permission', implode('+', $permissions_add))
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE);

    $routes['entity.group_content.group_niobi_resource_add_page'] = new Route('group/{group}/niobi_resource/create');
    $routes['entity.group_content.group_niobi_resource_add_page']
      ->setDefaults([
        '_title' => 'Create Resource Content',
        '_controller' => '\Drupal\niobi_resource\Controller\GroupNiobiResourceController::addPage',
        'create_mode' => TRUE,
      ])
      ->setRequirement('_group_permission', implode('+', $permissions_create))
      ->setRequirement('_group_installed_content', implode('+', $plugin_ids))
      ->setOption('_group_operation_route', TRUE);

    return $routes;
  }

}
