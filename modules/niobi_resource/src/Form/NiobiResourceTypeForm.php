<?php

namespace Drupal\niobi_resource\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class NiobiResourceTypeForm.
 */
class NiobiResourceTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $niobi_resource_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $niobi_resource_type->label(),
      '#description' => $this->t("Label for the Niobi Resource type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $niobi_resource_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\niobi_resource\Entity\NiobiResourceType::load',
      ],
      '#disabled' => !$niobi_resource_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $niobi_resource_type = $this->entity;
    $status = $niobi_resource_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Niobi Resource type.', [
          '%label' => $niobi_resource_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Niobi Resource type.', [
          '%label' => $niobi_resource_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($niobi_resource_type->toUrl('collection'));
  }

}
