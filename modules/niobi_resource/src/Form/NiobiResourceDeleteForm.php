<?php

namespace Drupal\niobi_resource\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Niobi Resource entities.
 *
 * @ingroup niobi_resource
 */
class NiobiResourceDeleteForm extends ContentEntityDeleteForm {


}
