<?php

namespace Drupal\niobi_resource;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Niobi Resource entity.
 *
 * @see \Drupal\niobi_resource\Entity\NiobiResource.
 */
class NiobiResourceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\niobi_resource\Entity\NiobiResourceInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished niobi resource entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published niobi resource entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit niobi resource entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete niobi resource entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add niobi resource entities');
  }

}
