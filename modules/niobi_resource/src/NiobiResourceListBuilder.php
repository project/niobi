<?php

namespace Drupal\niobi_resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Niobi Resource entities.
 *
 * @ingroup niobi_resource
 */
class NiobiResourceListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Niobi Resource ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\niobi_resource\Entity\NiobiResource */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.niobi_resource.edit_form',
      ['niobi_resource' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
