<?php

namespace Drupal\niobi_resource;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for niobi_resource.
 */
class NiobiResourceTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
