<?php

namespace Drupal\niobi_pages\Plugin\niobi\admin;

use Drupal\Core\Plugin\PluginBase;
use Drupal\niobi_admin\NiobiAdminInterface;

/**
 * @NiobiAdmin(
 *   id = "niobi_pages",
 *   label = @Translation("Niobi Pages Settings"),
 *   description = @Translation("System Page Settings."),
 * )
 */
class NiobiPages extends PluginBase implements NiobiAdminInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('');
  }

  public static function getFormOptions() {
    $niobi_config = \Drupal::configFactory()->getEditable('niobi_admin.settings');
    $ret = [
      'niobi_pages' => [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => t('Homepage Dashboard'),
        '#group' => 'niobi',
        'niobi_pages__anonymous_homepage' => [
          '#type' => 'textfield',
          '#title' => t('Homepage for Anonymous Users'),
          '#description' => t('Anonymous users will be redirected to this location. Internal URLs should start with a slash (/).'),
          '#default_value' => $niobi_config->getOriginal('niobi_pages.anonymous_homepage'),
        ],
      ],
    ];
    return $ret;
  }

}