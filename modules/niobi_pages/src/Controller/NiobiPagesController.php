<?php

namespace Drupal\niobi_pages\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class NiobiGroupController
 * @package Drupal\niobi_pages
 */
class NiobiPagesController extends ControllerBase {

  /**
   * Creates the homepage dashboard.
   * @return $text;
   *   The page text to return
   */
  public function dashboard() {
    if (\Drupal::currentUser()->isAnonymous()) {
      $anon_url = \Drupal::configFactory()->getEditable('niobi_admin.settings')->getOriginal('niobi_pages.anonymous_homepage');
      $anon_url = !empty($anon_url) ? $anon_url : '/user/login';
      $url = Url::fromUserInput($anon_url);
      $response = new RedirectResponse($url->toString());
      $request = \Drupal::request();

      // Save the session so things like messages get saved.
      $request->getSession()->save();
      $response->prepare($request);

      // Make sure to trigger kernel events.
      \Drupal::service('kernel')->terminate($request, $response);
      $response->send();
      return $response;
    }
    else {
      return ['#markup' => ''];
    }
  }
}