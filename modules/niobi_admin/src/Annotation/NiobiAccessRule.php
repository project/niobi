<?php

namespace Drupal\niobi_admin\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Niobi access rule item annotation object.
 *
 * @see \Drupal\niobi_admin\Plugin\NiobiAccessRuleManager
 * @see plugin_api
 *
 * @Annotation
 */
class NiobiAccessRule extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
