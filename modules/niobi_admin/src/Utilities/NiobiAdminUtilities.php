<?php
/**
 * @file
 * Contains \Drupal\niobi_admin\Utilities\NiobiAdminUtilities.
 */
namespace Drupal\niobi_admin\Utilities;

/**
 * Class NiobiAdminUtilities
 * @package Drupal\niobi_admin\Utilities
 */
class NiobiAdminUtilities {

  /**
   * Generate the settings form.
   *
   * @param string $option_type
   * May be 'form' for settings form elements, or 'key' for keys to use when settings the config.
   *
   * @return mixed
   */
  public static function getSettingsOptions($option_type = 'form') {
    $plugin_manager = \Drupal::service('plugin.manager.niobi_admin');
    $plugin_definitions = $plugin_manager->getDefinitions();

    $options = [];
    foreach ($plugin_definitions as $pd) {
      switch ($option_type) {
        case 'form':
          $options[$pd['id']] = $pd['class']::getFormOptions();
          break;
      }
    }
    return $options;
  }

}
