<?php

namespace Drupal\niobi_admin\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Niobi access rule plugins.
 */
interface NiobiAccessRuleInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.

}
