<?php

namespace Drupal\niobi_admin\Plugin\niobi\admin;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;
use Drupal\niobi_admin\NiobiAdminInterface;

/**
 * @NiobiAdmin(
 *   id = "niobi_admin_settings",
 *   label = @Translation("Niobi Admin Settings"),
 *   description = @Translation("Admin page introduction and basic settings."),
 * )
 */
class NiobiAdminSettings extends PluginBase implements NiobiAdminInterface {

  /**
   * @return string
   *   A string description.
   */
  public function description() {
    return $this->t('');
  }

  public static function getFormOptions() {
    $niobi_config = \Drupal::configFactory()->getEditable('niobi_admin.settings');
    $ret = [
      'introduction' => [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => t('Introduction'),
        '#group' => 'niobi',
        'niobi_admin__introduction' => [
          '#type' => 'markup',
          '#markup' => '<h2>Welcome to Niobi Research Center</h2>',
        ],
      ],
    ];
    $ret['introduction']['niobi_admin__introduction']['#markup'] .= '<p>Module settings will appear in settings tabs to the left.</p>';
    $suppressions = $niobi_config->get('route_suppression');
    $ret['suppressions'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => t('Route Suppression'),
      '#group' => 'niobi',
      'niobi_admin__route_suppression' => [
        '#type' => 'textarea',
        '#title' => t('List of routes to suppress'),
        '#description' => t('List one route per line. Access to the route will be suppressed for people who do not have the "access niobi suppressed routes" permission'),
        '#description_display' => 'before',
        '#default_value' => $suppressions ? $suppressions : '',
      ],
    ];
    return $ret;
  }

}