<?php

namespace Drupal\niobi_admin\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Niobi access rule plugin manager.
 */
class NiobiAccessRuleManager extends DefaultPluginManager {


  /**
   * Constructs a new NiobiAccessRuleManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/NiobiAccessRule', $namespaces, $module_handler, 'Drupal\niobi_admin\Plugin\NiobiAccessRuleInterface', 'Drupal\niobi_admin\Annotation\NiobiAccessRule');

    $this->alterInfo('niobi_admin_niobi_access_rule_info');
    $this->setCacheBackend($cache_backend, 'niobi_admin_niobi_access_rule_plugins');
  }

}
