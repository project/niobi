<?php

namespace Drupal\niobi_admin\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Niobi access rule plugins.
 */
abstract class NiobiAccessRuleBase extends PluginBase implements NiobiAccessRuleInterface {


  // Add common methods and abstract methods for your plugin type here.

}
