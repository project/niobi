<?php

/**
 * @file
 * Provides Drupal\niobi_admin\NiobiAdminInterface;
.
 */

namespace Drupal\niobi_admin;

/**
 * An interface for all NiobiAdmin type plugins.
 */
interface NiobiAdminInterface {
    /**
     * Provide a description of the plugin.
     * @return string
     *   A string description of the plugin.
     */
    public function description();
}
