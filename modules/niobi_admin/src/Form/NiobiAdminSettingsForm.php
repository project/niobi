<?php

namespace Drupal\niobi_admin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\niobi_admin\Utilities\NiobiAdminUtilities;

/**
 * Class NiobiAdminSettingsForm.
 */
class NiobiAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'niobi_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['niobi_admin.settings'];
  }

  /**
   * Defines the settings form for Niobi Research Center.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = NiobiAdminUtilities::getSettingsOptions('form');
    $config = $this->config('niobi_admin.settings');

    $form['niobi'] = [
      '#type' => 'vertical_tabs',
    ];

    foreach($options as $key => $item) {
      foreach ($item as $form_key => $form_item) {
        $form[$form_key] = $form_item;
        $form[$form_key]['#group'] = 'niobi';
      }
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit'
    ];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('niobi_admin.settings');
    // Clear old data
    $old_data = $config->getRawData();
    foreach ($old_data as $key => $value) {
      $config->clear($key);
    }

    // Set the new data
    $settings = array_diff(array_keys($form_state->getValues()), ['submit', 'form_build_id', 'form_token', 'form_id', 'op', 'niobi__active_tab']);
    foreach ($settings as $key) {
      $form_val = $form_state->getValue($key);
      $k = str_replace('__', '.', $key);
      $config->set($k, $form_val);
      $config->save();
    }
    // Some items require a cache clear.
    drupal_flush_all_caches();
    // We are done, show the finish message.
    \Drupal::messenger()->addMessage('The configuration options have been saved');
  }

}
