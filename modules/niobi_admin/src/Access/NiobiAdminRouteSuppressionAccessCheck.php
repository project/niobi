<?php

namespace Drupal\niobi_admin\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\Routing\Route;


class NiobiAdminRouteSuppressionAccessCheck implements AccessInterface {

  /**
   * @param Route $route
   * @param RouteMatchInterface $route_match
   * @param AccountInterface $account
   * @return AccessResult
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account,'access niobi suppressed routes');
  }

}
