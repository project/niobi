<?php

namespace Drupal\niobi_admin\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


/**
 * Class NiobiAdminRouteSubscriber
 * @package Drupal\flashpoint\Routing
 */
class NiobiAdminRouteSubscriber extends RouteSubscriberBase {

  public function alterRoutes(RouteCollection $collection) {
    $niobi_config = \Drupal::configFactory()->getEditable('niobi_admin.settings');
    $suppressions = $niobi_config->get('niobi_admin.route_suppression');
    $sup_array = preg_split('/$\R?^/m', $suppressions);
    foreach ($sup_array as $sup_route) {
      $sup_route = trim($sup_route);
      if (!empty($sup_route)) {
        $route = $collection->get($sup_route);
        $reqs = $route->getRequirements();
        $reqs['_niobi_admin_route_suppression'] = 'TRUE';
        $route->setRequirements($reqs);
        $collection->add($sup_route, $route);
      }
    }
  }

}
